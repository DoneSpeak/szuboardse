# 深圳大学公文通检索系统

# 基础安装
请提前安装好一下列表：
1. JAVA jdk 1.8
2. Apache Tomcat-8.25.9
3. Apache Maven 3.5.0
4. nodejs 8.9.4
5. git

# 后台打包
## 下载代码
```shell
cd <目标目录>
git clone git@gitlab.com:DoneSpeak/szuboardse.git
```
## 打包
利用maven进行打包
```shell
# windows 服务器
cd szuboardse/szuboardse
mvn clean
mvn package -Dmaven.test.skip=true
```
复制war包到tomcat服务器的webapps下
```shell
# windows 服务器
copy szuboardse-web\target\szuboardse-web.war C:\server\tomcat\webapps
```
## 启动tomcat服务
```shell
cd /tomcat/bin
service.bat install # 安装tomcat服务器
# 服务安装后会输出服务名称
# The service 'Tomcat8' has been installed.
net start Tomcat8 # Tomcat8 是服务安装之后的名字
```
# 前段代码打包
## 下载代码
```shell
cd <目标目录>
git clone git@gitlab.com:DoneSpeak/szuboardse-webfront.git
```
## 打包
```shell
npm install
npm run-script build
```
将dist中的代码复制到C:\var\szuboardse\www\static中，C:\var\szuboardse\www\static是我们服务器存放前段静态代码的目录。
# 配置nginx
## 配置nginx
1. [Windows下Nginx+Tomcat整合的安装与配置](https://blog.csdn.net/cclovett/article/details/26377269)
2. [配置tomcat+nginx反向代理](https://github.com/biezhi/java-bible/blob/master/learn_server/config-nginx-proxy.md)  

下载 -> 解压 -> 双击 nginx.exe(一闪而过) -> 修改 conf/nginx.conf
## 修改 conf/nginx.conf
```shell
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;

    #access_log  logs/host.access.log  main;

    location / {
        root   C:\var\szuboardse\www\static;
        index  index.html index.htm;
    }

	location /resources {
	    proxy_pass http://127.0.0.1:8080/szuboardse-web;
	}

	location /files	{
	    root C:\var\szuboardse\www\store;
	}

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
}

```
修改配置之后，重新启动nginx
```shell
nginx -s reload
```
## 访问
http://127.0.0.1/#/
