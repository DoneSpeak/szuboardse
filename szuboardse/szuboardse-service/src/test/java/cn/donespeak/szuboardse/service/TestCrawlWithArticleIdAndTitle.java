package cn.donespeak.szuboardse.service;

import java.io.IOException;
import java.util.List;

import cn.donespeak.szuboardse.util.crawler.ArticleListCrawler;
import cn.donespeak.szuboardse.util.crawler.ArticleListItem;
import cn.donespeak.szuboardse.util.crawler.BoardSearchOption;
import cn.donespeak.szuboardse.util.crawler.BoardSearchOption.DayyOption;
import cn.donespeak.szuboardse.util.crawler.PageRequester;

public class TestCrawlWithArticleIdAndTitle {
	public static void main(String[] args) throws IOException {
		
		String title = "关于预计2018届本科毕结业学生核对学历电子图像及补采集的通知";
		long articleId = 363874;

		ArticleListCrawler listCrawler = new ArticleListCrawler();
		
		PageRequester requester = new PageRequester(ArticleListCrawler.SZU_BOARD_URL);
		requester.setCharest(ArticleListCrawler.CHARSET);
		BoardSearchOption option = BoardSearchOption.oneMonthSearch();
		option.keyword = title;
		option.dayy = DayyOption.TEN_YEAR;
		
		requester.setParameters(option.toParamMap());
		
		List<ArticleListItem> articleItems = null;
		try {
			articleItems = listCrawler.listArticleItems(requester.post());
		} catch (Exception e) {
			System.out.println("Fail to crawl article " + articleId + " with " + title);
			e.printStackTrace();
		}
		System.out.println("articleItems.size： " + articleItems.size());
		for(ArticleListItem item: articleItems) {
			if(item.getArticleId() == articleId) {
				System.out.println(item);
			}
		}
	}
}
