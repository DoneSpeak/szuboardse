package cn.donespeak.szuboardse.service;

import cn.donespeak.szuboardse.exception.ServiceException;

public interface ArticleService {

	long getMaxArticleId();
	
}
