package cn.donespeak.szuboardse.service;

import java.util.List;
import java.util.Map;

import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.exception.ServiceException;

public interface CategoryService {
	
	List<Category> listCategories() throws ServiceException;

	List<Category> listCategoriesFromCache() throws ServiceException;

}
