package cn.donespeak.szuboardse.service;

import java.util.List;

import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.User;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.vo.securityForm;

public interface UserService {

	User getById(long uid) throws Exception;

	User createUser(String email, String password) throws ServiceException;
	
	void updateUserName(long userId, String name) throws ServiceException;

	void updateUserPassword(long userId, securityForm form) throws ServiceException;
}
