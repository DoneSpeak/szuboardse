package cn.donespeak.szuboardse.service;

import java.util.List;
import java.util.Map;

import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.util.crawler.ArticleListItem;

public interface NotificationService {
	
	void pushFavoriteContents(long userId, String email, 
			Map<FavoriteSource, List<ArticleListItem>> articles) throws ServiceException;
}
