package cn.donespeak.szuboardse.service;

import java.util.List;
import java.util.Map;

import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.ServiceException;

public interface PublisherService {
	
	List<Publisher> listPublishers() throws ServiceException;
	
	List<Publisher> listPublishersFromCache() throws ServiceException;
}
