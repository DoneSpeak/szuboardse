package cn.donespeak.szuboardse.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.SearchCode;
import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.entity.FavoriteContent;
import cn.donespeak.szuboardse.entity.IdValue;
import cn.donespeak.szuboardse.entity.Picture;
import cn.donespeak.szuboardse.exception.AccessDatabaseException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.ArticleMapper;
import cn.donespeak.szuboardse.mapper.AttachMapper;
import cn.donespeak.szuboardse.mapper.ContentMapper;
import cn.donespeak.szuboardse.mapper.FavoriteContentMapper;
import cn.donespeak.szuboardse.mapper.PictureMapper;
import cn.donespeak.szuboardse.service.SearchService;
import cn.donespeak.szuboardse.util.ResultUtil;
import cn.donespeak.szuboardse.util.search.ContentSearcher;
import cn.donespeak.szuboardse.util.search.HighlighterUtil;
import cn.donespeak.szuboardse.util.search.QueryIndexer;
import cn.donespeak.szuboardse.util.search.QuerySearcher;
import cn.donespeak.szuboardse.util.search.SearcherManager;
import cn.donespeak.szuboardse.vo.Pagination;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;
import cn.donespeak.szuboardse.vo.TinyAttachItem;

@Service("searchService")
public class SearchServiceImpl implements SearchService{
	
	Logger logger = Logger.getLogger(SearchServiceImpl.class);
	
	@Inject
	ArticleMapper articleMapper;
	
	@Inject
	ContentMapper contentMapper;
	
	@Inject
	AttachMapper attachMapper;
	
	@Inject
	PictureMapper pictureMapper;
	
	@Inject
	FavoriteContentMapper favoriteContentMapper;
		
	private final int MAX_CONTENT_LENGTH = 140;
	private final int RELATIVE_QUERY_MAX_LENGTH = 10;
	private final int RELATIVE_QUERY_MIN_LENGTH = 2;
	

	private static SearcherManager searcherManager = new SearcherManager();
	
	@Override
	public ResultPage search(SearchOption searchOption, long uid) throws ServiceException {
		long start = System.currentTimeMillis();
		String query = searchOption.getQuery();
		ResultPage resultPage = null;
		if(query == null || query.length() == 0) {
			resultPage = searcheFromDB(searchOption);
		} else {
			try {
				addQueryToSearchHistory(query);
			} catch (IOException | ParseException e) {
				logger.warn("Fail to index query", e);
			}
			resultPage = searchFromIndex(searchOption);
		}
		
		decorateResult(resultPage, uid);
		long end = System.currentTimeMillis();
		resultPage.setTimecost(end - start);
		return resultPage;
	}
	
	private void decorateResult(ResultPage resultPage, long uid) throws ServiceException {
		Map<Long, ResultItem> articleItemMap = new HashMap<Long, ResultItem>();
		Map<Long, ResultItem> attachItemMap = new HashMap<Long, ResultItem>();
		divideResultToArticleAttach(resultPage.getResultItems(), articleItemMap, attachItemMap);
		
		try {
			addPicturesToArticle(articleItemMap);
			addAttachesToResult(articleItemMap, attachItemMap);
			setFavoriteToResult(articleItemMap, attachItemMap, uid);
		} catch (AccessDatabaseException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry, e);
		}
	}

	private void divideResultToArticleAttach(List<ResultItem> resultItems, 
			Map<Long, ResultItem> articleItemMap, Map<Long, ResultItem> attachItemMap) {
		for(ResultItem item : resultItems) {
			if(item.getRegion() == Region.ARTICLE) {
				articleItemMap.put(item.getArticleId(), item);
			} else if(item.getRegion() == Region.ATTACH) {
				attachItemMap.put(item.getAttachId(), item);
			}
		}
	}
	
	@Override
	public void addAttachesToResult(List<ResultItem> resultItems) throws AccessDatabaseException {
		Map<Long, ResultItem> articleItemMap = new HashMap<Long, ResultItem>();
		Map<Long, ResultItem> attachItemMap = new HashMap<Long, ResultItem>();
		divideResultToArticleAttach(resultItems, articleItemMap, attachItemMap);
		
		addAttachesToResult(articleItemMap, attachItemMap);
	}

	public void addPicturesToArticle(Map<Long, ResultItem> articleItemMap) throws AccessDatabaseException {
		if(articleItemMap == null || articleItemMap.size() == 0){
			return;
		}
		List<Long> articleIds = new ArrayList<Long>();
		for(Long id: articleItemMap.keySet()) {
			if(id != null) {
				articleIds.add(id);
			}
		}
		List<Picture> pictures = null;
		if(articleIds != null && articleIds.size() > 0) {
			try {
				// TODO@DoneSpeak 考虑有很多内容的情况
				pictures = pictureMapper.listPicturesIn(articleIds);
			} catch (Exception e) {
				throw new AccessDatabaseException("Fail to get tiny attach item for the articles", e);
			}
			for(Picture pic: pictures) {
				ResultItem resultItem = articleItemMap.get(pic.getArticleId());
				if(resultItem.getAttaches() == null) {
					List<Picture> picturesInArticle = new ArrayList<Picture>();
					picturesInArticle.add(pic);
					resultItem.setPictures(picturesInArticle);
				} else {
					resultItem.getPictures().add(pic);
				}
			}
		}
	}
	
	public void addAttachesToResult(Map<Long, ResultItem> articleItemMap, Map<Long, ResultItem> attachItemMap) 
			throws AccessDatabaseException {
		// 获取一个文章所包含的附件
		addAttachesToArticles(articleItemMap);
		// 获取附件所在文章的其他附件
		addPeerAttachesToAttaches(attachItemMap);
	}
	
	private void addAttachesToArticles(Map<Long, ResultItem> articleItemMap) throws AccessDatabaseException {
		if(articleItemMap == null || articleItemMap.size() == 0){
			return;
		}
		List<Long> articleIds = new ArrayList<Long>();
		for(Long id: articleItemMap.keySet()) {
			if(id != null) {
				articleIds.add(id);
			}
		}
		System.out.println(articleIds);
		List<TinyAttachItem> attaches = null;
		if(articleIds != null && articleIds.size() > 0) {
			try {
				attaches = attachMapper.listTinyAttachesIn(articleIds);
			} catch (Exception e) {
				throw new AccessDatabaseException("Fail to get tiny attach item for the articles", e);
			}
			for(TinyAttachItem attachItem: attaches) {
				ResultItem resultItem = articleItemMap.get(attachItem.getArticleId());
				if(resultItem.getAttaches() == null) {
					List<TinyAttachItem> attacheInArticle = new ArrayList<TinyAttachItem>();
					attacheInArticle.add(attachItem);
					resultItem.setAttaches(attacheInArticle);
				} else {
					resultItem.getAttaches().add(attachItem);
				}
			}
		}
	}
	
	private void addPeerAttachesToAttaches(Map<Long, ResultItem> attachItemMap) throws AccessDatabaseException {
		if(attachItemMap == null || attachItemMap.size() == 0) {
			return;
		}
		// 获取附件所在的文章 -- 这里需要注意的是，不同的附件可能出现在相同的文章中
		// 重新组织为 articleId - list<attachResultItem>
		Map<Long, List<ResultItem>> articleIdMapAttaches = new HashMap<Long, List<ResultItem>>();
		for(ResultItem resultItem: attachItemMap.values()) {
			if(resultItem.getArticleId() != null) {
				List<ResultItem> attachList = articleIdMapAttaches.get(resultItem.getArticleId());
				if(attachList == null) {
					attachList = new ArrayList<ResultItem>();
					attachList.add(resultItem);
					articleIdMapAttaches.put(resultItem.getArticleId(), attachList);
				} else {
					attachList.add(resultItem);
				}
			}
		}
		List<Long> articleIds = new ArrayList<Long>(articleIdMapAttaches.keySet());
		List<TinyAttachItem> peerAttaches = null;
		try {
			peerAttaches = attachMapper.listTinyAttachesIn(articleIds);
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to get peer tiny attach item for the attaches", e);
		}
		// 将结果填入到resultItem 的 attaches 中
		for(TinyAttachItem attach: peerAttaches) {
			for(ResultItem resultItem: articleIdMapAttaches.get(attach.getArticleId())){
				if(resultItem.getAttachId() != attach.getId()) {
					// 忽略自身
					List<TinyAttachItem> resultAttaches = resultItem.getAttaches();
					if(resultAttaches == null) {
						resultAttaches = new ArrayList<TinyAttachItem>();
						resultAttaches.add(attach);
						resultItem.setAttaches(resultAttaches);
					} else {
						resultAttaches.add(attach);
					}
				}
			}
		}
	}

	private ResultPage searcheFromDB(SearchOption searchOption) throws ServiceException {
		ResultPage resultPage = null;
		// 空检索则直接当成是一个浏览模式
		try {
			if(searchOption.getRegion() == Region.ATTACH) {
				resultPage = searchAttachesFromDB(searchOption);
			} else {
				resultPage = searchArticlesFromDB(searchOption);
			}
		} catch (AccessDatabaseException e) {
			logger.error("Fail to search result item from database", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry, e);
		}
		return resultPage;
	}
	
	private ResultPage searchAttachesFromDB(SearchOption searchOption) throws AccessDatabaseException {
		long start = System.currentTimeMillis();
		int pageSize = ContentSearcher.HITS_PER_PAGE;
		int startIndex = (searchOption.getPageIndex() - 1) * pageSize;
		// 获取按照时间升序排序的前10篇文章
		List<Attach> attaches = null;
		int total = 0;
		try {
			attaches = attachMapper.searchAttach(searchOption, startIndex, pageSize);
			total = attachMapper.countSearchAttach(searchOption);
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to list article with search opiton", e);
		}
		
		Pagination pageInfo = new Pagination();
		pageInfo.setPageIndex(searchOption.getPageIndex());
		pageInfo.setPageSize(pageSize);
		pageInfo.setPageNum((total + pageSize - 1) / pageSize);
		pageInfo.setTotal(total);
		
		ResultPage resultPage = new ResultPage(ResultUtil.attachesToItems(attaches), pageInfo);

		long end = System.currentTimeMillis();
		resultPage.setTimecost(end - start);
		return resultPage;
	}
	
	private ResultPage searchArticlesFromDB(SearchOption searchOption) throws AccessDatabaseException {
		long start = System.currentTimeMillis();
		int pageSize = ContentSearcher.HITS_PER_PAGE;
		int startIndex = (searchOption.getPageIndex() - 1) * pageSize;
		// 获取按照时间升序排序的前10篇文章
		List<Article> articles = null;
		int total = 0;
		try {
			articles = articleMapper.searchArticle(searchOption, startIndex, pageSize);
			total = articleMapper.countSearchArticle(searchOption);
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to list article with search opiton", e);
		}
		
		Pagination pageInfo = new Pagination();
		pageInfo.setPageIndex(searchOption.getPageIndex());
		pageInfo.setPageSize(pageSize);
		pageInfo.setPageNum((total + pageSize - 1) / pageSize);
		pageInfo.setTotal(total);
		
		ResultPage resultPage = new ResultPage(ResultUtil.articlesToItems(articles), pageInfo);

		long end = System.currentTimeMillis();
		resultPage.setTimecost(end - start);
		resultPage.setTotalhits(total);
		return resultPage;
	}
	
	public ResultPage searchFromIndex(SearchOption searchOption) throws ServiceException {
		String query = searchOption.getQuery();
		// 添加检索内容到检索历史
		ContentSearcher searcher = searcherManager.getContentSearcher(false);
		ResultPage resultPage = null;
		long start = System.currentTimeMillis();
		try {
			resultPage = searcher.search(searchOption);
			highlightResult(query, resultPage.getResultItems());
		} catch (ParseException | IOException | InvalidTokenOffsetsException e) {
			logger.error("Fail to highlight result", e);
			throw new ServiceException(SearchCode.SEARCH_FAIL.entry, e);
		} catch (AccessDatabaseException e) {
			logger.error("exception in highlight content", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry, e);
		}
		long end = System.currentTimeMillis();
		resultPage.setTimecost(end - start);
		return resultPage;
	}

	private void highlightResult(String queryStr, List<ResultItem> resultItems) 
			throws ParseException, IOException, InvalidTokenOffsetsException, AccessDatabaseException {
		
		Map<Long, ResultItem> articleItemMap = new HashMap<Long, ResultItem>();
		Map<Long, ResultItem> attachItemMap = new HashMap<Long, ResultItem>();		
		divideResultToArticleAttach(resultItems, articleItemMap, attachItemMap);
		
		highlightTitle(queryStr, resultItems);
		highlightContent(queryStr, articleItemMap, attachItemMap);
		
	}
	
	private void highlightContent(String queryStr, Map<Long, ResultItem> articleItemMap, Map<Long, ResultItem> attachItemMap)
			throws IOException, InvalidTokenOffsetsException, AccessDatabaseException, ParseException {
		
		HighlighterUtil highlighter = new HighlighterUtil(queryStr, MAX_CONTENT_LENGTH);
		highlightArticleContent(highlighter, articleItemMap);
		highlightAttachContent(highlighter, attachItemMap);
	}
	
	private void highlightTitle(String queryStr, List<ResultItem> resultItems)
			throws IOException, InvalidTokenOffsetsException, AccessDatabaseException, ParseException {
		
		HighlighterUtil highlighter = new HighlighterUtil(queryStr, Integer.MAX_VALUE);
		if(resultItems != null && resultItems.size() > 0) {
			for(ResultItem item: resultItems) {
				item.setTitle(highlighter.hightlight(item.getTitle()));
			}
		}
	}
	
	private void highlightArticleContent(HighlighterUtil highlighter, Map<Long, ResultItem> articleItemMap) 
			throws IOException, InvalidTokenOffsetsException, AccessDatabaseException {
		if(articleItemMap != null && articleItemMap.size() > 0) {
			List<Long> articleIds = new ArrayList<Long>(articleItemMap.keySet());
			List<IdValue> highlightContents;
			try {
				highlightContents = contentMapper.listArticleContentIn(articleIds);
			} catch (Exception e) {
				throw new AccessDatabaseException("Fail to list articles in " + articleIds, e);
			}
			highlightContent(highlighter, highlightContents);
			for(IdValue idValue: highlightContents) {
				articleItemMap.get(idValue.getId()).setDigest(idValue.getValue());
			}
		}
	}
	
	private void highlightAttachContent(HighlighterUtil highlighter,  Map<Long, ResultItem> attachItemMap) 
			throws IOException, InvalidTokenOffsetsException, AccessDatabaseException {
		if(attachItemMap != null && attachItemMap.size() > 0) {
			List<Long> attachIds = new ArrayList<Long>(attachItemMap.keySet());
			// TODO@DoneSpeak 考虑如果有非常多的ID会发生什么事？
			List<IdValue> highlightContents;
			try {
				highlightContents = contentMapper.listAttachContentIn(attachIds);
			} catch (Exception e) {
				throw new AccessDatabaseException("Fail to list attaches in " + attachIds, e);
			}
			highlightContent(highlighter, highlightContents);
			for(IdValue idValue: highlightContents) {
				attachItemMap.get(idValue.getId()).setDigest(idValue.getValue());
			}
		}
	}
	
	private void highlightContent(HighlighterUtil highlighter, List<IdValue> idValues) 
			throws IOException, InvalidTokenOffsetsException, AccessDatabaseException {
		if(idValues == null || idValues.size() == 0) {
			return;
		}
		for(IdValue idValue: idValues) {
			String highlightedValue = highlighter.hightlight(idValue.getValue());
			idValue.setValue(highlightedValue);
		}
	} 
	
	private void setFavoriteToResult(Map<Long, ResultItem> articleItemMap, Map<Long, ResultItem> attachItemMap,long uid)
			throws AccessDatabaseException {
		if(articleItemMap != null && articleItemMap.size() > 0) {
			List<Long> articleIds = new ArrayList<Long>();
			for(Long id: articleItemMap.keySet()) {
				if(id != null) {
					articleIds.add(id);
				}
			}
			if(articleIds != null && articleIds.size() > 0) {
				List<FavoriteContent> favorites = null;
				try {
					favorites = favoriteContentMapper.listFavoriteArticles(articleIds, uid);
				} catch (Exception e) {
					throw new AccessDatabaseException("Fail to list favorite article", e);
				}
				for(FavoriteContent favorite: favorites) {
					articleItemMap.get(favorite.getArticleId()).setIsfavorite(true);
				}
			}
		}
		if(attachItemMap != null && attachItemMap.size() > 0) {
			List<Long> attachIds = new ArrayList<Long>();
			for(Long id: attachItemMap.keySet()) {
				if(id != null) {
					attachIds.add(id);
				}
			}
			if(attachIds != null && attachIds.size() > 0) {
				List<FavoriteContent> favorites = null;
				try {
					favorites = favoriteContentMapper.listFavoriteAttaches(attachIds, uid);
				} catch (Exception e) {
					throw new AccessDatabaseException("Fail to list favorite attaches", e);
				}
				for(FavoriteContent favorite: favorites) {
					attachItemMap.get(favorite.getAttachId()).setIsfavorite(true);
				}
			}
		}
	}

	private void addQueryToSearchHistory(String query) throws IOException, ParseException {
		if(queryNeedStored(query)) {
			QuerySearcher searcher = searcherManager.getQuerySearcher(true);
			List<String> relativeQueries = searcher.search(query, 15);
			for(String rq: relativeQueries) {
				if(query.equals(rq)) {
					// 不记录重复的内容
					return;
				}
			}
			QueryIndexer indexer = new QueryIndexer();
			indexer.addQueryAndClose(query);
		}
	}

	@Override
	public List<String> relativeQueries(String keyword, int top) throws ServiceException {		
		try {
			QuerySearcher searcher = searcherManager.getQuerySearcher(true);
			List<String> relativeQueries = searcher.search(keyword, top);
			List<String> result = new ArrayList<String>();
			for(String rq: relativeQueries) {
				if(!(rq == null || rq.equals(keyword))) {
					result.add(rq);
				}
			}
			return result;
		} catch (IOException | ParseException e) {
			logger.error("Fail to index query or get relative", e);
			throw new ServiceException(SystemCode.INNER_ERROR.entry);
		}
	}
	
	private boolean queryNeedStored(String query) {
		// TODO@DoneSpeak 应该移除开始和结尾处的空白符号
		return (query != null && query.length() >= RELATIVE_QUERY_MIN_LENGTH
				&& query.length() <= RELATIVE_QUERY_MAX_LENGTH);
	}
	
	@Override
	public List<ResultItem> searchSimilarArticles(long articleId) throws ServiceException {
		// TODO@DoneSpeak 有空再去完成这个功能吧
		return null;
	}

	@Override
	public List<ResultItem> searchSimilarAttaches(long attachId) throws ServiceException {
		// TODO@DoneSpeak 有空再去完成这个功能吧
		return null;
	}
}
