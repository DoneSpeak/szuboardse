package cn.donespeak.szuboardse.service.impl;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.entity.FavoriteContent;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.ArticleMapper;
import cn.donespeak.szuboardse.mapper.FavoriteContentMapper;
import cn.donespeak.szuboardse.service.ArticleService;
import cn.donespeak.szuboardse.util.DateUtil;

@Service("articleService")
public class ArticleServiceImpl implements ArticleService{
	
	@Inject
	ArticleMapper articleMapper;
	
	@Override
	public long getMaxArticleId() {
		Long id = null;
		try {
			id = articleMapper.getMaxId();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id == null? -1: id;
	}
}
