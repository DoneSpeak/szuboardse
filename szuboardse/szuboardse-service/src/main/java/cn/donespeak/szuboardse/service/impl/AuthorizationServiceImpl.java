package cn.donespeak.szuboardse.service.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.Auth;
import cn.donespeak.szuboardse.base.responsecode.BaseCode;
import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.dto.TokenUser;
import cn.donespeak.szuboardse.entity.User;
import cn.donespeak.szuboardse.exception.InnerAlgorithmException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.UserMapper;
import cn.donespeak.szuboardse.service.AuthorizationService;
import cn.donespeak.szuboardse.util.JwtUtil;
import cn.donespeak.szuboardse.util.account.PasswordUtil;

@Service("authorizationService")
public class AuthorizationServiceImpl implements AuthorizationService{

	@Inject
	UserMapper userMapper;
	
	@Override
	public TokenUser generateToken(String email, String password) throws ServiceException {
		User user = null;
		try {
			user = userMapper.getByEmail(email);
		} catch (Exception e) {
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		if(user == null) {
			throw new ServiceException(Auth.USER_NOT_FOUND.entry);
		}
		try {
			if(!PasswordUtil.isValidate(password, user.getPassword())) {
				throw new ServiceException(Auth.EMAIL_OR_PASSWORD_INCORRECT.entry);
			}
		} catch (InnerAlgorithmException e) {
			throw new ServiceException(SystemCode.INNER_ERROR.entry);
		}
		String token = JwtUtil.createLoginJWT(user.getUid());
		
		TokenUser tokenUser = new TokenUser();
		tokenUser.setToken(token);
		tokenUser.setUser(user);
		return tokenUser;
	}

	@Override
	public String refreshToken(long userId) throws ServiceException {
		return JwtUtil.createLoginJWT(userId);
	}

}
