package cn.donespeak.szuboardse.service;

import cn.donespeak.szuboardse.dto.TokenUser;
import cn.donespeak.szuboardse.exception.ServiceException;

public interface AuthorizationService {
	TokenUser generateToken(String email, String password) throws ServiceException;
	
	String refreshToken(long userId) throws ServiceException;
}
