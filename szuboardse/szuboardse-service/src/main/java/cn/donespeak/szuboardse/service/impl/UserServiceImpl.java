package cn.donespeak.szuboardse.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.base.responsecode.UserCode;
import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.User;
import cn.donespeak.szuboardse.exception.InnerAlgorithmException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.FavoriteSourceMapper;
import cn.donespeak.szuboardse.mapper.UserMapper;
import cn.donespeak.szuboardse.service.UserService;
import cn.donespeak.szuboardse.util.DateUtil;
import cn.donespeak.szuboardse.util.account.FormUtil;
import cn.donespeak.szuboardse.util.account.PasswordUtil;
import cn.donespeak.szuboardse.vo.securityForm;

@Service("userService")
public class UserServiceImpl implements UserService{
	Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	@Inject
	UserMapper userMapper;
	@Inject
	FavoriteSourceMapper favoriteSourceMapper;
	
	@Override
	public User getById(long uid) throws Exception {
		return userMapper.getById(uid);
	}

	@Override
	public User createUser(String email, String password) throws ServiceException {
		User user = null;
		try {
			user = userMapper.getByEmail(email);
		} catch (Exception e) {
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		if (user != null) {
			throw new ServiceException(UserCode.EMAIL_TAKEN.entry);
		}
		long now = DateUtil.getNow();
		
		User newUser = new User();
		newUser.setAvatar(User.DEFAULT_AVATAR);
		newUser.setEmail(email);
		newUser.setGmtCreated(now);
		newUser.setRegistrationTime(now);
		newUser.setName(getUserNameFromEmail(email));
		try {
			String passwordEncrypted = PasswordUtil.encrypt(password);
			newUser.setPassword(passwordEncrypted);
		} catch (InnerAlgorithmException e) {
			logger.error("Exception happens when encrypting password", e);
			throw new ServiceException(SystemCode.INNER_ERROR.entry);
		}
		try {
			userMapper.insert(newUser);
		} catch (Exception e) {
			logger.error("Fail to insert user into user table", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		return newUser;
	}
	
	private String getUserNameFromEmail(String email) {
		return email.substring(0, email.indexOf("@")); 
	}

	@Override
	public void updateUserName(long userId, String name) throws ServiceException {
		try {
			userMapper.updateName(userId, name);
		} catch (Exception e) {
			logger.error("Fail to update user name.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}

	@Override
	public void updateUserPassword(long userId, securityForm form) throws ServiceException {
		User user;
		try {
			System.out.println(form);
			user = userMapper.getByEmail(form.getEmail());
			checkForm(userId, form, user);
			userMapper.updatePassword(userId, PasswordUtil.encrypt(form.getNewPassword()));
		} catch (ServiceException e) {
			logger.info(e);
			throw e;
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}

	private void checkForm(long userId, securityForm form, User user) throws ServiceException {		
		if(!FormUtil.validatePassword(form.getNewPassword())) {
			// 密码格式不对
			throw new ServiceException(UserCode.PASSWORD_INVALID.entry);
		}
		if(user == null || user.getUid() != userId) {
			// 邮箱错误
			throw new ServiceException(UserCode.EMAIL_WRONG.entry);
		}
		try {
			if(!PasswordUtil.isValidate(form.getPassword(), user.getPassword())) {
				// 密码错误
				throw new ServiceException(UserCode.PASSWORD_WRONG.entry);
			}
		} catch (InnerAlgorithmException e) {
			throw new ServiceException(SystemCode.INNER_ERROR.entry);
		}
	}
}
