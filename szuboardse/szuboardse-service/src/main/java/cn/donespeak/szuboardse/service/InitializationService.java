package cn.donespeak.szuboardse.service;

import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.util.search.IndexException;

public interface InitializationService {

	void collectDate();
	
	void init();
}
