package cn.donespeak.szuboardse.service.impl;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.NotificationService;
import cn.donespeak.szuboardse.util.board.CategoryCache;
import cn.donespeak.szuboardse.util.board.PublisherCache;
import cn.donespeak.szuboardse.util.crawler.ArticleListItem;
import cn.donespeak.szuboardse.util.msg.EmailSender;

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {
	
	private Logger logger = Logger.getLogger(NotificationServiceImpl.class);

	@Override
	public void pushFavoriteContents(long userId, String email, Map<FavoriteSource, List<ArticleListItem>> articles)
			throws ServiceException {
		EmailSender sender = EmailSender.getDefaultEmailSender();
		String subject = "你所关注的公文通来源有新的发布";
		String content = generateFavUpdateNofitication(articles);
		
		try {
			sender.send(email, subject, content);
		} catch (UnsupportedEncodingException | GeneralSecurityException | MessagingException e) {
			logger.warn("Fail to send email to " + email, e);
		}
	}

	private String generateFavUpdateNofitication(Map<FavoriteSource, List<ArticleListItem>> articles) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div>");
		for(Entry<FavoriteSource, List<ArticleListItem>> fav: articles.entrySet()) {
			FavoriteSource favSrc = fav.getKey();
			String categoryName = CategoryCache.get(favSrc.getCategoryId()).getName();
			String pulisherName = PublisherCache.get(favSrc.getPublisherId()).getName();
	        sb.append("<h3>" + pulisherName + " - " + categoryName + "</h3>");
	        sb.append("<p>");
			for(ArticleListItem item: fav.getValue()) {
		        sb.append("<a href='" + item.getUrl() + "'>" + item.getTitle() + "</a></br>");
			}
	        sb.append("</p>");
		}
		sb.append("</br></br>");
		sb.append("<p><a href='http://carelative.donespeak.com' target='_blank'>carelative.donespeak.com</a></p>");
        sb.append("</div>");
        return sb.toString();
	}

}
