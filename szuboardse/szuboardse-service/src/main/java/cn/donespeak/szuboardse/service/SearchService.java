package cn.donespeak.szuboardse.service;

import java.util.List;
import java.util.Map;

import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.exception.AccessDatabaseException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;

public interface SearchService {
	
	ResultPage search(SearchOption searchOption, long uid) throws ServiceException;

	List<String> relativeQueries(String query, int top) throws ServiceException;

	List<ResultItem> searchSimilarArticles(long articleId) throws ServiceException;

	List<ResultItem> searchSimilarAttaches(long attachId) throws ServiceException;

	void addAttachesToResult(List<ResultItem> resultItems) throws AccessDatabaseException;
}
