package cn.donespeak.szuboardse.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.CategoryCode;
import cn.donespeak.szuboardse.base.responsecode.PublisherCode;
import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.dto.FavoriteSources;
import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.FavoriteContent;
import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.IdValue;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.AccessDatabaseException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.ArticleMapper;
import cn.donespeak.szuboardse.mapper.AttachMapper;
import cn.donespeak.szuboardse.mapper.FavoriteContentMapper;
import cn.donespeak.szuboardse.mapper.FavoriteSourceMapper;
import cn.donespeak.szuboardse.service.FavoriteService;
import cn.donespeak.szuboardse.service.SearchService;
import cn.donespeak.szuboardse.util.DateUtil;
import cn.donespeak.szuboardse.util.ResultUtil;
import cn.donespeak.szuboardse.util.board.CategoryCache;
import cn.donespeak.szuboardse.util.board.PublisherCache;
import cn.donespeak.szuboardse.vo.Pagination;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;

@Service("favoriteService")
public class FavoriteServiceImpl implements FavoriteService{

	private Logger logger = Logger.getLogger(ArticleServiceImpl.class);
	
	@Inject
	FavoriteContentMapper favoriteContentMapper;
	
	@Inject
	ArticleMapper articleMapper;
	
	@Inject
	AttachMapper attachMapper;

	@Inject
	FavoriteSourceMapper favoriteSourceMapper;

	@Inject
	SearchService searchService;
	
	@Override
	public ResultPage listFavoriteArticles(long userId, int pageIndex, int pageSize) throws ServiceException {
		if(pageIndex < 1 || pageSize < 1) {
			return ResultPage.getEmptyResultPage();
		}
		int startIndex = Pagination.calculateStartIndex(pageIndex, pageSize);
		List<Article> articles = null;
		int total = 0;
		try {
			articles= articleMapper.listFavoriteArticles(userId, startIndex, pageSize);
			total = favoriteContentMapper.countFavoriteArticles(userId);
		} catch (Exception e) {
			logger.error("Fail to list favorite articles or count favorite articles", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		int pageNum = Pagination.countPageNum(pageSize, total);
		Pagination pageInfo = new Pagination(pageIndex, pageSize, pageNum, total);
		List<ResultItem> resultItems = ResultUtil.articlesToItems(articles);
		decorateResultItems(resultItems);
		
		ResultPage resultPage = new ResultPage();
		resultPage.setResultItems(resultItems);
		resultPage.setPageInfo(pageInfo);
		resultPage.setTotalhits(total);
		
		return resultPage;
	}

	@Override
	public ResultPage listFavoriteAttaches(long userId, int pageIndex, int pageSize) throws ServiceException {
		if(pageIndex < 1 || pageSize < 1) {
			return ResultPage.getEmptyResultPage();
		}
		int startIndex = Pagination.calculateStartIndex(pageIndex, pageSize);
		List<Attach> attaches = null;
		int total = 0;
		try {
			attaches= attachMapper.listFavoriteAttaches(userId, startIndex, pageSize);
			total = favoriteContentMapper.countFavoriteAttaches(userId);
		} catch (Exception e) {
			logger.error("Fail to list favorite articles or count favorite articles", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		int pageNum = Pagination.countPageNum(pageSize, total);
		Pagination pageInfo = new Pagination(pageIndex, pageSize, pageNum, total);
		List<ResultItem> resultItems = ResultUtil.attachesToItems(attaches);
		decorateResultItems(resultItems);
		ResultPage resultPage = new ResultPage();
		resultPage.setResultItems(resultItems);
		resultPage.setPageInfo(pageInfo);
		resultPage.setTotalhits(total);
		
		return resultPage;
	}
	
	private void decorateResultItems(List<ResultItem> resultItems) throws ServiceException {
		if(resultItems == null) {
			return;
		}
		for(ResultItem item: resultItems) {
			item.setIsfavorite(true);
		}
		try {
			searchService.addAttachesToResult(resultItems);
		} catch (AccessDatabaseException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry, e);
		}
	}
	
	@Override
	public void addFavoriteArticle(long userId, long articleId) throws ServiceException{
		FavoriteContent favoriteContent = new FavoriteContent();
		favoriteContent.setUid(userId);
		favoriteContent.setArticleId(articleId);
		favoriteContent.setGmtCreated(DateUtil.getNow());
		try {
			favoriteContentMapper.insertFavoriteArticle(favoriteContent);
		} catch (Exception e) {
			logger.error("Fail to insert favorite content.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}
	
	@Override
	public void addFavoriteAttach(long userId, long attachId) throws ServiceException{
		FavoriteContent favoriteContent = new FavoriteContent();
		favoriteContent.setUid(userId);
		favoriteContent.setAttachId(attachId);
		favoriteContent.setGmtCreated(DateUtil.getNow());
		try {
			favoriteContentMapper.insertFavoriteAttach(favoriteContent);
		} catch (Exception e) {
			logger.error("Fail to insert favorite content.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}
	
	@Override
	public void cancelFavoriteArticle(long userId, long articleId) throws ServiceException {
		try {
			favoriteContentMapper.deleteFavoriteArticle(userId, articleId);
		} catch (Exception e) {
			logger.error("Fail to delete favorite content.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}

	@Override
	public void cancelFavoriteAttach(long userId, long attachId) throws ServiceException {
		try {
			favoriteContentMapper.deleteFavoriteAttach(userId, attachId);
		} catch (Exception e) {
			logger.error("Fail to delete favorite content.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}
	
	@Override
	public long addFavoriteSource(long userId, int categoryId, int publisherId) throws ServiceException {
		if(CategoryCache.get(categoryId) == null){
			throw new ServiceException(CategoryCode.DATA_NOT_EXIST.entry);
		}
		if(PublisherCache.get(publisherId) == null) {
			throw new ServiceException(PublisherCode.DATA_NOT_EXIST.entry);
		}
		FavoriteSource favoriteSource = new FavoriteSource();
		favoriteSource.setUid(userId);
		favoriteSource.setCategoryId(categoryId);
		favoriteSource.setPublisherId(publisherId);
		favoriteSource.setGmtCreated(DateUtil.getNow());
		try {
			favoriteSourceMapper.insert(favoriteSource);
		} catch (Exception e) {
			logger.error("Fail to insert favorite source.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		return favoriteSource.getId();
	}
	
	@Override
	public void removeFavoriteSource(long userId, int categoryId, int publisherId) throws ServiceException {
		if(CategoryCache.get(categoryId) == null){
			throw new ServiceException(CategoryCode.DATA_NOT_EXIST.entry);
		}
		if(PublisherCache.get(publisherId) == null) {
			throw new ServiceException(PublisherCode.DATA_NOT_EXIST.entry);
		}
		try {
			favoriteSourceMapper.removeSource(userId, categoryId, publisherId);
		} catch (Exception e) {
			logger.error("Fail to insert favorite source.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}


	@Override
	public void deleteFavorieSource(long userId, long favoriteSourceId) throws ServiceException {
		try {
			favoriteSourceMapper.delete(userId, favoriteSourceId);
		} catch (Exception e) {
			logger.error("Fail to insert favorite source.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}

	@Override
	public List<FavoriteSources> listFavoriteSources(long userId) throws ServiceException {
		List<FavoriteSource> favorites = null;
		try {
			favorites = favoriteSourceMapper.listFavoriateSources(userId);
		} catch (Exception e) {
			logger.error("Fail to list favorite source.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
		return packageSources(favorites);
	}
	
	public List<FavoriteSources> packageSources(List<FavoriteSource> favorites) {
		Map<Integer, FavoriteSources> categoryMapSources = new HashMap<Integer, FavoriteSources>();
		for(FavoriteSource src: favorites) {
			Publisher p = PublisherCache.get(src.getPublisherId());
			if(categoryMapSources.containsKey(src.getCategoryId())) {
				categoryMapSources.get(src.getCategoryId()).addPublisher(p);
			} else {
				Category c = CategoryCache.get(src.getCategoryId());
				FavoriteSources favorite = new FavoriteSources(c);
				favorite.addPublisher(p);
				categoryMapSources.put(src.getCategoryId(), favorite);
			}
		}
		return new ArrayList<FavoriteSources>(categoryMapSources.values());
	}
}
