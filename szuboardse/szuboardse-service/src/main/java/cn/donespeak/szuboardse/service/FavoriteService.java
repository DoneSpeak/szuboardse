package cn.donespeak.szuboardse.service;

import java.util.List;
import java.util.Map;

import cn.donespeak.szuboardse.dto.FavoriteSources;
import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.IdValue;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;

public interface FavoriteService {
	
	ResultPage listFavoriteArticles(long userId, int pageIndex, int pageSize) throws ServiceException;
	
	ResultPage listFavoriteAttaches(long userId, int pageIndex, int pageSize)  throws ServiceException;

	void addFavoriteArticle(long userId, long articleId) throws ServiceException;

	void addFavoriteAttach(long userId, long attachId) throws ServiceException;

	void cancelFavoriteArticle(long userId, long articleId) throws ServiceException;
	
	void cancelFavoriteAttach(long userId, long attachId) throws ServiceException;

	long addFavoriteSource(long userId, int categoryId, int publisherId) throws ServiceException;

	void removeFavoriteSource(long userId, int categoryId, int publisherId) throws ServiceException;

	void deleteFavorieSource(long userId, long favoriteSourceId) throws ServiceException;

	List<FavoriteSources> listFavoriteSources(long userId) throws ServiceException;
}
