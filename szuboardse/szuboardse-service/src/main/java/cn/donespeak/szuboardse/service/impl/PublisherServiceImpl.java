package cn.donespeak.szuboardse.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.AccessDatabaseException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.PublisherMapper;
import cn.donespeak.szuboardse.service.PublisherService;
import cn.donespeak.szuboardse.util.board.PublisherCache;

@Service("publisherService")
public class PublisherServiceImpl implements PublisherService{
	private Logger logger = Logger.getLogger(PublisherServiceImpl.class);
	
	@Inject
	private PublisherMapper publisherMapper;
	
	@Override
	public List<Publisher> listPublishers() throws ServiceException {
		try {
			return publisherMapper.listPublishers();
		} catch (Exception e) {
			logger.error("Fail to get publisher from database.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}

	@Override
	public List<Publisher> listPublishersFromCache() throws ServiceException {

		return PublisherCache.listPublishers();
	}
}
