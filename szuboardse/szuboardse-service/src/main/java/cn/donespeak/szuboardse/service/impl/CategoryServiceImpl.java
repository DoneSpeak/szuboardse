package cn.donespeak.szuboardse.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.exception.AccessDatabaseException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.mapper.CategoryMapper;
import cn.donespeak.szuboardse.service.CategoryService;
import cn.donespeak.szuboardse.util.board.CategoryCache;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService{
	private Logger logger = Logger.getLogger(CategoryServiceImpl.class);
		
	@Inject
	private CategoryMapper categoryMapper;
	
	@Override
	public List<Category> listCategories() throws ServiceException {
		try {
			return categoryMapper.listCategories();
		} catch (Exception e) {
			logger.error("Fail to get category from database.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}
	
	@Override
	public List<Category> listCategoriesFromCache() throws ServiceException {
		return CategoryCache.listCategories();
	}
		
}
