package cn.donespeak.szuboardse.service.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.base.responsecode.SystemCode;
import cn.donespeak.szuboardse.dto.FullArticle;
import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.IdValue;
import cn.donespeak.szuboardse.entity.Picture;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.AccessDatabaseException;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.exception.crawler.WrongPageLayoutException;
import cn.donespeak.szuboardse.mapper.ArticleMapper;
import cn.donespeak.szuboardse.mapper.AttachMapper;
import cn.donespeak.szuboardse.mapper.ContentMapper;
import cn.donespeak.szuboardse.mapper.FavoriteSourceMapper;
import cn.donespeak.szuboardse.mapper.PictureMapper;
import cn.donespeak.szuboardse.service.ArticleService;
import cn.donespeak.szuboardse.service.CategoryService;
import cn.donespeak.szuboardse.service.InitializationService;
import cn.donespeak.szuboardse.service.NotificationService;
import cn.donespeak.szuboardse.service.PublisherService;
import cn.donespeak.szuboardse.util.board.CategoryCache;
import cn.donespeak.szuboardse.util.board.PublisherCache;
import cn.donespeak.szuboardse.util.crawler.ArticleCrawler;
import cn.donespeak.szuboardse.util.crawler.ArticleListCrawler;
import cn.donespeak.szuboardse.util.crawler.ArticleListItem;
import cn.donespeak.szuboardse.util.crawler.BoardSearchOption;
import cn.donespeak.szuboardse.util.crawler.BoardSearchOption.DayyOption;
import cn.donespeak.szuboardse.util.crawler.CrawlerHelper;
import cn.donespeak.szuboardse.util.crawler.PageRequester;
import cn.donespeak.szuboardse.util.search.ContentIndexer;
import cn.donespeak.szuboardse.util.search.ContentSearcher;
import cn.donespeak.szuboardse.util.search.IndexException;
import cn.donespeak.szuboardse.util.search.QueryIndexer;

@Service("initializationService")
public class InitializationServiceImpl implements InitializationService {
	Logger logger = Logger.getLogger(InitializationServiceImpl.class);
	
	@Inject
	ArticleMapper articleMapper;
	
	@Inject
	PictureMapper pictureMapper;
	
	@Inject
	AttachMapper attachMapper;
	
	@Inject
	ContentMapper contentMapper;
	
	@Inject
	FavoriteSourceMapper favoriteSourceMapper;
	
	@Inject
	CategoryService categoryService;
	
	@Inject
	PublisherService publisherService;
	
	@Inject
	ArticleService articleService;
	
	@Inject
	NotificationService notificationService;
	
	// 该值为通过测试得出最小的 Article id， 但时间过长，有些category（如广播）或者publisher已经不存在
	// private final int MIN_ARTICLE_ID = 65254;
	// 计算机与软件学院开学典礼 http://www1.szu.edu.cn/board/view.asp?id=285232
	private final int MIN_ARTICLE_ID = 285232;
	
	private static boolean dataInitialized = false;

	@PostConstruct
	@Override
	public void init() {
		collectDate();
		new Thread(new Runnable() {
			@Override
			public void run() {
				initData();
				dataInitialized = true;
			}
		}).start();
	}
	
	public void initialCache() throws ServiceException {
		CategoryCache.updateCache(categoryService.listCategories());
		PublisherCache.updateCache(publisherService.listPublishers());
	}
	
	public void initData() {
		// TODO@Donespeak
		// 先利用数据库的数据进行构造index
		try {
			initialCache();
		} catch (ServiceException e) {
			logger.error("Fail to init the category cache and publishers cache.", e);
			return;
		}
		if(dataInitialized) {
			return;
		}
		// 然后利用爬虫抓取之后的数据
		long lastArticleId = -1L;
		long latestArticleId = -1;
		try {
			// 从数据库中找到当前已经抓取的文章的最大编号
			lastArticleId = articleService.getMaxArticleId();
			if(lastArticleId < 0) {
				// 数据库中没有任何数据 需要进行初始化
				deleteAllIndex();
			} else {
				// 数据库中已有数据, 已经经过初始化
				logger.info("System has been initialized");
			}
			lastArticleId = lastArticleId < MIN_ARTICLE_ID ? MIN_ARTICLE_ID: lastArticleId;
			// 尝试抓取当前最新的文章
			latestArticleId = getLatestArticleId();
		} catch (Exception e) {
			logger.error("Fail to init system data", e);
			return;
		}
		logger.info("crawl new articles from " + lastArticleId + " to " + latestArticleId);
		// 初始化应该从 已经发布的文章的大id开始，一直抓取到数据库中已经存在的最大id
		// lastArticleId < latestArticleId
		crawlWithMutilThreads(lastArticleId, latestArticleId);
	}
	
	private void crawlOneByOne(long lastArticleId, long latestArticleId) {
		
	}
	
	private void crawlWithMutilThreads(long lastArticleId, long latestArticleId) {
		ExecutorService exec = Executors.newFixedThreadPool(10); 
	    CompletionService<List<FullArticle>> execcomp = new ExecutorCompletionService<List<FullArticle>>(exec);

		final int MAX_ARTICLE_BATCH_SIZE = 100;
	    long articleId = lastArticleId + 1;
	    long taskNum = 0;
		for(; articleId <= latestArticleId; articleId ++ ) {
			long endArticleId = articleId + MAX_ARTICLE_BATCH_SIZE;
			endArticleId = endArticleId < latestArticleId? endArticleId: latestArticleId; 
			execcomp.submit(getCrawlDataTask(articleId, endArticleId));
			taskNum ++;
			articleId = endArticleId;
		}
		for(int taskIndex = 0; taskIndex < taskNum; taskIndex ++) {
			List<FullArticle> fullArticles = null;
			try {
				Future<List<FullArticle>>future = execcomp.take();
				fullArticles = future.get();
			} catch (InterruptedException | ExecutionException e) {
				logger.error("Fail the crawl article for the executor is interrupted", e);
				continue;
			}
			saveAndIndex(fullArticles);
		}
		exec.shutdown();
		
	}

	private Callable<List<FullArticle>> getCrawlDataTask(final long startArticeIndex, final long endArticleIndex) {
		return new Callable<List<FullArticle>>() {
			@Override
			public List<FullArticle> call() throws Exception {
				return crawlData(startArticeIndex, endArticleIndex);
			}
		};
	}
	
	private List<FullArticle> crawlData(long startArticeIndex, long endArticleIndex) {
		List<FullArticle> articles = new ArrayList<FullArticle>();
		for(long articleId = startArticeIndex; articleId <= endArticleIndex; articleId ++ ) {
			// TODO@DoneSpeak 尝试是用多线程处理
			FullArticle fullArticle = null;
			try {
				fullArticle = crawlArticleFromArticleId(articleId);
			} catch (IOException | WrongPageLayoutException | ParseException e) {
				logger.error(e.getMessage(), e);
				// 会导致丢失一篇文章 或者 该编号的文章不存在
				continue;
			}
			if(fullArticle == null) {
				// 会导致丢失一篇文章 或者 该编号的文章不存在
				continue;
			}
			articles.add(fullArticle);
		}
		return articles;
	}
	
	private void saveAndIndex(List<FullArticle> fullArticles) {
		List<Article> articles = new ArrayList<Article>();
		List<Picture> pictures = new ArrayList<Picture>();
		List<Attach> attaches = new ArrayList<Attach>();
		for(FullArticle fullArticle: fullArticles) {
			if(fullArticle == null) {
				continue;
			}
			articles.add(fullArticle.getArticle());
			pictures.addAll(fullArticle.getPictures());
			attaches.addAll(fullArticle.getAttaches());
		}
		
		if(articles.size() > 0) {
			try {
				saveAndIndex(articles, attaches, pictures);
			} catch (AccessDatabaseException | IndexException | IOException e) {
				// TODO@DoneSpeak 严重错误 info me
				logger.error(e.getMessage(), e);
			}
		}
	}

	public void initDataFromDB(boolean removeIndexes) throws ServiceException, IndexException {
		initialCache();
		// TODO@Donespeak
		// 先利用数据库的数据进行构造index
		if(removeIndexes) {
			try {
				deleteAllIndex();
			} catch (IOException e) {
				throw new IndexException(e);
			}
		}
		initIndexesFromDB();
	}
	
	private void deleteAllIndex() throws IOException {
		ContentIndexer indexer = new ContentIndexer();
		indexer.deleteAll();
		indexer.close();
		QueryIndexer queryIndexer = new QueryIndexer();
		queryIndexer.deleteAll();
		queryIndexer.close();
	}

	/**
	 * 利用文章的id，抓取到完整的文章内容
	 * @param articleId
	 * @return
	 * @throws IOException
	 * @throws WrongPageLayoutException
	 * @throws ParseException
	 */
	private FullArticle crawlArticleFromArticleId(long articleId) 
			throws IOException, WrongPageLayoutException, ParseException {
		ArticleListItem item = new ArticleListItem();
		String url = CrawlerHelper.generateArticleUrl(articleId);
		item.setArticleId(articleId);
		item.setUrl(url);
		item.setTitle(null);
		item.setCategory(new Category(-1, "FAKE CATEGORY"));
		item.setPublisher(new Publisher(-1, "FAKE CATEGORY"));

		logger.info("crawl article " + articleId + " from " + url);
		
		ArticleCrawler crawler = new ArticleCrawler(item);
		FullArticle fullArticle = null;
		try {
			fullArticle = crawler.extractFullArticle();
		} catch (Exception e) {
			logger.warn("Fail to crawl article " + item.getUrl(), e);
			return null;
		}
		if(fullArticle == null || fullArticle.getArticle() == null) {
			logger.warn("Fail to crawl article " + item.getUrl());
			return null;
		}
		/**
		 * 由于直接进入到文章内容页时没有办法获取到文章的 category 的，因而需要利用文章的标题和ID进行从文章列表也进行抓取
		 */
		ArticleListItem itemForArticle = getArticleListItemWithTitleAndId(
				fullArticle.getArticle().getTitle(), articleId);
		if(itemForArticle == null) {
			logger.warn("article list item with articleId " + articleId 
					+ " and title " + fullArticle.getArticle().getTitle() + " doesn't exist");
			return null;
		}
		Publisher publisher = itemForArticle.getPublisher();
		Category category = itemForArticle.getCategory();
		fullArticle.setPublisher(publisher);
		fullArticle.setCategory(category);
		return fullArticle;
	}
	
	// TODO@DoneSpeak 无法按照请求的内容请求到应该得到的结果。
	private ArticleListItem getArticleListItemWithTitleAndId(String title, long articleId) throws IOException {
		ArticleListCrawler listCrawler = new ArticleListCrawler();
		
		PageRequester requester = new PageRequester(ArticleListCrawler.SZU_BOARD_URL);
		requester.setCharest(ArticleListCrawler.CHARSET);
		BoardSearchOption option = BoardSearchOption.oneMonthSearch();
		option.keyword = title;
		option.dayy = DayyOption.FIFTY_YEAR;
		
		requester.setParameters(option.toParamMap());
		
		List<ArticleListItem> articleItems = null;
		try {
			logger.info("crawl article list items for title " + title + " with articleId " + articleId);
			String articleListHtml = requester.post();
			articleItems = listCrawler.listArticleItems(articleListHtml);
			if(articleItems.isEmpty()) {
				logger.error("EMPTY: crawl article list item with articleId " + articleId + " and title");				
			} else {
				logger.error("SUCCESS: crawl article list item with articleId " + articleId + " and title");
			}
		} catch (Exception e) {
			logger.error("Fail to crawl article list item with articleId " + articleId + " and title " + title, e);
			return null;
		}
		
		if(articleItems.size() == 0) {
			return null;
		}
		for(ArticleListItem item: articleItems) {
			if(item.getArticleId() == articleId) {
				return item;
			}
		}
		return null;
	}
	
	private long getLatestArticleId() throws IOException {
		BoardSearchOption option = BoardSearchOption.defaultSearch(DayyOption.THREE_DAIES);
		
		// 从公文通列表页面获取三天内发布的文章 -> 根据数据库中最大id划分最新发布文章和旧的文章
		ArticleListCrawler listCrawler = new ArticleListCrawler();
		List<ArticleListItem> articleItems = listCrawler.listArticleItems(option);
		if(articleItems.size() == 0){
			option.dayy = DayyOption.ONE_WEEK;
			listCrawler = new ArticleListCrawler();
			articleItems = listCrawler.listArticleItems(option);
		}
		long latestId = -1;
		for(ArticleListItem item: articleItems) {
			latestId = latestId > item.getArticleId()? latestId: item.getArticleId();
		}
		return latestId;
	}

	public void initIndexesFromDB() throws ServiceException {
		try {
			initFromDB();
		} catch(IndexException e) {
			logger.error("Fail to init article or attaches indexs from Database.", e);
			throw new ServiceException(SystemCode.INNER_ERROR.entry);
		} catch (Exception e) {
			// TODO@DoneSpeak 处理各种各样的异常
			logger.error("Fail to list article or attaches indexs from Database.", e);
			throw new ServiceException(SystemCode.DATABASE_EXCEPTION.entry);
		}
	}
	
	public void initFromDB() throws IndexException, Exception {
		// TODO@DoneSpeak 抛出各种各样的异常
		// 重数据库中获取所有的 Article, attach。如果数据集合太大，可以考虑按照id范围分批次获取
		// TODO@DoneSpeak 利用多线程可以加快初始化的进度
		long articleSum = articleMapper.countAll();
		long attachSum = attachMapper.countAll();
		final int pageSize = 1000;
		for(int start = 0; start < articleSum; start += pageSize) {
			List<Article> articles = articleMapper.listFullArticlePage(start, pageSize);
			indexArticlesToSearcher(articles);
		}
		for(int start = 0; start < attachSum; start += pageSize) {
			List<Attach> attaches = attachMapper.listFullAttachPage(start, pageSize);
			indexAttachesToSearcher(attaches);
		}		
	}

	/**
	 * 每天的6点会开始，每隔半小时运行一次，一直运行到第二天的凌晨2点
	 * reference: 
	 * [SpringMVC定时任务注解实现@Schedule](https://blog.csdn.net/xn_28/article/details/70666333)
	 * [spring schedule定时任务（一）：注解的方式](https://blog.csdn.net/tuzongxun/article/details/51576301)
	 */
	@Override
	public void collectDate() {
		logger.info("Init collectDate schedule");
		// 延迟一小时
		long initialDelay = 1 * 60 * 60 * 1000;
		// 每次间隔 30 分钟
		long interval = 30 * 60 * 1000;
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleWithFixedDelay(  
			crawlSheduleTask(),  
			initialDelay,
			interval,
			TimeUnit.MILLISECONDS);
	}
	
	private Runnable crawlSheduleTask() {
		return new Runnable() {
			@Override
			public void run() {
				crawlNewContents();
			}
		};
	}
	
	private void crawlNewContents() {
		if(!dataInitialized) {
			return;
		}
		List<ArticleListItem> articleItems = null;
		logger.info("scheduled task for crawl new articles");
		try {
			// 从公文通列表页面获取一个月内发布的文章 -> 根据数据库中最大id划分最新发布文章和旧的文章
			ArticleListCrawler listCrawler = new ArticleListCrawler();
			articleItems = listCrawler.listArticleItems(BoardSearchOption.defaultSearch(DayyOption.THREE_DAIES));
		} catch (IOException e) {
			logger.error("Fail to crawl article list items.", e);
			// TODO@guanrong.yang: info me
			return;
		}
		List<ArticleListItem> newArticleItems = new ArrayList<ArticleListItem>();
		List<ArticleListItem> oldArticleItems = new ArrayList<ArticleListItem>();
		
		try {			
			divideToNewAndOld(articleItems, newArticleItems, oldArticleItems);
			// 写入最新发布文章到数据库中，并构建倒排索引
			addNewArticles(newArticleItems);
			
			infoUserForFavorite(newArticleItems);
			// 判断旧的文章是否有更新 -> 如果有更新，则更新相关的文章内容和附件内容
			// TODO@Donespeak 更新未更新的旧的文章的阅读次数
//			updateArticles(oldArticleItems);
		} catch (AccessDatabaseException e) {
			// TODO Auto-generated catch block
			logger.error("Fail to Insert articles or pictures or attaches data into database");
		} catch (ServiceException e) {
			logger.error("fail to info user for new article", e);
		}
	}

	private void divideToNewAndOld(List<ArticleListItem> articleItems, List<ArticleListItem> newArticleItems,
			List<ArticleListItem> oldArticleItems) throws AccessDatabaseException {
		// 获取数据库中存在的 id 最大的文章
		Long maxArticleId = null;
		try {
			maxArticleId = articleMapper.getMaxId();
			maxArticleId = maxArticleId == null? -1: maxArticleId;
		} catch (Exception e) {
			logger.error("Fail to get the max article id.", e);
			throw new AccessDatabaseException("Fail to get the max article id.");
		}
		// 大于 已有最大文章id的为新文章，否则为旧文章
		for(ArticleListItem item: articleItems) {
			if(item.getArticleId() > maxArticleId) {
				newArticleItems.add(item);
			} else {
				oldArticleItems.add(item);
			}
		}
	}
	
	private void addNewArticles(List<ArticleListItem> newArticleItems) throws AccessDatabaseException {
		List<Article> articles = new ArrayList<Article>();
		List<Picture> pictures = new ArrayList<Picture>();
		List<Attach> attaches = new ArrayList<Attach>();
		final int MAX_ARTICLE_BATCH_SIZE = 100;
		
		for(ArticleListItem item: newArticleItems) {
			// 尝试使用多线程/线程池
			try {
				ArticleCrawler crawler = new ArticleCrawler(item);
				FullArticle fullArticle = crawler.extractFullArticle();
				if(fullArticle == null) {
					continue;
				}
				articles.add(fullArticle.getArticle());
				if(fullArticle.getPictures() != null) {
					pictures.addAll(fullArticle.getPictures());
				}
				if(fullArticle.getAttaches() != null) {
					attaches.addAll(fullArticle.getAttaches());
				}
				if(articles.size() >= MAX_ARTICLE_BATCH_SIZE) {
					saveAndIndex(articles, attaches, pictures);
					articles = new ArrayList<Article>();
					pictures = new ArrayList<Picture>();
					attaches = new ArrayList<Attach>();
				}
			} catch (IOException | ParseException | WrongPageLayoutException | IndexException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if(articles.size() > 0) {
			try {
				saveAndIndex(articles, attaches, pictures);
			} catch (IndexException | IOException e) {
				logger.error(e.getMessage(), e);
				return;
			}
		}
	}
	
	private void saveAndIndex(List<Article> articles, List<Attach> attaches, List<Picture> pictures) 
			throws AccessDatabaseException, IndexException, IOException {
		try {
			insertArticlesToDB(articles);
			 insertPicturesToDB(pictures);
			insertAttachesToDB(attaches);
		} catch (Exception e) {
			logger.error("Fail to insert articles or pictures or attaches into database.", e);
			throw new AccessDatabaseException(e.getMessage());
		}
		indexArticlesToSearcher(articles);
		indexAttachesToSearcher(attaches);
		ContentSearcher.refreshManager(false);
	}
	
	private void insertArticlesToDB(List<Article> articles) throws Exception {
		if(articles == null || articles.size() == 0) {
			return;
		}
		insertArticlesBatch(articles);
		insertArticleContent(articles);
		
	}

	private void insertArticlesBatch(List<Article> articles) throws Exception {
		if(articles == null || articles.size() == 0) {
			return;
		}
		int batchSize = 50;
		for(int start = 0; start < articles.size(); start += batchSize) {
			int end = start + batchSize > articles.size()? articles.size(): start + batchSize;
			List<Article> subArticles = articles.subList(start, end);
			articleMapper.insertBatch(subArticles);
		}
	}
	
	private void insertArticleContent(List<Article> articles) throws AccessDatabaseException {
		final int MAX_INSERT_BATCH_SIZE = 10;
		List<IdValue> contents = new ArrayList<IdValue>();
		try {
			for(Article article: articles) {
				IdValue idValue = new IdValue(article.getId(), article.getContent());
				contents.add(idValue);
				if(contents.size() >= MAX_INSERT_BATCH_SIZE) {
					contentMapper.insertArticleBatch(contents);
					contents = new ArrayList<IdValue>();
				}
			}
			if(contents.size() > 0) {
				contentMapper.insertArticleBatch(contents);
			}
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to insert article content", e);
		}
		
	}
	
	private void insertPicturesToDB(List<Picture> pictures) throws Exception {
		insertPicturesBatch(pictures);
	}
	
	private void insertPicturesBatch(List<Picture> pictures) throws Exception {
		if(pictures == null || pictures.size() == 0) {
			return;
		}
		int batchSize = 100;
		for(int start = 0; start < pictures.size(); start += batchSize) {
			int end = start + batchSize > pictures.size()? pictures.size(): start + batchSize;
			List<Picture> subPictures = pictures.subList(start, end);
			pictureMapper.insertBatch(subPictures);
		}
	}
	
	@SuppressWarnings("unused")
	private void insertPictureOneByOne(List<Picture> pictures) {
		if(pictures == null || pictures.size() == 0) {
			return;
		}
		for(Picture picture: pictures) {
			try {
				pictureMapper.insert(picture);
			} catch (Exception e) {
				logger.error("Fail to insert picture " + picture.getUrl(), e);
			}
		}
	}
	
	private void insertAttachesToDB(List<Attach> attaches) throws AccessDatabaseException {
		if(attaches == null || attaches.size() == 0) {
			return;
		}
		insertAttachesOneByOne(attaches);
		insertAttachContent(attaches);
	}

	private void insertAttachesOneByOne(List<Attach> attaches) throws AccessDatabaseException {
		if(attaches == null || attaches.size() == 0) {
			return;
		}
		try {
			for(Attach attach: attaches) {
				attachMapper.insert(attach);
			}
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to insert attach batch", e);
		}
	}
	
	@SuppressWarnings("unused")
	private void insertAttachesBatch(List<Attach> attaches) throws AccessDatabaseException {
		if(attaches == null || attaches.size() == 0) {
			return;
		}
		int batchSize = 1000;
		try {
			for(int start = 0; start < attaches.size(); start += batchSize) {
				int end = start + batchSize > attaches.size()? attaches.size(): start + batchSize;
				List<Attach> subAttaches = attaches.subList(start, end);
				attachMapper.insertBatch(subAttaches);
			}
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to insert attach batch", e);
		}
	}

	private void insertAttachContent(List<Attach> attaches) throws AccessDatabaseException {
		final int MAX_INSERT_BATCH_SIZE = 10;
		List<IdValue> contents = new ArrayList<IdValue>();
		try {
			for(Attach attach: attaches) {
				IdValue idValue = new IdValue(attach.getId(), attach.getContent() == null? "": attach.getContent());
				if(attach.getId() == null || attach.getId() == 0) {
					logger.error("attach done's have a id");
				}
				contents.add(idValue);
				if(contents.size() >= MAX_INSERT_BATCH_SIZE) {
					contentMapper.insertAttachBatch(contents);
					contents = new ArrayList<IdValue>();
				}
			}
			if(contents.size() > 0) {
				contentMapper.insertAttachBatch(contents);
			}
		} catch (Exception e) {
			throw new AccessDatabaseException("Fail to insert attach content batch", e);
		}
		
	}
	
	private void indexArticlesToSearcher(List<Article> articles) throws IndexException {
		 try {
			 ContentIndexer indexConstructor = new ContentIndexer();
			 indexConstructor.addArticles(articles);
			 indexConstructor.commit();
			 indexConstructor.close();
			 logger.info("index " + articles.size() + " articles");
		} catch (IOException e) {
			throw new IndexException("Fail to index articles" ,e);
		}
	}
	
	private void indexAttachesToSearcher(List<Attach> attaches) throws IndexException {
		 try {
			 ContentIndexer indexConstructor = new ContentIndexer();
			 indexConstructor.addAttaches(attaches);
			 indexConstructor.commit();
			 indexConstructor.close();
			 logger.info("index " + attaches.size() + " attaches");
		} catch (IOException e) {
			throw new IndexException("Fail to index attaches" ,e);
		}
	}
	
	private List<Long> updateArticles(List<ArticleListItem> oldArticleItems) {
		// 分开为完全更新和仅更新点击次数
		long maxArticleId = Long.MIN_VALUE;
		long minArticleId = Long.MAX_VALUE;
		
		for(ArticleListItem articleItem: oldArticleItems) {
			long articleId = articleItem.getArticleId();
			if(articleId > maxArticleId) {
				maxArticleId = articleId;
			} else if(articleId < minArticleId) {
				minArticleId = articleId;
			}
		}
		Set<String> articleIdUpdatedTimes = null;
		try {
			articleIdUpdatedTimes = articleMapper.listArticleIdUpdatedTime(minArticleId, maxArticleId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<FullArticle> fullArticles = new ArrayList<FullArticle>();
		for(ArticleListItem item: oldArticleItems) {
			try {
				ArticleCrawler crawler = new ArticleCrawler(item);
				long updatedTime = crawler.extractUpdatedTime();
				if(articleIdUpdatedTimes.contains(item.getArticleId() + "#" + updatedTime)) {
					// TODO@DoneSpeak 更新点击次数
				} else {
					// TODO@DoneSpeak 更新整篇
					FullArticle fullArticle = crawler.extractFullArticle();
					if(fullArticle == null) {
						continue;
					}
					fullArticles.add(fullArticle);
				}
			} catch (IOException | WrongPageLayoutException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	private void infoUserForFavorite(List<ArticleListItem> newArticleItems) 
			throws AccessDatabaseException, ServiceException {
		// 分开成文章类别映射文章列表 -> categoryId#publisherId-title & url
		Map<FavoriteSource, List<ArticleListItem>> favoriteMap = new HashMap<FavoriteSource, List<ArticleListItem>>();
		for(ArticleListItem item: newArticleItems) {
			FavoriteSource fav = new FavoriteSource();
			fav.setCategoryId(item.getCategory().getId());
			fav.setPublisherId(item.getPublisher().getId());
			
			if(favoriteMap.containsKey(fav)) {
				favoriteMap.get(fav).add(item);
			} else {
				List<ArticleListItem> articles = new ArrayList<ArticleListItem>();
				articles.add(item);
				favoriteMap.put(fav, articles);
			}
		}
		Map<IdValue, Map<FavoriteSource, List<ArticleListItem>>> userFavoriteArticle = 
				new HashMap<IdValue, Map<FavoriteSource, List<ArticleListItem>>>();
		// 找到相关的用户
		for(FavoriteSource fav: favoriteMap.keySet()) {
			List<IdValue> users;
			try {
				users = favoriteSourceMapper.listUserOfFavoriteSrouce(fav.getCategoryId(), fav.getPublisherId());
			} catch (Exception e) {
				throw new AccessDatabaseException(e);
			}
			for(IdValue idMail: users) {
				if(userFavoriteArticle.containsKey(idMail)) {
					userFavoriteArticle.get(idMail).put(fav, favoriteMap.get(fav));
				} else {
					Map<FavoriteSource, List<ArticleListItem>> favItem = new HashMap<FavoriteSource, List<ArticleListItem>>();
					favItem.put(fav, favoriteMap.get(fav));	
					
					userFavoriteArticle.put(idMail, favItem);
				}
			}
		}
		// 发送通知邮件
		for(Entry<IdValue, Map<FavoriteSource, List<ArticleListItem>>> entry: userFavoriteArticle.entrySet()) {
			notificationService.pushFavoriteContents(entry.getKey().getId(), entry.getKey().getValue(), entry.getValue());
		}
	}
	
	public static void main(String[] args) throws IOException {
		BoardSearchOption option = BoardSearchOption.defaultSearch(DayyOption.TEN_YEAR);
		option.keyword = "IntegratingMolecularRigidityandSoftMatterCharacted";
		ArticleListCrawler listCrawler = new ArticleListCrawler();
		List<ArticleListItem> articleItems = listCrawler.listArticleItems(option);
		System.out.println(articleItems);
	}
}
