package cn.donespeak.szuboardse.service.impl;

import org.springframework.stereotype.Service;

import cn.donespeak.szuboardse.service.IndexService;

@Service("indexService")
public class IndexServiceImpl implements IndexService{

	@Override
	public int calculate(int a, int b) {
		return a + b;
	}

}
