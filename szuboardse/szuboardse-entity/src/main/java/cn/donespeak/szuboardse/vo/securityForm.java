package cn.donespeak.szuboardse.vo;

public class securityForm implements View{
	private String email;
	private String password;
	private String newPassword;
	
	/* getter and setter */
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	@Override
	public String toString() {
		return "securityForm [email=" + email + ", password=" + password + ", newPassword=" + newPassword + "]";
	}
}
