package cn.donespeak.szuboardse.dto;

import cn.donespeak.szuboardse.enums.TimeDurationOption;

public class SearchOption {
	
	public static enum Region {
		ALL(0),
		ARTICLE(1),
		ATTACH(2)
		;
		
		private final int code;
		Region(int code) {
			this.code = code;
		}
		public static Region getEnum(int code) {
			for(Region region: Region.values()) {
				if(code == region.code) {
					return region;
				}
			}
			return null;
		}
		public static Region getEnum(String value) {
			try {
				return Region.valueOf(value);
			} catch (Exception e) {
				return null;
			}
		}
		public int code() {
			return code;
		}
	}
	
	private String query;
	private Region region;
	private Integer category;
	private Integer publisher;
	// 自定义的检索时间区间，如果durationStart 和 durationEnd 均有值，则忽略 duration
	private Long durationStart;
	private Long durationEnd;
	
	// 用于翻页使用
	private Integer pageIndex;
	
	private static final int LIMITED_LENGTH = 100;

	public void setDuration(long[] duration) {
		durationStart = duration[0];
		durationEnd = duration[1];
	}
	public SearchOption trimQuery() {
		query = query == null? "": query.trim();
		return this;
	}
	public boolean searchRegion() {
		return region != null && region != Region.ALL;
	}
	public boolean searchCategory() {
		return category != null;
	}
	public boolean searchPublisher() {
		return publisher != null;
	}
	public boolean searchDuration() {
		return durationStart != null && durationEnd != null && durationStart < durationEnd;
	}
	

	public void setDuration(Long durationStart, Long durationEnd, String duration) {
		if(durationStart != null || durationEnd != null) {
			this.durationStart = durationStart;
			this.durationEnd = durationEnd;
			return;
		}
		// durationStart 空，durationEnd 空
		if(duration != null) {
			TimeDurationOption durationOption = TimeDurationOption.getOption(duration);
			if(durationOption == null) {
				// 不是预设时间区间
				this.durationStart = null;
				this.durationEnd = null;
				return;
			}
			this.setDuration(durationOption.toDuration());
		}
		this.durationStart = null;
		this.durationEnd = null;
	}
	
	public void setDuration(Long durationStart, Long durationEnd) {
		this.durationStart = durationStart;
		this.durationEnd = durationEnd;
	}
	
	/* getter and setter */
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public void setLimitQuery(String query) {
		if(query != null) {
			query = query.length() <= LIMITED_LENGTH? query: query.substring(0, LIMITED_LENGTH);
		}
		this.query = query;
	}
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	public void setRegion(String region) {
		if(region == null) {
			// 默认是检索所有
			this.region = Region.ALL;
		} else {
			Region targetRegion = Region.getEnum(region.toUpperCase());
			this.region = targetRegion == null? Region.ALL: targetRegion;		
		}
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public Integer getPublisher() {
		return publisher;
	}
	public void setPublisher(Integer publisher) {
		this.publisher = publisher;
	}
	public Long getDurationStart() {
		return durationStart;
	}
	public void setDurationStart(Long durationStart) {
		this.durationStart = durationStart;
	}
	public Long getDurationEnd() {
		return durationEnd;
	}
	public void setDurationEnd(Long durationEnd) {
		this.durationEnd = durationEnd;
	}
	public Integer getPageIndex() {
		return pageIndex == null? 0: pageIndex;
	}
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex == null? 1: pageIndex;
	}
	
	@Override
	public String toString() {
		return "SearchOption [query=" + query + ", region=" + region + ", category=" + category + ", pubisher="
				+ publisher + ", durationStart=" + durationStart + ", durationEnd=" + durationEnd + ", pageIndex="
				+ pageIndex + "]";
	}
}
