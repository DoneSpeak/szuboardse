package cn.donespeak.szuboardse.vo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResultPage implements View{
	private List<ResultItem> resultItems;
	private Pagination pageInfo;
	private long timecost;
	private long totalhits;
	
	public ResultPage(){}
	
	public ResultPage(List<ResultItem> resultItems, Pagination pageInfo) {
		this.resultItems = resultItems;
		this.pageInfo = pageInfo;
	}
	
	@JsonIgnore
	public static ResultPage getEmptyResultPage() {
		ResultPage resultPage = new ResultPage();
		resultPage.setResultItems(new ArrayList<ResultItem>());
		resultPage.setPageInfo(Pagination.getEmptyPagination());
		resultPage.setTimecost(0);
		resultPage.setTotalhits(0);
		return resultPage;
	}
	
	/*getter and setter*/
	public List<ResultItem> getResultItems() {
		return resultItems;
	}
	public void setResultItems(List<ResultItem> resultItems) {
		this.resultItems = resultItems;
	}
	public Pagination getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(Pagination pageInfo) {
		this.pageInfo = pageInfo;
	}
	public long getTimecost() {
		return timecost;
	}
	public void setTimecost(long timecost) {
		this.timecost = timecost;
	}
	public long getTotalhits() {
		return totalhits;
	}
	public void setTotalhits(long totalhits) {
		this.totalhits = totalhits;
	}
}
