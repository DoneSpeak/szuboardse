package cn.donespeak.szuboardse.vo;

public class TinyAttachItem implements View{
	private long id;
	private String filename;
	private String url;
	
	private long articleId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	@Override
	public String toString() {
		return "TinyAttachItem [id=" + id + ", filename=" + filename + ", url=" + url + ", articleId=" + articleId
				+ "]";
	}
}
