package cn.donespeak.szuboardse.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Picture;
import cn.donespeak.szuboardse.entity.Publisher;

public class ResultItem implements View{
	private Region region;

	// 如果是article, 则表示为该article的id，如果是attache, 则表示该attach所在的article的id
	private Long articleId;
	private Long attachId;
	
	private String title;
	// 自身的url
	private String url;
	private Category category;
	private Publisher publisher;
	private long publishedTime;
	private long updatedTime;
	private String digest;
	
	private boolean isfavorite;
	
	// 附件所在的文章的url
	private String articleUrl;
	
	// used for article
	private List<Picture> pictures;
	// used for article
	private List<TinyAttachItem> attaches;
	
	private static final int ABSTRACT_MAX_LENGHT = 140;
	
	public ResultItem() {}
	
	public static ResultItem getAttachResultImte() {
		ResultItem item = new ResultItem();
		item.setRegion(Region.ATTACH);
		return item;
	}
	
	public static ResultItem getArticleResultImte() {
		ResultItem item = new ResultItem();
		item.setRegion(Region.ARTICLE);
		return item;
	}
	
	// 得到该 项 的id
	public long getId() {
		if(region == Region.ARTICLE) {
			return articleId;
		} else {
			return attachId;
		}
	}
	/* getter and setter */
	@JsonIgnore
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	public String getType() {
		return region == null? null: region.name();
	}
	public Long getArticleId() {
		return articleId;
	}
	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}
	public Long getAttachId() {
		return attachId;
	}
	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	public long getPublishedTime() {
		return publishedTime;
	}
	public void setPublishedTime(long publishedTime) {
		this.publishedTime = publishedTime;
	}
	public long getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public void setLimitedDigest(String digest) {
		if(digest != null && digest.length() > ABSTRACT_MAX_LENGHT ) {
			this.digest = digest.substring(0, ABSTRACT_MAX_LENGHT);
		}
		this.digest = digest;
	}
	public boolean isIsfavorite() {
		return isfavorite;
	}
	public void setIsfavorite(boolean isfavorite) {
		this.isfavorite = isfavorite;
	}
	public String getArticleUrl() {
		return articleUrl;
	}
	public void setArticleUrl(String articleUrl) {
		this.articleUrl = articleUrl;
	}
	public List<Picture> getPictures() {
		return pictures;
	}
	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}
	public List<TinyAttachItem> getAttaches() {
		return attaches;
	}
	public void setAttaches(List<TinyAttachItem> attaches) {
		this.attaches = attaches;
	}
	public boolean isIsArticle() {
		return region != null && region == Region.ARTICLE;
	}
	public boolean isIsAttach() {
		return region != null && region == Region.ATTACH;
	}
	@Override
	public String toString() {
		return "ResultItem [region=" + region + ", title=" + title + ", url=" + url + ", category=" + category
				+ ", publisher=" + publisher + ", publishedTime=" + publishedTime + ", updatedTime=" + updatedTime
				+ ", abstact=" + digest + ", isfavorite=" + isfavorite + ", srcUrl=" + articleUrl + ", pictures="
				+ pictures + ", attaches=" + attaches + "]";
	}
}
