package cn.donespeak.szuboardse.dto;

import java.util.List;

import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Picture;
import cn.donespeak.szuboardse.entity.Publisher;

public class FullArticle {
	private Article article;
	private List<Picture> pictures;
	private List<Attach> attaches;
	
	public FullArticle(){}
	
	public FullArticle(Article article, List<Picture> pictures, List<Attach> attaches){
		this.article = article;
		this.pictures = pictures;
		this.attaches = attaches;
	}

	public void setPublisher(Publisher publisher) {
		if(publisher == null || article == null) {
			return;
		}
		article.setPublisherId(publisher.getId());
		if(attaches == null) {
			return;
		}
		for(Attach attach: attaches) {
			attach.setPublisherId(publisher.getId());
		}
	}
	
	public void setCategory(Category category) {
		if(category == null || article == null) {
			return;
		}
		article.setCategoryId(category.getId());
		if(attaches == null) {
			return;
		}
		for(Attach attach: attaches) {
			attach.setCategoryId(category.getId());
		}
	}
	
	/* getter and setter */
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public List<Picture> getPictures() {
		return pictures;
	}
	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}
	public List<Attach> getAttaches() {
		return attaches;
	}
	public void setAttaches(List<Attach> attaches) {
		this.attaches = attaches;
	}

	public void copy(FullArticle fullArticle) {
		this.article = fullArticle.getArticle();
		this.pictures = fullArticle.getPictures();
		this.attaches = fullArticle.getAttaches();
	}

	@Override
	public String toString() {
		return "FullArticle [article=" + article + ",\n pictures=" + pictures + ",\n attaches=" + attaches + "]";
	}	
}
