package cn.donespeak.szuboardse.entity;

public class FavoriteContent {
	private long id;
	private long uid;
	private Long articleId;
	private Long attachId;
	private long gmtCreated;
	
	/* getter and setter */
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public Long getArticleId() {
		return articleId;
	}
	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}
	public Long getAttachId() {
		return attachId;
	}
	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}
	public long getGmtCreated() {
		return gmtCreated;
	}
	public void setGmtCreated(long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	@Override
	public String toString() {
		return "FavoriteContent [id=" + id + ", uid=" + uid + ", articleId=" + articleId + ", attachId=" + attachId
				+ ", gmtCreated=" + gmtCreated + "]";
	}
}
