package cn.donespeak.szuboardse.dto;

public class ArticleClickCounter {
	private long articleId;
	private int baseClick;
	private int additionClick;
	
	/* getter and setter */
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	public int getBaseClick() {
		return baseClick;
	}
	public void setBaseClick(int baseClick) {
		this.baseClick = baseClick;
	}
	public int getAdditionClick() {
		return additionClick;
	}
	public void setAdditionClick(int additionClick) {
		this.additionClick = additionClick;
	}
	
	/* toString */
	@Override
	public String toString() {
		return "ArticleClickCounter [articleId=" + articleId + ", baseClick=" + baseClick + ", additionClick="
				+ additionClick + "]";
	}	
}
