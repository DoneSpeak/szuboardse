package cn.donespeak.szuboardse.dto;

import cn.donespeak.szuboardse.entity.User;

public class TokenUser {
	private User user;
	private String token;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public String toString() {
		return "TokenUser [user=" + user + ", token=" + token + "]";
	}
	
}
