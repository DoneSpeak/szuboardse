package cn.donespeak.szuboardse.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Picture {
	private Long id;
	// the id of the article that this image belongs to
	private long articleId;
	// the path of the image in the local storage
	private String path;
	// the original path in szu server
	private String url;
	private long gmtCreated;
	
	/* getter and setter */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@JsonIgnore
	public long getGmtCreated() {
		return gmtCreated;
	}
	public void setGmtCreated(long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	@Override
	public String toString() {
		return "Picture [id=" + id + ", articleId=" + articleId + ", path=" + path + ", url=" + url + ", gmtCreated="
				+ gmtCreated + "]";
	}
}
