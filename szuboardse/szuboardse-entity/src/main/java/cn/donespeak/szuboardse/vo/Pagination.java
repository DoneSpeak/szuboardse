package cn.donespeak.szuboardse.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Pagination implements View{
	private int pageIndex = 0;
	private int pageSize = 0;
	private int pageNum = 0;
	private int total = 0;
	
	public Pagination() {}
	
	public Pagination(int index,int size,int num,int total) {
		this.pageIndex = index;
		this.pageSize = size;
		this.pageNum = num;
		this.total = total;
	}
	
	@JsonIgnore
	public static Pagination getEmptyPagination() {
		return new Pagination(0, 0, 0, 0);
	}
	
	public static int calculateStartIndex(int pageIndex, int pageSize) {
		return (pageIndex - 1) * pageSize;
	}
	
	public static int countPageNum(int pageSize, int total) {
		return (total + pageSize - 1) / pageSize;
	}
	
		
	/*getter and setter*/
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "Pagination [pageIndex=" + pageIndex + ", pageSize=" + pageSize + ", pageNum=" + pageNum + ", total="
				+ total + "]";
	}
	public static void main(String[] args) {
		System.out.println(new Pagination());
	}
}
