package cn.donespeak.szuboardse.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Article {
	private Long id;
	private String url;
	private String title;
	private String digest;
	// 该参数不属于article对应的数据库表
	private String content;
	private int categoryId;
	private int publisherId;
	private long publishedTime;
	private long updatedTime;
	private int baseClick;
	private int additionClick;
	private long gmtCreated;
	private long gmtModified;
	
	/* getting and setting */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}
	public long getPublishedTime() {
		return publishedTime;
	}
	public void setPublishedTime(long publishedTime) {
		this.publishedTime = publishedTime;
	}
	public long getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	public int getBaseClick() {
		return baseClick;
	}
	public void setBaseClick(int baseClick) {
		this.baseClick = baseClick;
	}
	public int getAdditionClick() {
		return additionClick;
	}
	public void setAdditionClick(int additionClick) {
		this.additionClick = additionClick;
	}
	@JsonIgnore
	public long getGmtCreated() {
		return gmtCreated;
	}
	public void setGmtCreated(long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	@JsonIgnore
	public long getGmtModified() {
		return gmtModified;
	}
	public void setGmtModified(long gmtModified) {
		this.gmtModified = gmtModified;
	}
	
	@Override
	public String toString() {
		return "Article [id=" + id + ", url=" + url + ", title=" + title + ", content=" + content + ", categoryId="
				+ categoryId + ", publisherId=" + publisherId + ", publishedTime=" + publishedTime + ", updatedTime="
				+ updatedTime + ", baseClick=" + baseClick + ", additionClick=" + additionClick + ", gmtCreated="
				+ gmtCreated + ", gmtModified=" + gmtModified + "]";
	}
}
