package cn.donespeak.szuboardse.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Attach {
	private Long id;
	private String url;
	private String filename;
	private String digest;
	private String content;
	private long articleId;
	private int categoryId;
	private int publisherId;
	private long publishedTime;
	private long updatedTime;
	private int baseClick;
	private int additionClick;
	private long gmtCreated;
	private long gmtModified;
	
	private String articleUrl;
	
	public void inheritFromArticle(Article article) {
		this.articleId = article.getId();
		this.categoryId = article.getCategoryId();
		this.publisherId = article.getPublisherId();
		this.publishedTime = article.getPublishedTime();
		this.updatedTime = article.getUpdatedTime();
		this.additionClick = article.getAdditionClick();
		this.gmtCreated = article.getGmtCreated();
		this.gmtModified = article.getGmtModified();
	}
	
	/* getter and setter */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}
	public long getPublishedTime() {
		return publishedTime;
	}
	public void setPublishedTime(long publishedTime) {
		this.publishedTime = publishedTime;
	}
	public long getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	public int getBaseClick() {
		return baseClick;
	}
	public void setBaseClick(int baseClick) {
		this.baseClick = baseClick;
	}
	public int getAdditionClick() {
		return additionClick;
	}
	public void setAdditionClick(int additionClick) {
		this.additionClick = additionClick;
	}
	@JsonIgnore
	public long getGmtCreated() {
		return gmtCreated;
	}
	public void setGmtCreated(long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	@JsonIgnore
	public long getGmtModified() {
		return gmtModified;
	}
	public void setGmtModified(long gmtModified) {
		this.gmtModified = gmtModified;
	}
	public String getArticleUrl() {
		return articleUrl;
	}
	public void setArticleUrl(String articleUrl) {
		this.articleUrl = articleUrl;
	}
	
	@Override
	public String toString() {
		return "Attach [id=" + id + ", url=" + url + ", filename=" + filename + ", content=" + content + ", articleId="
				+ articleId + ", categoryId=" + categoryId + ", publisherId=" + publisherId + ", publishedTime="
				+ publishedTime + ", updatedTime=" + updatedTime + ", baseClick=" + baseClick + ", additionClick="
				+ additionClick + ", gmtCreated=" + gmtCreated + ", gmtModifyed=" + gmtModified + "]";
	}
}
