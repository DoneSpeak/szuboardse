package cn.donespeak.szuboardse.dto;

import java.util.ArrayList;
import java.util.List;

public class ID {
	private Long id;
	private List<Long> ids;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Long> getIds() {
		return ids;
	}
	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	public List<Long> all() {
		List<Long> allIds = new ArrayList<Long>();
		if(ids == null) {
			if(id != null) {		
				allIds.add(id);
			}
		} else {
			allIds.addAll(ids);
			if(id != null) {
				allIds.add(id);
			}
		}
		return allIds;
	}
	@Override
	public String toString() {
		return "ID [id=" + id + ", ids=" + ids + "]";
	}
}
