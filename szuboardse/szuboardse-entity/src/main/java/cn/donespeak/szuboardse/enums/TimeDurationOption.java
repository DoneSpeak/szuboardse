package cn.donespeak.szuboardse.enums;

import java.util.Calendar;
import java.util.Date;

public enum TimeDurationOption {
	ONE_DAIES("1d"),
	THREE_DAIES("3d"),
	ONE_WEEK("1w"),
	HALF_MONTH("hm"),
	ONE_MONTH("1m"),
	THREE_MONTHS("3m"),
	HALF_YEARS("hy"),
	ONE_YEAR("1y"),
	TWO_YEARS("2y"),
	THREE_YEARS("3y"),
	FOUR_YEARS("4y");
	
	private final String value;
	
	TimeDurationOption(String value) {
		this.value = value;
	}
	
	public String value() {
		return value;
	}
	
	public static TimeDurationOption getOption(String value) {
		if(value == null) {
			return null;
		}
		for(TimeDurationOption option: TimeDurationOption.values()) {
			if(option.value().equals(value)) {
				return option;
			}
		}
		return null;
	}
	
	public long[] toDuration() {
		Date now = new Date();
		long end = now.getTime();
		long start = -1;
		switch(this) {
		case ONE_DAIES: { start = addNDaies(now, -1).getTime(); break; }
		case THREE_DAIES: { start = addNDaies(now, -3).getTime(); break; }
		case ONE_WEEK: { start = addNWeeks(now, -1).getTime(); break; }
		case HALF_MONTH: { start = addNDaies(now, -15).getTime(); break; }
		case ONE_MONTH: { start = addNMonths(now, -1).getTime(); break; }
		case THREE_MONTHS: { start = addNMonths(now, -3).getTime(); break; }
		case HALF_YEARS:{ start = addNMonths(now, -6).getTime(); break; }
		case ONE_YEAR: { start = addNYears(now, -1).getTime(); break; }
		case TWO_YEARS: { start = addNYears(now, -2).getTime(); break; }
		case THREE_YEARS: { start = addNYears(now, -3).getTime(); break; }
		case FOUR_YEARS: { start = addNYears(now, -4).getTime(); break; }
		default: { return null; }
		}
		return new long[]{start, end};
	}
	
	private Date addNDaies(Date date, int n) {
		return addNDate(date, Calendar.DATE, n);
	}
	
	private Date addNWeeks(Date date, int n) {
		return addNDate(date, Calendar.DATE, n * 7);
	}
	
	private Date addNMonths(Date date, int n) {
		return addNDate(date, Calendar.MONTH, n);
	}
	
	private Date addNYears(Date date, int n) {
		return addNDate(date, Calendar.YEAR, n);
	}
	
	private Date addNDate(Date date, int type, int n) {
		Calendar clandar = Calendar.getInstance();
        clandar.setTime(date);
        clandar.add(type, n);
        return clandar.getTime();
	}
}
