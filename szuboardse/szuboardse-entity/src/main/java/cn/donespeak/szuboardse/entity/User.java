package cn.donespeak.szuboardse.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {

	private long uid;
	private String name;
	private String email;
	private String avatar;
	
	private String password;
	private long registrationTime;
	private long gmtCreated;
	
	public final static String DEFAULT_AVATAR = "http://119.29.119.37/files/website/users/avatar.png";
	
	/* getters and setters */
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@JsonIgnore
	public long getRegistrationTime() {
		return registrationTime;
	}
	public void setRegistrationTime(long registrationTime) {
		this.registrationTime = registrationTime;
	}
	@JsonIgnore
	public long getGmtCreated() {
		return gmtCreated;
	}
	public void setGmtCreated(long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	@Override
	public String toString() {
		return "User [uid=" + uid + ", name=" + name + ", email=" + email + ", avatar=" + avatar + ", password="
				+ password + ", registrationTime=" + registrationTime
				+ ", gmtCreated=" + gmtCreated + "]";
	}
	
}
