package cn.donespeak.szuboardse.dto;

public class UserSign {	
	private String email;
	private String password;
	
	/* getter and setter */
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserSign [email=" + email + ", password=" + password + "]";
	}
}
