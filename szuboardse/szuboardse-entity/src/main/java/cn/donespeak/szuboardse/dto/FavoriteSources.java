package cn.donespeak.szuboardse.dto;

import java.util.HashSet;
import java.util.Set;

import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;

public class FavoriteSources {
	private Category category;
	private Set<Publisher> publishers;
	
	public FavoriteSources() {
		publishers = new HashSet<Publisher>();
	}
	
	public FavoriteSources(Category category) {
		this.category = category;
		this.publishers = new HashSet<Publisher>();
	}
		
	public FavoriteSources(Category category, Set<Publisher> publishers) {
		this.category = category;
		this.publishers = publishers;
	}
	
	public void addPublisher(Publisher publisher) {
		publishers.add(publisher);
	}
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Set<Publisher> getPublishers() {
		return publishers;
	}
	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}
}
//
//private int category;
//private Set<Integer> publishers;
//
//public FavoriteSources() {
//	publishers = new HashSet<Integer>();
//}
//
//public FavoriteSources(int categoryId) {
//	this.category = categoryId;
//	this.publishers = new HashSet<Integer>();
//}
//	
//public FavoriteSources(int category, Set<Integer> publishers) {
//	this.category = category;
//	this.publishers = publishers;
//}
//
//public void addPublisher(int publisherId) {
//	publishers.add(publisherId);
//}
//
//public int getCategory() {
//	return category;
//}
//public void setCategory(int category) {
//	this.category = category;
//}
//public Set<Integer> getPublishers() {
//	return publishers;
//}
//public void setPublishers(Set<Integer> publishers) {
//	this.publishers = publishers;
//}
