package cn.donespeak.szuboardse.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FavoriteSource {
	
	private long id;
	private long uid;
	private int categoryId;
	private int publisherId;
	private long gmtCreated;
	
	/* getter and setter */
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@JsonIgnore
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}
	@JsonIgnore
	public long getGmtCreated() {
		return gmtCreated;
	}
	public void setGmtCreated(long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + categoryId;
		result = prime * result + (int) (gmtCreated ^ (gmtCreated >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + publisherId;
		result = prime * result + (int) (uid ^ (uid >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FavoriteSource other = (FavoriteSource) obj;
		if (categoryId != other.categoryId)
			return false;
		if (gmtCreated != other.gmtCreated)
			return false;
		if (id != other.id)
			return false;
		if (publisherId != other.publisherId)
			return false;
		if (uid != other.uid)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "FavoriteSource [id=" + id + ", uid=" + uid + ", categoryId=" + categoryId + ", publisherId="
				+ publisherId + ", gmtCreated=" + gmtCreated + "]";
	}
}
