package cn.donespeak.szuboardse.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.service.ArticleService;
import cn.donespeak.szuboardse.service.IndexService;
import cn.donespeak.szuboardse.service.InitializationService;
import cn.donespeak.szuboardse.service.UserService;

@Controller
@RequestMapping("/")
public class IndexController {
	
	@Inject
	InitializationService initializationService;
	
	@Inject
	IndexService indexService;
	
	@Inject
	UserService userService;
	
	@Inject
	ArticleService articleService;
	
	@RequestMapping(value = "test", method = RequestMethod.GET)
	@ResponseBody
	public ResponseMapJson collectData() {
		ResponseMapJson json = new ResponseMapJson();
		return json;
	}
}
