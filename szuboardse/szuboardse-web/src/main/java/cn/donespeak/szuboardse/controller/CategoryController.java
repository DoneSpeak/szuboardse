package cn.donespeak.szuboardse.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.base.ResponseJson;
import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.CategoryService;

@Controller
@RequestMapping("/categories")
public class CategoryController {
	
	@Inject
	CategoryService categoryService; 
	
	@RequestMapping(value="", method=RequestMethod.GET)
	@ResponseBody
	public ResponseJson listCategories(){
		ResponseMapJson json = new ResponseMapJson();
		try {
			List<Category> categories = categoryService.listCategoriesFromCache();
			json.push("categories", categories);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}		
		return json;
	}
}
