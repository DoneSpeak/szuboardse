package cn.donespeak.szuboardse.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.base.ResponseJson;
import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.SearchService;
import cn.donespeak.szuboardse.vo.ResultItem;

@Controller
@RequestMapping("/attaches")
public class AttachController {
	
	@Inject
	SearchService searchService;
	
	@RequestMapping(value = "/similarity", method = RequestMethod.GET)
	@ResponseBody
	public ResponseJson similarity(long attachId) {
		ResponseMapJson json = new ResponseMapJson();
		try {
			List<ResultItem> resultItems = searchService.searchSimilarAttaches(attachId);
			json.push("resultItems", resultItems);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/{attachId}/file")
	@ResponseBody
	public ResponseJson download() {
		ResponseMapJson json = new ResponseMapJson();
		// TODO@Donespeak: 通过流的方式获取文件
		return json;
	}
}