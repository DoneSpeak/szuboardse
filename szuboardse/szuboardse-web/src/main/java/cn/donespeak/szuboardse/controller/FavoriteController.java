package cn.donespeak.szuboardse.controller;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.annotation.Authorization;
import cn.donespeak.szuboardse.annotation.CurrentUser;
import cn.donespeak.szuboardse.base.ResponseJson;
import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.base.ResponseObjectJson;
import cn.donespeak.szuboardse.dto.FavoriteSources;
import cn.donespeak.szuboardse.dto.ID;
import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.IdValue;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.ArticleService;
import cn.donespeak.szuboardse.service.AttachService;
import cn.donespeak.szuboardse.service.FavoriteService;
import cn.donespeak.szuboardse.vo.ResultPage;

@Controller
@RequestMapping("/users/{uid}/favorite")
public class FavoriteController {
	
	@Inject
	FavoriteService favoriteService;
	
	@Inject
	AttachService attachService;
	
	/**
	 * 获取收藏的文章和附件
	 * @param userId
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/contents", method=RequestMethod.GET)
	@ResponseBody
	public ResponseJson listFavoriteContents(@CurrentUser long userId, 
			@RequestParam(value="pageIndex", required = true)int pageIndex, 
			@RequestParam(value = "pageSize", required = true)int pageSize){
		ResponseMapJson json = new ResponseMapJson();
//		try {
//			ResultPage page = favoriteService.listFavoriteAttaches();
//		} catch (ServiceException e) {
//			json.setCode(e.getCode());
//			json.setMsg(e.getMessage());
//		}
		return json;
	}
	
	/**
	 * 获取收藏的文章列表
	 * @param userId
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/articles", method=RequestMethod.GET)
	@ResponseBody
	public ResponseJson listFavoriteArticles(@CurrentUser long userId,
			@RequestParam(value="pageIndex", required = true)int pageIndex, 
			@RequestParam(value = "pageSize", required = true)int pageSize){
		ResponseObjectJson json = new ResponseObjectJson();
		try {
			ResultPage resultPage = favoriteService.listFavoriteArticles(userId, pageIndex, pageSize);
			json.setData(resultPage);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	/**
	 * 获取收藏的附件
	 * @param userId
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/attaches", method=RequestMethod.GET)
	@ResponseBody
	public ResponseJson listFavoriteAttaches(@CurrentUser long userId,
			@RequestParam(value="pageIndex", required = true)int pageIndex, 
			@RequestParam(value = "pageSize", required = true)int pageSize){
		ResponseObjectJson json = new ResponseObjectJson();
		try {
			ResultPage resultPage = favoriteService.listFavoriteAttaches(userId, pageIndex, pageSize);
			json.setData(resultPage);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	/**
	 * 收藏一篇文章
	 * @param id
	 * @param userId
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/articles", method=RequestMethod.POST)
	@ResponseBody
	public ResponseJson favoriteArticle(@RequestBody ID id, @CurrentUser long userId){
		ResponseMapJson json = new ResponseMapJson();
		try {
			favoriteService.addFavoriteArticle(userId, id.getId());
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	/**
	 * 取消一篇文章的收藏
	 * @param articleId
	 * @param userId
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/articles/{articleId}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseMapJson cancelFavoriteArticle(@PathVariable long articleId, @CurrentUser long userId){
		ResponseMapJson json = new ResponseMapJson();
		try {
			favoriteService.cancelFavoriteArticle(userId, articleId);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	/**
	 * 收藏一份附件
	 * @param id
	 * @param userId
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/attaches", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMapJson favoriteAttach(@RequestBody ID id, @CurrentUser long userId){
		ResponseMapJson json = new ResponseMapJson();
		try {
			favoriteService.addFavoriteAttach(userId, id.getId());
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	/**
	 * 取消一份附件的收藏
	 * @param attachId
	 * @param userId
	 * @return
	 */
	@Authorization
	@RequestMapping(value = "/attaches/{attachId}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseMapJson cancelFavoriteAttach(@PathVariable long attachId, @CurrentUser long userId){
		ResponseMapJson json = new ResponseMapJson();
		try {
			favoriteService.cancelFavoriteAttach(userId, attachId);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	/**
	 * 关注一个发布来源
	 * @param favoriteSource
	 * @param userId
	 * @return
	 */
	@Authorization
	@RequestMapping(value="/sources", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMapJson addFavoriteSource(@RequestBody FavoriteSource favoriteSource, @CurrentUser long userId) {
		ResponseMapJson json = new ResponseMapJson();
		try {
			long id = favoriteService.addFavoriteSource(userId, 
					favoriteSource.getCategoryId(), favoriteSource.getPublisherId());
			json.push("sourceId", id);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
		
	@Authorization
	@RequestMapping(value="/sources", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseMapJson removeFavoriteSource(@RequestBody FavoriteSource favoriteSource, @CurrentUser long userId) {
		ResponseMapJson json = new ResponseMapJson();
		try {
			favoriteService.removeFavoriteSource(userId, 
					favoriteSource.getCategoryId(), favoriteSource.getPublisherId());
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	
	@Authorization
	@RequestMapping(value="/sources", method=RequestMethod.GET)
	@ResponseBody
	public ResponseMapJson listFavoriteSource(@CurrentUser long userId) {
		ResponseMapJson json = new ResponseMapJson();
		try {
			List<FavoriteSources> sources = favoriteService.listFavoriteSources(userId);
			json.push("sources", sources);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
	
	@Authorization
	@RequestMapping(value="/sources/{favoriteSourceId}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseMapJson deleteFavoriteSource(@PathVariable long favoriteSourceId, @CurrentUser long userId) {
		ResponseMapJson json = new ResponseMapJson();
		try {
			favoriteService.deleteFavorieSource(userId, favoriteSourceId);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
}
