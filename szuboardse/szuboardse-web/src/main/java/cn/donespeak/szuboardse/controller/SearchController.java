package cn.donespeak.szuboardse.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.base.ResponseBaseJson;
import cn.donespeak.szuboardse.base.ResponseJson;
import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.base.ResponseObjectJson;
import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.enums.TimeDurationOption;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.SearchService;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;

@Controller
@RequestMapping("/")
public class SearchController {

	@Inject
	SearchService searchService;
	
	@RequestMapping(value="/search", method=RequestMethod.GET)
	@ResponseBody
	public ResponseJson search(String query, Integer pageIndex,
			String region, Integer category, Integer publisher, String duration, Long durationStart, Long durationEnd) {
		ResponseObjectJson json = new ResponseObjectJson();
		SearchOption searchOption = new SearchOption();
		searchOption.setLimitQuery(query);
		searchOption.setPageIndex(pageIndex);
		searchOption.setRegion(region);		
		searchOption.setCategory(category);
		searchOption.setPublisher(publisher);
		searchOption.setDuration(durationStart, durationEnd, duration);
		
		ResultPage resultPage = null;
		// TODO@DoneSpeak 
		long userId = -1;
		try {
			resultPage = searchService.search(searchOption.trimQuery(), userId);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
			return json;
		}
		json.setData(resultPage);
		return json;
	}
	
	@RequestMapping(value="/query/relative", method=RequestMethod.GET)
	@ResponseBody
	public ResponseJson relativeQuery(@RequestParam("query") String query, 
			@RequestParam(value = "top", required=true) int top) {
		ResponseMapJson json = new ResponseMapJson();
		List<String> queries = null;
		try {
			queries = searchService.relativeQueries(query, top);
			json.push("queries", queries);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
			return json;
		}
		return json;
	}
}
