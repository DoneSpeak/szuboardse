package cn.donespeak.szuboardse.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.donespeak.szuboardse.annotation.Authorization;
import cn.donespeak.szuboardse.util.JwtUtil;
import io.jsonwebtoken.Claims;

public class AuthorizationInterceptor extends HandlerInterceptorAdapter{
	Logger logger = Logger.getLogger(AuthorizationInterceptor.class);

	private static final String TOKEN_HEADER_KEY = "x-access-token";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if(!(handler instanceof HandlerMethod)) {
			// 不是映射到方法，直接通过
			return true;
		}
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Method method = handlerMethod.getMethod();
		// 检查是否有Authorization 的注解
		if(method.getAnnotation(Authorization.class) == null) {
			return true;
		}
		String token = request.getHeader(TOKEN_HEADER_KEY);
		Claims claims = JwtUtil.parseJWT(token);
		if(JwtUtil.jwtIsValid(claims)) {
			request.setAttribute("userId", Long.parseLong(claims.getSubject()));
            return true;
		} else {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
		}
	}
	
	@Override
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(
			HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}
}
