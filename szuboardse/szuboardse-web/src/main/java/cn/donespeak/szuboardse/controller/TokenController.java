package cn.donespeak.szuboardse.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.annotation.Authorization;
import cn.donespeak.szuboardse.annotation.CurrentUser;
import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.dto.TokenUser;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.AuthorizationService;
import cn.donespeak.szuboardse.vo.SignForm;

@Controller
@RequestMapping("/tokens")
public class TokenController {
	@Inject
	AuthorizationService authorizationService;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	@ResponseBody
	public ResponseMapJson generateToken(@RequestBody SignForm signForm){
		ResponseMapJson json = new ResponseMapJson();
		TokenUser tokenUser = null;
		try {
			tokenUser = authorizationService.generateToken(signForm.getEmail(), signForm.getPassword());
			json.push("token", tokenUser.getToken());
			json.push("user", tokenUser.getUser());
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}

	@Authorization
	@RequestMapping(value="", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseMapJson refreshToken(@CurrentUser long userId){
		ResponseMapJson json = new ResponseMapJson();
		String token = null;
		try {
			token = authorizationService.refreshToken(userId);
			json.push("token", token);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}
		return json;
	}
}
