package cn.donespeak.szuboardse.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.PublisherService;

@Controller
@RequestMapping("/publishers")
public class PublisherController {
	@Inject
	PublisherService publisherService; 
	
	@RequestMapping(value="", method=RequestMethod.GET)
	@ResponseBody
	public ResponseMapJson listPublishers(){
		ResponseMapJson json = new ResponseMapJson();
		try {
			List<Publisher> publishers = publisherService.listPublishersFromCache();
			json.push("publishers", publishers);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
		}		
		return json;
	}
}
