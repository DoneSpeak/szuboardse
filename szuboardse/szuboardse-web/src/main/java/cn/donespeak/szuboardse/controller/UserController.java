package cn.donespeak.szuboardse.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.donespeak.szuboardse.annotation.Authorization;
import cn.donespeak.szuboardse.annotation.CurrentUser;
import cn.donespeak.szuboardse.base.ResponseJson;
import cn.donespeak.szuboardse.base.ResponseMapJson;
import cn.donespeak.szuboardse.dto.UserSign;
import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.IdValue;
import cn.donespeak.szuboardse.entity.User;
import cn.donespeak.szuboardse.exception.ServiceException;
import cn.donespeak.szuboardse.service.FavoriteService;
import cn.donespeak.szuboardse.service.UserService;
import cn.donespeak.szuboardse.vo.SignForm;
import cn.donespeak.szuboardse.vo.securityForm;

@Controller
@RequestMapping("/users")
public class UserController {
	
	@Inject
	UserService userService;
	
	@Inject
	FavoriteService favoriteService;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	@ResponseBody
	public ResponseJson signup(@RequestBody SignForm signForm){
		ResponseMapJson json = new ResponseMapJson();
		try {
			userService.createUser(signForm.getEmail(), signForm.getPassword());
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
			return json;
		}
		return json;
	}

	// TODO@DoneSpeak 添加一个拦截器来判断url路径是否指向当前用户
	@Authorization
	@RequestMapping(value="/{uid}/name", method=RequestMethod.PATCH)
	@ResponseBody
	public ResponseJson signup(@CurrentUser long userId, @RequestBody User user){
		ResponseMapJson json = new ResponseMapJson();
		try {
			userService.updateUserName(userId, user.getName());
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
			return json;
		}
		return json;
	}

	@Authorization
	@RequestMapping(value="/{uid}/password", method=RequestMethod.PATCH)
	@ResponseBody
	public ResponseJson updatePassword(@CurrentUser long userId, @RequestBody securityForm form){
		ResponseMapJson json = new ResponseMapJson();
		try {
			userService.updateUserPassword(userId, form);
		} catch (ServiceException e) {
			json.setCode(e.getCode());
			json.setMsg(e.getMessage());
			return json;
		}
		return json;
	}
}