package cn.donespeak.szuboardse.mapper;

import org.apache.ibatis.annotations.Param;

import cn.donespeak.szuboardse.entity.User;

public interface UserMapper {

	User getById(long uid) throws Exception;

	User getByEmail(String email) throws Exception;

	void insert(User newUser) throws Exception;

	void updateName(@Param("uid")long uid, @Param("name")String name) throws Exception;

	void updatePassword(@Param("uid")long uid, @Param("password")String password) throws Exception;
}
