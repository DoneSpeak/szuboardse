package cn.donespeak.szuboardse.mapper;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.entity.Article;

public interface ArticleMapper {

	Long getMaxId() throws Exception;
	
	Article getById(long articleId) throws Exception;

	void insertBatch(List<Article> articles) throws Exception;

	Set<String> listArticleIdUpdatedTime(@Param("minArticleId")long minArticleId, 
			@Param("maxArticleId")long maxArticleId) throws Exception;

	void insert(Article article) throws Exception;

	List<Article> listAllArticles() throws Exception;

	List<Article> searchArticle(@Param("option")SearchOption searchOption, 
			@Param("start")int start, @Param("pageSize")int pageSize) throws Exception;

	int countSearchArticle(SearchOption searchOption) throws Exception;

	long countAll() throws Exception;

	List<Article> listPage(@Param("start")int start, @Param("pageSize")int pageSize) throws Exception;
	
	List<Article> listFullArticlePage(@Param("start")int start, @Param("pageSize")int pageSize) throws Exception;

	List<Article> listFavoriteArticles(@Param("uid")long uid, @Param("start")int start,
			@Param("pageSize")int pageSize) throws Exception;
}
