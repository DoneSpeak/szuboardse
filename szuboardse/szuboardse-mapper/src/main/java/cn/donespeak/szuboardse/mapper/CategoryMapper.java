package cn.donespeak.szuboardse.mapper;

import java.util.List;

import cn.donespeak.szuboardse.entity.Category;

public interface CategoryMapper {
	
	Category getById(int id) throws Exception;
	
	List<Category> listCategories() throws Exception;
}
