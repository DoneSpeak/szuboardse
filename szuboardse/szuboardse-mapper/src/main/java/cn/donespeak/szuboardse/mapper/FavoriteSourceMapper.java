package cn.donespeak.szuboardse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.donespeak.szuboardse.entity.FavoriteSource;
import cn.donespeak.szuboardse.entity.IdValue;

public interface FavoriteSourceMapper {
	
	void insert(FavoriteSource favoriteSource) throws Exception;
	
	void delete(@Param("uid")long userId, @Param("id")long id) throws Exception;

	List<FavoriteSource> listFavoriateSources(long uid) throws Exception;

	void removeSource(@Param("uid")long uid, @Param("categoryId")int categoryId,
			@Param("publisherId") int publisherId) throws Exception;

	List<IdValue> listUserOfFavoriteSrouce(@Param("categoryId")int categoryId, @Param("publisherId")int publisherId) throws Exception;
}
