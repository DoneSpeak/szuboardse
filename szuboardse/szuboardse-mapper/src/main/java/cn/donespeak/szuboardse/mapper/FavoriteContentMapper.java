package cn.donespeak.szuboardse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.donespeak.szuboardse.entity.FavoriteContent;

public interface FavoriteContentMapper {
	
	List<FavoriteContent> listFavoriteArticles(@Param("articleIds")List<Long> articleIds, @Param("uid")long uid) throws Exception;
	
	List<FavoriteContent> listFavoriteAttaches(@Param("attachIds")List<Long> attachIds, @Param("uid")long uid) throws Exception;
	
	void insertFavoriteArticle(FavoriteContent favoriteContent) throws Exception;

	void insertFavoriteAttach(FavoriteContent favoriteContent) throws Exception;
	
	void deleteFavoriteArticle(@Param("uid")long uid, @Param("articleId")long articleId) throws Exception;
	
	void deleteFavoriteAttach(@Param("uid")long uid, @Param("attachId")long attachId) throws Exception;

	int countFavoriteArticles(long uid) throws Exception;
	
	int countFavoriteAttaches(long uid) throws Exception;
}
