package cn.donespeak.szuboardse.mapper;

import java.util.List;

import cn.donespeak.szuboardse.entity.IdValue;

public interface ContentMapper {
	
	IdValue getArticleContent(long articleId) throws Exception;
	
	IdValue getAttachContent(long attachId) throws Exception;
	
	List<IdValue> listArticleContentIn(List<Long> articleIds) throws Exception;
	
	List<IdValue> listAttachContentIn(List<Long> attachIds) throws Exception;

	void insertArticleBatch(List<IdValue> contents) throws Exception;
	
	void insertAttachBatch(List<IdValue> contents) throws Exception;
}
