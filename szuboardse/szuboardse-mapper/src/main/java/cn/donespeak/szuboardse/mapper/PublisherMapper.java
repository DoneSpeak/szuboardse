package cn.donespeak.szuboardse.mapper;

import java.util.List;

import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;

public interface PublisherMapper {
	
	Category getById(int id) throws Exception;
	
	List<Publisher> listPublishers() throws Exception;
}
