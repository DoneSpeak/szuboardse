package cn.donespeak.szuboardse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.vo.TinyAttachItem;

public interface AttachMapper {

	void insertBatch(@Param("attaches")List<Attach> attaches) throws Exception;

	void insert(Attach attach) throws Exception;

	List<Attach> listAllAttaches() throws Exception;

	List<TinyAttachItem> listTinyAttachesIn(List<Long> articleIds) throws Exception;

	long countAll() throws Exception;

	List<Attach> listPage(@Param("start")int start, @Param("pageSize")int pageSize) throws Exception;
	
	List<Attach> listFullAttachPage(@Param("start")int start, @Param("pageSize")int pageSize) throws Exception;

	List<Attach> searchAttach(@Param("option")SearchOption searchOption, 
			@Param("start")int start, @Param("pageSize")int pageSize) throws Exception;

	int countSearchAttach(SearchOption searchOption) throws Exception;

	List<Attach> listFavoriteAttaches(@Param("uid")long uid, @Param("start")int startIndex,
			@Param("pageSize")int pageSize) throws Exception;
}