package cn.donespeak.szuboardse.mapper;

import java.util.List;

import cn.donespeak.szuboardse.entity.Picture;

public interface PictureMapper {

	void insertBatch(List<Picture> pictures) throws Exception;

	void insert(Picture picture) throws Exception;

	List<Picture> listPicturesIn(List<Long> articleIds) throws Exception;

}
