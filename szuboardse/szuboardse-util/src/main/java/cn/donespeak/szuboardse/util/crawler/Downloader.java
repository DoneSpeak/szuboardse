package cn.donespeak.szuboardse.util.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class Downloader {
	
	public static String download(String urlString, String destfile) throws IOException{
		File file = new File(destfile);
		return download(urlString, file.getParent(), file.getName());
	}
	
	/**
	 * download the image file that the urlString points to. The file will be stored in <code>destDir</code>.
	 * 	If you provide the filename, the file will be rename to <code>filename</code> you provide,
	 *  otherwise it will get an filename from urlString.
	 *  
	 * @param urlString One url point to an image file.
	 * @param destDir The directory which the image will be stored in.
	 * @param filename The local name of the image file. If it is <code>null</code>, 
	 * 		the image will get an filename from urlString.
	 * @return The path of the image file.
	 * @throws IOException
	 */
	public static String download(String urlString, String destDir, String filename) 
			throws IOException {
		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		byte[] bs = new byte[1024];
		int len;
		if(filename == null) {
			filename = getFileNameFromUrl(urlString);
		}
		File destFile = new File(destDir, filename);
		File dir = new File(destDir);
		if(!dir.exists() || !dir.isDirectory()) {
			dir.mkdirs();
		}
		InputStream input = conn.getInputStream();
		OutputStream output = null;
		try {
			conn.getInputStream();
			output = new FileOutputStream(destFile);
			while((len = input.read(bs)) != -1) {
				output.write(bs, 0, len);
			}
			output.flush();
		} finally {
			try {
				if(input != null) {
					input.close();
				}
				if(output != null) {
					output.close();
				}
			} catch (IOException e) {
				// do nothing
				// e.printStackTrace();
			}
		}
		return destFile.getAbsolutePath();
	}
	
	private static String getFileNameFromUrl(String urlString) {
		int index = urlString.lastIndexOf("/");
		if(index < 0) {
			return urlString;
		}
		return urlString.substring(index + 1);
	}
}
