package cn.donespeak.szuboardse.util.search;

public class IndexException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -909412217290410321L;

	public IndexException(){
		super();
	}
	
	public IndexException(String message){
		super(message);
	}
	
	public IndexException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public IndexException(Throwable cause) {
        super(cause);
    }
}
