package cn.donespeak.szuboardse.util.account;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import cn.donespeak.szuboardse.exception.InnerAlgorithmException;

public class PasswordUtil {
	
	/**
	 * encrypt the password
	 * @param password the real password
	 * @return the hash string created from encrypting the <code>password</code>
	 * @throws InnerAlgorithmException Something wrong with the encryption algorithm
	 */
	public static String encrypt(String password) throws InnerAlgorithmException {
		try {
			return PasswordHash.createHash(password);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new InnerAlgorithmException("The encryption algorithm has some internal errors "
					+ "when encrypting the password.");
		}
	}
	
	/**
	 * check the password is right or not
	 * @param rawPassword the password unencrypted
	 * @param passwordEncrypted a password encrypted
	 * @return true if the password is validate, otherwise false. 
	 * @throws NoSuchAlgorithmException Something wrong with the encryption algorithm
	 */
	public static boolean isValidate(String rawPassword, String passwordEncrypted) throws InnerAlgorithmException {
		try {
			return PasswordHash.validatePassword(rawPassword, passwordEncrypted);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new InnerAlgorithmException("The encryption algorithm has some internal errors "
					+ "when checking the password is validate or not.");
		}
	}
	
	/**
	 * 判断密码是符合规范的
	 * @param password
	 * @return
	 */
	public static boolean isValidate(String password) {
		// TODO@DoneSpeak
		return true;
	}
}
