package cn.donespeak.szuboardse.util;

public interface ObjectStringizer<T> {
	String stringize(T t);
}
