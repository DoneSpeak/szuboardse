package cn.donespeak.szuboardse.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Map;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;

/**
 * 自定义tokenClaims,因为token过期会导致无法解析，所以手动解析返回TokenClaims
 */
public class TokenClaims extends DefaultClaims {

	private static TokenClaims instance;
	
	private TokenClaims() {
        super();
    }
	
	private TokenClaims(Map<String, Object> map) {
        super(map);
    }
	
	/**
	 * 创建TokenClaims实体，使用单例模式，用id判断是否为同一个token
	 */
	public static synchronized TokenClaims getInstance(String token) {
		Map<String, Object> map = getClaimsMap(token);
        if (instance != null) {
        	String id = (String)map.get(Claims.ID);
        	if (instance.getId().equals(id))
        		return instance;
        }
        instance = new TokenClaims(map);   
        return instance;   
    }   
	
	/**
	 * 获取token中的字段信息
	 */
	public static Map<String, Object> getClaimsMap(String token) {
		String[] parts = token.split("\\.");
		String headers = new String(Base64.getDecoder().decode(parts[0].getBytes()));
		String payload = new String(Base64.getDecoder().decode(parts[1].getBytes()));
		System.out.println("headers: " + headers);
		System.out.println("payload: " + payload);
		
		@SuppressWarnings("unchecked")
		Map<String, Object> headersMap = JsonUtil.fromJson(headers);
		@SuppressWarnings("unchecked")
		Map<String, Object> payloadMap = JsonUtil.fromJson(payload);
		headersMap.putAll(payloadMap);
		return headersMap;
	}
	
}
