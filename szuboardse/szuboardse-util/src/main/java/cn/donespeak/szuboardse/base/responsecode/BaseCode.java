package cn.donespeak.szuboardse.base.responsecode;

public interface BaseCode {
	String category();
	String subcode();
	String code();
	String message();
}
