package cn.donespeak.szuboardse.exception;

public class AccessDatabaseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2341450216886265443L;

	public AccessDatabaseException(){
		super();
	}
	
	public AccessDatabaseException(String message){
		super(message);
	}
	
	public AccessDatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public AccessDatabaseException(Throwable cause) {
        super(cause);
    }
}
