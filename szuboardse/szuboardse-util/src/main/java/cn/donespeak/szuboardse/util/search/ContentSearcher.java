package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;

import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.entity.Picture;
import cn.donespeak.szuboardse.util.board.CategoryCache;
import cn.donespeak.szuboardse.util.board.PublisherCache;
import cn.donespeak.szuboardse.vo.Pagination;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;
import cn.donespeak.szuboardse.vo.TinyAttachItem;

public class ContentSearcher {
	private static Logger logger = Logger.getLogger(ContentSearcher.class);
	
	private static SearcherManager searcherManager = null;
	
	private static Analyzer analyzer;
	
	public static int HITS_PER_PAGE = 7;
	public static int MAX_PAGE_NUM = 77;
	public static int MAX_RESULT_ITEM_NUM = HITS_PER_PAGE * MAX_PAGE_NUM;
	
	static {
		try {
			searcherManager = new SearcherManager(LuceneHelper.getContentIndexFSD(), null);
			analyzer = LuceneHelper.getChineseAnalyzer();
		} catch (IOException e) {
			logger.error("Fail to init searchmanager", e);
		}
	}
	
	public static void refreshManager(boolean blocking) throws IOException {		
		if (!searcherManager.isSearcherCurrent()) {
			// 说明有新数据，需要刷新
			if(blocking) {
				searcherManager.maybeRefreshBlocking();
			} else {
				searcherManager.maybeRefresh();
			}
		}
	}
	
	public IndexSearcher acquireIndexSearcher() throws IOException {
		return searcherManager.acquire();
	}
	
	public void releaseIndexSearcher(IndexSearcher indexSearcher) throws IOException {
		searcherManager.release(indexSearcher);
	}
	
	public ResultPage search(SearchOption searchOption) throws ParseException, IOException {
		Query query = generateQuery(searchOption);
		int pageIndex = searchOption.getPageIndex();
		int start = (pageIndex - 1) * HITS_PER_PAGE;
		int end = start + HITS_PER_PAGE;
		
		IndexSearcher searcher = searcherManager.acquire();
		List<ResultItem> results = null;
		int hitsNum = 0;
		long totalHits = 0;
		try {
			TopDocs topDocs = searcher.search(query, MAX_RESULT_ITEM_NUM);
			ScoreDoc[] hits = topDocs.scoreDocs;
			hitsNum = hits.length;
			totalHits = topDocs.totalHits;
			end = Math.min(hits.length, end);
			results = new ArrayList<ResultItem>();
			// 一定要保持结果的顺序不发生变化
			for(int index = start; index < end; index ++) {
				Document doc = searcher.doc(hits[index].doc);
				ResultItem item = docToResultItem(doc);
				if(item != null) {
					results.add(item);
				}
			}
		} finally {
			// 释放检索器
			searcherManager.release(searcher);
		}
		Pagination pageInfo = new Pagination();
		pageInfo.setPageIndex(pageIndex);
		pageInfo.setPageSize(HITS_PER_PAGE);
		pageInfo.setPageNum((hitsNum + HITS_PER_PAGE - 1) / HITS_PER_PAGE);
		pageInfo.setTotal(hitsNum);

		ResultPage page = new ResultPage();
		page.setResultItems(results);
		page.setTotalhits(totalHits);
		page.setPageInfo(pageInfo);
		return page;
	}
	
	private ResultItem docToResultItem(Document doc) {
		int regionCode = Integer.parseInt(doc.get(FieldName.REGION.name()));
		Region region = Region.getEnum(regionCode);
		if(region == Region.ARTICLE) {
			return docToArticleResultItem(doc);
		} else if (region == Region.ATTACH) {
			return docToAttachResultItem(doc);
		} else {
			logger.error("wrong region type was indexed : " + region);
			return null;
		}
	}
	
	private ResultItem docToArticleResultItem(Document doc) {
		ResultItem item = ResultItem.getArticleResultImte();
		long articleId = FieldName.articleFieldIdValueToId(doc.get(FieldName.ID.name()));
		item.setArticleId(articleId);
		item.setAttachId(null);
		item.setTitle(doc.get(FieldName.TITLE.name()));		
		item.setUrl(doc.get(FieldName.URL.name()));
		
		int categoryId = Integer.parseInt(doc.get(FieldName.CATEGORY.name()));
		int publisherId = Integer.parseInt(doc.get(FieldName.PUBLISHER.name()));
		item.setCategory(CategoryCache.get(categoryId));
		item.setPublisher(PublisherCache.get(publisherId));
		
		item.setPublishedTime(Long.parseLong(doc.get(FieldName.PUBLISHED_TIME.name())));
		item.setUpdatedTime(Long.parseLong(doc.get(FieldName.UPDATED_TIME.name())));
		item.setDigest("");
		item.setIsfavorite(false);
		item.setPictures(new ArrayList<Picture>());
		item.setAttaches(new ArrayList<TinyAttachItem>());
		return item;
	}
	
	private ResultItem docToAttachResultItem(Document doc) {
		ResultItem item = ResultItem.getAttachResultImte();
		long attachId = FieldName.attachFieldIdValueToId(doc.get(FieldName.ID.name()));
		item.setAttachId(attachId);
		long articleId = Long.parseLong(doc.get(FieldName.ARTICLE_ID.name()));
		item.setArticleId(articleId);
		item.setTitle(doc.get(FieldName.TITLE.name()));
		item.setUrl(doc.get(FieldName.URL.name()));
		
		int categoryName = Integer.parseInt(doc.get(FieldName.CATEGORY.name()));
		int publisherName = Integer.parseInt(doc.get(FieldName.PUBLISHER.name()));
		item.setCategory(CategoryCache.get(categoryName));
		item.setPublisher(PublisherCache.get(publisherName));
		
		item.setPublishedTime(Long.parseLong(doc.get(FieldName.PUBLISHED_TIME.name())));
		item.setUpdatedTime(Long.parseLong(doc.get(FieldName.UPDATED_TIME.name())));

		item.setArticleUrl(doc.get(FieldName.ARTICLE_URL.name()));
		
		return item;
	}
	/**
	 * reference: [lucene7.1.0 (四) 各种查询](https://blog.csdn.net/u014401141/article/details/78329753)
	 * @param searchOption
	 * @return
	 * @throws ParseException
	 */
	public Query generateQuery(SearchOption searchOption) throws ParseException {
		
		/*
        1、MUST和MUST表示“与”的关系，即“交集”。
        2、MUST和MUST_NOT前者包含后者不包含。
        3、MUST_NOT和MUST_NOT没意义
        4、SHOULD与MUST表示MUST，SHOULD失去意义；
        5、SHOUlD与MUST_NOT相当于MUST与MUST_NOT。
        6、SHOULD与SHOULD表示“或”的概念。
		 */
		Builder booleanQueryBuilder = new Builder();
		booleanQueryBuilder.setMinimumNumberShouldMatch(0);
		
		// 对 title 和 content进行检索
		Query titleContentQuery = MultiFieldQueryParser.parse(searchOption.getQuery(), 
				new String[]{FieldName.TITLE.name(), FieldName.CONTENT.name()}, 
				new BooleanClause.Occur[]{BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD}, 
				analyzer);

		booleanQueryBuilder.add(titleContentQuery, BooleanClause.Occur.MUST);
		if(searchOption.searchRegion()) {
			Query regionQuery = new TermQuery(new Term(FieldName.REGION.name(), searchOption.getRegion().code() + ""));
			booleanQueryBuilder.add(regionQuery, BooleanClause.Occur.MUST);
		}
		if(searchOption.searchCategory()) {
			Query categoryQuery = new TermQuery(new Term(FieldName.CATEGORY.name(), searchOption.getCategory() + ""));
			booleanQueryBuilder.add(categoryQuery, BooleanClause.Occur.MUST);
		}
		if(searchOption.searchPublisher()) {
			Query publisherQuery = new TermQuery(new Term(FieldName.PUBLISHER.name(), searchOption.getPublisher() + ""));
			booleanQueryBuilder.add(publisherQuery, BooleanClause.Occur.MUST);
		}
		if(searchOption.searchDuration()) {
			Builder timeRangeQueryBuilder = new Builder();
			timeRangeQueryBuilder.setMinimumNumberShouldMatch(2);
			// reference: [从Lucene 4.10.3到Lucene 7.1.0:带你了解版本之间的些许差异](https://blog.csdn.net/cslucifer/article/details/78548980)
			// 检索的时间为发布时间或者更新时间 - 包含了边界值
			Query publishedTimeRangeQuery = LongPoint.newRangeQuery(FieldName.PUBLISHED_TIME.name(), 
					searchOption.getDurationStart(), searchOption.getDurationEnd());
			Query updatedTimeRangeQuery = LongPoint.newRangeQuery(FieldName.UPDATED_TIME.name(), 
					searchOption.getDurationStart(), searchOption.getDurationEnd());
			timeRangeQueryBuilder.add(publishedTimeRangeQuery, BooleanClause.Occur.SHOULD);
			timeRangeQueryBuilder.add(updatedTimeRangeQuery, BooleanClause.Occur.SHOULD);
			Query timeRangeQuery = timeRangeQueryBuilder.build();
			
			booleanQueryBuilder.add(timeRangeQuery, BooleanClause.Occur.MUST);
		}

		return booleanQueryBuilder.build();
	}
}
