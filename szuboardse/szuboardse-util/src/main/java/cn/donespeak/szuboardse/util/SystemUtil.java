package cn.donespeak.szuboardse.util;

public class SystemUtil {
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static boolean isWindows() {
		return OS.indexOf("windows") >= 0;
	}
	
	public static boolean isLinux() {
		return OS.indexOf("linux") >= 0;
	}
	
	public static String getOSName() {
		return OS;
	}
	
	public static void main(String[] args) {
		System.out.println(getOSName());
		System.out.println(isWindows());
		System.out.println(isLinux());
	}
}
