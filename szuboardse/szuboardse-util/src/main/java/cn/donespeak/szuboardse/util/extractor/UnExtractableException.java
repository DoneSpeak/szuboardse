package cn.donespeak.szuboardse.util.extractor;

public class UnExtractableException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7601584251875571918L;

	UnExtractableException(){
		super();
	}
	
	UnExtractableException(String message){
		super(message);
	}
}
