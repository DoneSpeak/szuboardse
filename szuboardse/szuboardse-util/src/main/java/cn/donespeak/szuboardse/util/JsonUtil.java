package cn.donespeak.szuboardse.util;

import java.util.Map;

import net.sf.json.JSONObject;

public class JsonUtil {

	public static Map<String, Object> fromJson(String json) {
		JSONObject jsonobj = JSONObject.fromObject(json);
		@SuppressWarnings("unchecked")
		Map<String, Object> map = jsonobj;
		return map;
	}
	
	public static void main(String[] args) {
		String json = "{\"PayPal key2\":\"PayPal value2\",\"PayPal key1\":\"PayPal value1\",\"PayPal key3\":\"PayPalvalue3\"}";
		System.out.println(fromJson(json));
	}
}
