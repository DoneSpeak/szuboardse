package cn.donespeak.szuboardse.util.crawler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import cn.donespeak.szuboardse.util.crawler.BoardSearchOption.DayyOption;

/**
 * Created by DoneSpeak on 2017/6/2.
 * 下载完整的HTML文件
 * 【参考】
 * [基于apache —HttpClient的小爬虫获取网页内容](http://www.dongcoder.com/detail-449399.html)
 */
public class PageRequester {

	private final String url;
	private Map<String, String> params; 
	private String charset = "GB2312";
	
	public static enum RequestMethod {
		POST,
		GET
		;
	}
	
	public PageRequester(String url) {
		this.url = url;
		this.params = new HashMap<String, String>();
	}
	
	public void setCharest(String charset) {
		this.charset = charset;
	}
	
	public void setParameter(String key, String value) {
		this.params.put(key, value);
	}
	
	public void setParameters(Map<String, String> params) {
		if(params == null) {
			return;
		}
		for(Entry<String, String> param: params.entrySet()) {
			this.params.put(param.getKey(), param.getValue());
		}
	}
	
	public String post() throws URISyntaxException, Exception {
		return getContent(generateURI(), RequestMethod.POST, charset);
	}
	
	public String get() throws ParseException, IOException, URISyntaxException {
		return getContent(generateURI(), RequestMethod.GET, charset);
	}
	
	private URI generateURI() throws URISyntaxException {
		URIBuilder uriBuilder = new URIBuilder(url);
		for(Entry<String, String> param: params.entrySet()) {
			uriBuilder.setParameter(param.getKey(), param.getValue());
		}
		if(charset != null) {
			uriBuilder.setCharset(Charset.forName(charset));
		}
        return uriBuilder.build();     
	}
	
    // 获取 uri 指向连接的 html 字符串
    private String getContent(URI uri, RequestMethod method, String charset) throws ParseException, IOException {
        CloseableHttpResponse response = null;
        String html = null;
        try {
            response = getResponse(uri, method);
            // 请求不成功
            if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
                String error = "parse failed: "+ response.getStatusLine();
                throw new IOException(error);
            }
            // 返回获取实体
            HttpEntity entity=response.getEntity();
            html = EntityUtils.toString(entity, charset);
        } finally{
            try {
                if(response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return html;
    }

    private CloseableHttpResponse getResponse(URI uri, RequestMethod method) throws ClientProtocolException, IOException {
        return createResponse(uri, method);
    }
    
    private CloseableHttpResponse createResponse(URI uri, RequestMethod method) throws ClientProtocolException, IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        //设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(5000)
                .setConnectTimeout(5000)
                .build();
        if(method == RequestMethod.GET){
            HttpGet httpget = null;
            httpget = new HttpGet(uri);
            httpget.setConfig(requestConfig);
            //执行 get 请求
            response = httpclient.execute(httpget);
        }else if(method == RequestMethod.POST){
            HttpPost httppost = null;
            httppost = new HttpPost(uri);
            httppost.setConfig(requestConfig);
            //执行 post 请求
            response = httpclient.execute(httppost);
        }else{
            String error = "can't deal with method type ：" + method;
            throw new IllegalArgumentException(error);
        }
        return response;
    }
    
    private static void crawlOlder() throws URISyntaxException, Exception {
    	String title = "英国教育讲座";
		BoardSearchOption option = BoardSearchOption.defaultSearch(DayyOption.FIFTY_YEAR);
		option.keyword = title;
		
    	PageRequester requester = new PageRequester("http://www1.szu.edu.cn/board/");
    	requester.setParameters(option.toParamMap());
    	System.out.println(requester.post());
    }
    
    private static void crawlNewOne() throws URISyntaxException, Exception {
    	String title = "关于2018年成人本科学士学位外语水平统考监考培训的通知";
		BoardSearchOption option = BoardSearchOption.defaultSearch(DayyOption.FIFTY_YEAR);
		option.keyword = title;
		
    	PageRequester requester = new PageRequester("http://www1.szu.edu.cn/board/");
    	requester.setParameters(option.toParamMap());
    	System.out.println(requester.post());
    }
    
    public static void main(String[] args) throws Exception {
    	crawlNewOne();
    	crawlOlder();
    }
}