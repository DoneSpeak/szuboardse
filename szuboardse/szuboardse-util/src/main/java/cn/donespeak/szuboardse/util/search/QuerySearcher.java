package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TopDocs;

public class QuerySearcher {
	private static Logger logger = Logger.getLogger(QuerySearcher.class);
	
	private static final int RELATIVE_QUERY_MAX_TOP = 50;
	
	private static SearcherManager searcherManager = null;
	private static Analyzer analyzer = null;
	
	static {
		try {
			searcherManager = new SearcherManager(LuceneHelper.getRelativeQueryFSDir(), null);
			analyzer = LuceneHelper.getChineseAnalyzer();
		} catch (IOException e) {
			logger.error("Fail to init query searchmanager", e);
		}
	}
	
	public static void refreshManager(boolean blocking) throws IOException {		
		if (!searcherManager.isSearcherCurrent()) {
			// 说明有新数据，需要刷新
			if(blocking) {
				searcherManager.maybeRefreshBlocking();
			} else {
				searcherManager.maybeRefresh();
			}
		}
	}
	
	public List<String> search(String keyWord, int top) throws IOException, ParseException {
		if(keyWord == null || keyWord.length() == 0) {
			return new ArrayList<String>();
		}
		IndexSearcher indexSearcher = searcherManager.acquire();
		
		QueryParser queryParser = new QueryParser(FieldName.QUERY.name(), analyzer);
		Query query = queryParser.parse(keyWord);
		
		top = top > RELATIVE_QUERY_MAX_TOP ? RELATIVE_QUERY_MAX_TOP: top;
		List<String> relativeQueries = new ArrayList<String>();
		try {
			TopDocs topDocs = indexSearcher.search(query, top);
			ScoreDoc[] scoreDocs = topDocs.scoreDocs;
			for(ScoreDoc scoreDoc: scoreDocs) {
				Document doc = indexSearcher.doc(scoreDoc.doc);
				String queryStr = doc.get(FieldName.QUERY.name());
				relativeQueries.add(queryStr);
			}
		} finally {
			// 每次获得一个searcher, 都需要release.
			searcherManager.release(indexSearcher);
		}
		return relativeQueries;
	}
}
