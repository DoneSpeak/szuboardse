package cn.donespeak.szuboardse.base.responsecode;

public enum PublisherCode{

	DATA_NOT_EXIST("data_not_exist", "发文单位不存在")
	;
	
	private final String CATEGORY = "publisher";	
	public final CodeEntry entry;
	PublisherCode(String subcode, String message) {
		this.entry = new CodeEntry(CATEGORY, subcode, message);
	}
}
