package cn.donespeak.szuboardse.util.account;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormUtil {
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
	    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	public static final Pattern VALID_PASSWORD_REGEX =
		Pattern.compile("^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$");

	public static boolean validateEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.find();
	}
	/**
	 * 必须包含数字和字符，且长度应为8-20
	 * @param newPassword
	 * @return
	 */
	public static boolean validatePassword(String password) {
		 Matcher matcher = VALID_PASSWORD_REGEX .matcher(password);
	     return matcher.find();
	}
	
	public static void main(String[] args) {
		String[] passes = new String[] {
			"",
			"123456789",
			"12345678a",
			"12345q",
			"1234adsf",
			"12345678901234567890q"
		};
		for(String p: passes) {
			System.out.println(p + ":" + validatePassword(p));
		}
	}

}
