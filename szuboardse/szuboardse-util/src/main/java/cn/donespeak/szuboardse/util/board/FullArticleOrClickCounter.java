package cn.donespeak.szuboardse.util.board;

import cn.donespeak.szuboardse.dto.ArticleClickCounter;
import cn.donespeak.szuboardse.dto.FullArticle;

public class FullArticleOrClickCounter {
	private FullArticle fullArticle;
	private ArticleClickCounter clickCounter;
	
	public boolean isFullArticle(){
		return fullArticle != null;
	}
	public boolean isClickCounter(){
		return clickCounter != null;
	}
	
	/* getter and setter */
	public FullArticle getFullArticle() {
		return fullArticle;
	}
	public void setFullArticle(FullArticle fullArticle) {
		this.fullArticle = fullArticle;
		this.clickCounter = null;
	}
	public ArticleClickCounter getClickCounter() {
		return clickCounter;
	}
	public void setClickCounter(ArticleClickCounter clickCounter) {
		this.clickCounter = clickCounter;
		this.fullArticle = null;
	}
}
