package cn.donespeak.szuboardse.util.extractor;

/**
 * Created by DoneSpeak on 2017/6/10.
 */
public enum TextType {
    DOC("doc"),
    DOCX("docx"),
    PPT("ppt"),
    PPTX("pptx"),
    XLS("xls"),
    XLT("xlt"),
    XLSX("xlsx"),
    TXT("txt"),
    PDF("pdf");
    
	private String extension;
	
	TextType(String extension) {
		this.extension = extension;
	}
	
	public String getExtension(){
		return extension;
	}
	
	public static TextType getTextType(String path){
		path = path.trim();
        int lastPoint = path.lastIndexOf(".");
        if(lastPoint < 0) {
        	return null;
        }
        String extention = path.substring(lastPoint + 1).toLowerCase();
        TextType[] types = TextType.values();
        for(TextType type: types) {
        	if(extention.equals(type.getExtension())) {
        		return type;
        	}
        }
        return null;
    }
}
