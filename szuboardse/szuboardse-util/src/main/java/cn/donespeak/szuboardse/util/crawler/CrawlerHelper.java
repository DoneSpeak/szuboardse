package cn.donespeak.szuboardse.util.crawler;

public class CrawlerHelper {
	public static final String SZUBOARD_HOST = "http://www1.szu.edu.cn";
	
	public static String generateArticleUrl(long articleId) {
		String url = SZUBOARD_HOST + "/board/view.asp?id=" + articleId;
		return url;
	}
}
