package cn.donespeak.szuboardse.util.search.ikanalyzer;

import java.io.IOException;
import java.io.StringReader;

/**
 * 关于停用词： [IKAnalyzer使用停用词词典进行分词](https://blog.csdn.net/shijiebei2009/article/details/39697043)
 * 由于IKAnalyzer与Lucene 7.2.1部分不兼容。所已重写了这部分该类
 */
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

/**
 * IK分词器，Lucene Analyzer接口实现
 * 兼容Lucene 4.0版本
 */
public final class GIKAnalyzer extends Analyzer {

  private boolean useSmart;

  public boolean useSmart() {
    return useSmart;
  }

  public void setUseSmart(boolean useSmart) {
    this.useSmart = useSmart;
  }

  /**
   * IK分词器Lucene  Analyzer接口实现类
   *
   * 默认细粒度切分算法
   */
  public GIKAnalyzer() {
    this(false);
  }

  /**
   * IK分词器Lucene Analyzer接口实现类
   *
   * @param useSmart 当为true时，分词器进行智能切分
   */
  public GIKAnalyzer(boolean useSmart) {
    super();
    this.useSmart = useSmart;
  }

  /**
   * 重载Analyzer接口，构造分词组件
   */
  @Override
  protected TokenStreamComponents createComponents(String fieldName) {
    Tokenizer _IKTokenizer = new GIKTokenizer(this.useSmart());
    return new TokenStreamComponents(_IKTokenizer);
  }

  public static void main(String[] args) throws IOException {
	Analyzer analyzer = new GIKAnalyzer();
	TokenStream tokenStream = analyzer.tokenStream("Field", new StringReader("计算机与软件学院 奖学金"));
	CharTermAttribute cta = tokenStream.addAttribute(CharTermAttribute.class);
	tokenStream.reset();  //必须先调用reset方法
	while(tokenStream.incrementToken()){
		System.out.println(cta);
	}
  }
}
