package cn.donespeak.szuboardse.base.responsecode;

public enum SystemCode {
	INNER_ERROR("inner_error", "系统内部错误"),
	DATABASE_EXCEPTION("database_exception", "数据库错误")
	;
	
	private final String CATEGORY = "system";
	public final CodeEntry entry;	
	SystemCode(String subcode, String message) {
		this.entry = new CodeEntry(CATEGORY, subcode, message);
	}
}
