package cn.donespeak.szuboardse.base;

import java.util.HashMap;
import java.util.Map;

import cn.donespeak.szuboardse.vo.View;

public class ResponseCarrier {
	protected String code;
	protected String msg;
	protected Object data = null;
	protected Map<String, Object> map = null;
	
	public static final String SUCCESS_CODE = "success";
	public static final String SUCCESS_MSG = "Success";
	
	public ResponseCarrier() {
		this.code = SUCCESS_CODE;
		this.msg = SUCCESS_MSG;
		this.data = null;
		this.map = new HashMap<String, Object>();
	}
	public ResponseCarrier(String code, String msg){
		this.code = code;
		this.msg = msg;
		this.data = null;
		this.map = null;
	}
	public < T extends Object & View> ResponseCarrier(String code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.map = null;
	}	
	public ResponseCarrier(String code, String msg, Map<String, Object> data){
		this.code = code;
		this.msg = msg;
		this.data = null;
		this.map = data;
	}	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data == null? map: data;
	}
	public < T extends Object & View> void setData(T data) {
		this.data = data;
		this.map = null;
	}
	public void setData(Map<String, Object> data) {
		this.map = data;
		this.data = null;
	}
	public void push(String field, Object value){
		if(map != null) {
			map.put(field, value);
		}
	}	
	public void delete(String field){
		if(map != null) {
			map.remove(field);
		}
	}	
	public void empty(){
		data = null;
		map = null;
	}
}
