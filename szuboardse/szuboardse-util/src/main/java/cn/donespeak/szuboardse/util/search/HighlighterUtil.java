package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.wltea.analyzer.lucene.IKAnalyzer;

import cn.donespeak.szuboardse.util.search.ikanalyzer.GIKAnalyzer;

public class HighlighterUtil {
	
	private Highlighter highlighter;
	private Analyzer analyzer;
	private final int fragmentSize;
	
	private static String DEFAULT_PRE_TAG = "<span class='se-highlight'>";
	private static String DEFAULT_POST_TAG = "</span>";
	
	private static final String DEFAULT_HIGHLIGHT_FIELD = "field";
	
	private static final Analyzer DEFAULT_ANALYZER = LuceneHelper.getChineseAnalyzer();
		
	public HighlighterUtil(String queryStr, String preTag, String postTag, int fragmentSize, Analyzer analyzer) throws ParseException {
		QueryParser queryParse = new QueryParser(DEFAULT_HIGHLIGHT_FIELD, analyzer);
		Query query = queryParse.parse(queryStr);
		
		SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter(preTag, postTag);
		this.highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));
		this.highlighter.setTextFragmenter(new SimpleFragmenter(fragmentSize));
		
		this.analyzer = analyzer;
		this.fragmentSize = fragmentSize;
	}
	
	public HighlighterUtil(String queryStr, String preTag, String postTag, int fragmentSize) throws ParseException {
		this(queryStr, DEFAULT_PRE_TAG, DEFAULT_POST_TAG, fragmentSize, DEFAULT_ANALYZER);
	}
	
	public HighlighterUtil(String queryStr, int fragmentSize, Analyzer analyzer) throws ParseException {
		this(queryStr, DEFAULT_PRE_TAG, DEFAULT_POST_TAG, fragmentSize, analyzer);
	}
	
	public HighlighterUtil(String queryStr, int fragmentSize) throws ParseException {
		this(queryStr, fragmentSize, DEFAULT_ANALYZER);
	}
	
	public int getFragmentLeng() {
		return fragmentSize;
	}

	public String hightlight(String content) throws IOException, InvalidTokenOffsetsException {
		if(content == null || content.length() == 0) {
			return content;
		}
		TokenStream tokenStream = analyzer.tokenStream(DEFAULT_HIGHLIGHT_FIELD, new StringReader(content));
		String highlightText = highlighter.getBestFragment(tokenStream, content);
		if(highlightText == null || highlightText.length() == 0) {
			return content.length() > fragmentSize? content.substring(0, fragmentSize): content;
		}
		return highlightText;
	}
	
	public static void main(String[] args) throws ParseException, IOException {
		String queryStr = "模块并且访问权限类已经不鼓励使用的吧is to be a";
		String content = "域缓存加载所有文档中某个特定域的值到内存，便于随机存取该域值。"
				+ "域缓存只能用于包含单个项的域，也就是说使用域缓存的文档都必须拥有一个与指定域对应的单一域值，"
				+ "即域值不能被拆分。FieldCache在Lucene 5.5.0中被lucene-core模块移除，"
				+ "转移到了lucene-misc模块并且访问权限为包级私有，FieldCache类已经不鼓励使用，"
				+ "当要在某个Field上进行排序时，用户应该使用Doc值来索引相应的Field，"
				+ "相对于使用FieldCache更加快且耗费较少的堆大小";

		HighlighterUtil highlighterUtil = new HighlighterUtil(queryStr, 140, new GIKAnalyzer());
		String highlightText = "";
		try {
			highlightText = highlighterUtil.hightlight(content);
			highlightText = highlighterUtil.hightlight(content);
		} catch (IOException | InvalidTokenOffsetsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(highlightText);
	}
}
