package cn.donespeak.szuboardse.base.responsecode;

public enum CategoryCode{
	
	DATA_NOT_EXIST("data_not_exist", "类别不存在")
	;
	
	private final String CATEGORY = "category";	
	public final CodeEntry entry;
	CategoryCode(String subcode, String message) {
		this.entry = new CodeEntry(CATEGORY, subcode, message);
	}
}
