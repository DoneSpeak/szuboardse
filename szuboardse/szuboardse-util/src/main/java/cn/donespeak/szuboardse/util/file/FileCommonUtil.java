package cn.donespeak.szuboardse.util.file;

import java.io.File;

import cn.donespeak.szuboardse.util.SystemUtil;

public class FileCommonUtil {
	public static String separatorNormalize(String path) {
		if(SystemUtil.isWindows()) {
			return path.replace("/", File.separator);
		} else {
			return path.replace("\\", File.separator);
		}
	}
	
	public static void main(String[] args) {
		System.out.println(separatorNormalize("/var/temp/iamges/idfd.jpg"));
	}
}
