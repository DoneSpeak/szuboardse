package cn.donespeak.szuboardse.util.crawler;

import java.util.HashMap;
import java.util.Map;

public class BoardSearchOption implements Cloneable{
	
	public static enum DayyOption {
		ONE_DAY("1#一天"),
		THREE_DAIES("3#三天"),
		ONE_WEEK("7#一周"),
		HALF_MONTH("15#半个月"),
		ONE_MONTH("30#一个月"),
		THREE_MONTH("90#三个月"),
		HALF_YEAR("182#半年"),
		ONE_YEAR("365#一年"),
		TWO_YEAR("730#两年"),
		THREE_YEAR("1100#三年"),
		FIVE_YEAR("1830#五年"),
		TEN_YEAR("3650#十年"),
		FIFTY_YEAR("18250#五十年")
		;
		
		private final String value;
		
		DayyOption(String value) {
			this.value = value;
		}
		
		public String value() {
			return value;
		}
	}
	
	public DayyOption dayy;
	public String keyword;
	public String searchType = "title";
	public String keywordUser = "";
	private final String searchb1 = "搜索";
	
	/**
	 * @return
	 */
	 // TODO@DoneSpeak 思考，如果要设置成一个不可修改静态BoardSearchOption成员，该如何实现
	public static BoardSearchOption oneMonthSearch() {
		BoardSearchOption option = new BoardSearchOption();
		option.dayy = DayyOption.ONE_MONTH;
		option.searchType = "title";
		option.keyword = " ";
		return option;
	}
	
	public static BoardSearchOption defaultSearch(DayyOption dayy) {
		BoardSearchOption option = new BoardSearchOption();
		option.dayy = dayy;
		option.searchType = "title";
		option.keyword = " ";
		return option;	
	}
		
	public Map<String, String> toParamMap() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("dayy", dayy.value);
		params.put("search_type", searchType);
		params.put("keyword_user", keywordUser);
		params.put("keyword", keyword);
		params.put("searchb1", searchb1);
		return params;
	}
	
	@Override
	public BoardSearchOption clone() {
		BoardSearchOption option = null;
		try {
			option = (BoardSearchOption)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return option;
	}

	@Override
	public String toString() {
		return "BoardSearchOption [dayy=" + dayy + ", searchType=" + searchType + ", keyword=" + keyword
				+ ", keywordUser=" + keywordUser + ", searchb1=" + searchb1 + "]";
	}
}
