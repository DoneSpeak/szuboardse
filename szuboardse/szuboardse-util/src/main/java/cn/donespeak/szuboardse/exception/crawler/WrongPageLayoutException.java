package cn.donespeak.szuboardse.exception.crawler;

public class WrongPageLayoutException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2604478643969416353L;
	
	public WrongPageLayoutException() {
		super();
	}
	
	public WrongPageLayoutException(String message) {
		super(message);
	}

}
