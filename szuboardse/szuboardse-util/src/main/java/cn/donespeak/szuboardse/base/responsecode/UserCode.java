package cn.donespeak.szuboardse.base.responsecode;

public enum UserCode {
	EMAIL_TAKEN("email_taken", "邮箱已被注册"),
	PASSWORD_INVALID("password_invalid", "必须包含数字和字符，且长度大于8"),
	PASSWORD_WRONG("password_wrong", "密码错误"),
	EMAIL_WRONG("email_wrong", "邮箱错误")
	;
	private final String CATEGORY = "user";	
	public final CodeEntry entry;
	
	UserCode(String subcode, String message) {
		this.entry = new CodeEntry(CATEGORY, subcode, message);
	}
}
