package cn.donespeak.szuboardse.util.crawler;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.donespeak.szuboardse.dto.ArticleClickCounter;
import cn.donespeak.szuboardse.dto.FullArticle;
import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Picture;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.crawler.WrongPageLayoutException;
import cn.donespeak.szuboardse.util.DateUtil;
import cn.donespeak.szuboardse.util.SystemUtil;
import cn.donespeak.szuboardse.util.board.FullArticleOrClickCounter;
import cn.donespeak.szuboardse.util.extractor.CommonExtractor;
import cn.donespeak.szuboardse.util.extractor.Extractor;

public class ArticleCrawler {
	Logger logger = Logger.getLogger(ArticleCrawler.class);
	
	public static final String imagesStoredDestDir;
	public static final String imagesStorePathPrefix;
	public static final String websiteHost = "http://119.29.119.37/";
	
	static {
		if(SystemUtil.isWindows()) {
			imagesStoredDestDir = "C:\\var\\szuboardse\\www\\store\\files\\images\\";
			imagesStorePathPrefix = "C:\\var\\szuboardse\\www\\store\\";
		} else {
			imagesStoredDestDir = "/var/szuboardse/www/store/files/images/";
			imagesStorePathPrefix = "/var/szuboardse/www/store/";
		}
	}
	
	private ArticleListItem articleListItem;
	private Elements articleEles;
	private long articleId;
	private URL articleUrl;
	private File localFile;
	
	private String charsetName = "GB2312";
	
	private final int DIGEST_MAX_LENGTH = 140;
		
	private boolean downloadImages = true;
	private boolean extractAttachContent = true;
	
	public ArticleCrawler(ArticleListItem articleListItem) throws IOException, WrongPageLayoutException {
		this(articleListItem, null, "GB2312", true, true);
	}
	
	public ArticleCrawler(ArticleListItem articleListItem, File localFile) 
			throws IOException, WrongPageLayoutException {
		this(articleListItem, localFile, "GB2312", true, true);
	}
	
	public ArticleCrawler(ArticleListItem articleListItem, File localFile, String charsetName) 
			throws IOException, WrongPageLayoutException {
		this(articleListItem, localFile, charsetName, true, true);
	}
	
	public ArticleCrawler(ArticleListItem articleListItem, File localFile, 
			boolean downloadImages, boolean extractAttachContent) throws IOException, WrongPageLayoutException {
		this(articleListItem, localFile, "GB2312", downloadImages, extractAttachContent);
	}
	
	public ArticleCrawler(ArticleListItem articleListItem, File localFile, String charsetName, 
			boolean downloadImages, boolean extractAttachContent) throws IOException, WrongPageLayoutException {
		this.articleListItem = articleListItem;
		this.articleId = articleListItem.getArticleId();
		this.localFile = localFile;
		this.articleUrl = new URL(articleListItem.getUrl());
		this.charsetName = charsetName;
		this.downloadImages = downloadImages;
		this.extractAttachContent = extractAttachContent;
		
		init();
	}
	
	private void init() throws IOException, WrongPageLayoutException {
		if(localFile != null) {
			articleEles = getArticleElementsFromFile(localFile, articleListItem.getUrl());
		} else {
			articleEles = getArticleElementsFromUrl(articleListItem.getUrl());			
		}
		if(articleEles == null) {
			throw new WrongPageLayoutException("Can't find the article area.");
		}
	}
	
	private Elements getArticleElementsFromFile(File localFile, String baseUri) throws IOException {
		Document doc = Jsoup.parse(localFile, charsetName, baseUri);
		Elements articleEles = doc.getElementsByTag("tbody").get(6).children();
		return articleEles;
	}

	private Elements getArticleElementsFromUrl(String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		Elements articleEles = doc.getElementsByTag("tbody").get(6).children();
		return articleEles;
	}
	
	public long getArticleId() {
		return articleId;
	}
	
	/**
	 * Extract article, attaches, picture from the article url which contain in <code>ArticleListItem</code>. 
	 * This is the only API of ArticleCrawler.
	 */
	public FullArticle extractFullArticle() throws IOException, ParseException{
		logger.info("start to crawl article, attaches and pictures from " + articleUrl.toString());
		Article article = extractArticle();
		if(article == null) {
			return null;
		}
		List<Picture> pictures = extractPictures(article);
		
		List<Attach> attaches = extractAttaches(article);
		
		return new FullArticle(article, pictures, attaches);
	}
	
	public List<Picture> extractPictures() throws ParseException, MalformedURLException, 
			UnsupportedEncodingException {
		Article article = extractArticle();		
		return extractPictures(article);
	}
	
	public List<Attach> extractAttaches() throws ParseException {
		Article article = extractArticle();		
		return extractAttaches(article);
	}
	
	public Article extractArticle() throws ParseException {
		String title = extractTitle();
		String content = extractContent();
		long publishedTime = extractPublishedTime();
		long updateTime = extractUpdatedTime();
		int clickCount = extractClick();
		
		Article article = new Article();
		article.setTitle(title);
		
		Publisher publisher = articleListItem.getPublisher();
		Category category = articleListItem.getCategory();
		
		if(publisher == null || category == null) {
			logger.warn("Publisher or category is null. category: " + category + ", publisher: " + publisher);
			return null;
		}
		
		article.setPublisherId(publisher.getId());
		article.setCategoryId(category.getId());
		int digestLength = content.length() < DIGEST_MAX_LENGTH? content.length(): DIGEST_MAX_LENGTH;
		String digest = content.substring(0, digestLength);
		article.setDigest(digest);
		article.setContent(content);
		article.setPublishedTime(publishedTime);
		article.setUpdatedTime(updateTime);
		article.setBaseClick(clickCount);
		article.setAdditionClick(0);
		article.setGmtCreated(DateUtil.getNow());
		article.setGmtModified(article.getGmtCreated());
		article.setId(articleListItem.getArticleId());
		article.setUrl(articleListItem.getUrl());
		
		return article;
	}
	
	public String extractTitle() throws ParseException {
		Element titleEle = articleEles.get(0);
		String title = formatTitle(titleEle.text());
		return title;
	}
	
	public long extractPublishedTime() throws ParseException {
		Element publishEle = articleEles.get(1);
		long publishedTime = extractTime(publishEle.text());
		return publishedTime;
	}
	
	public String extractContent(){
		Element contentEle = articleEles.get(2);
		String content = contentEle.text();
		return content;
	}
		
	public long extractUpdatedTime() throws ParseException{
		return extractTime(extractUpdateTimeAndClickText());		
	}
	
	public int extractClick(){
		return extractClickCount(extractUpdateTimeAndClickText());
	}
	
	private String extractUpdateTimeAndClickText(){
		Elements attachesAndUpdateTimeAndClickCount = articleEles.get(3).getElementsByTag("tbody").first()
				.getElementsByTag("tr").first().getElementsByTag("td");
		Element updateTimeAndClickEle = attachesAndUpdateTimeAndClickCount.last();
		String updateTimeAndClickStr = updateTimeAndClickEle.text();
		return updateTimeAndClickStr;
	}
	
	public List<String> extractContentImageUrls() throws MalformedURLException, 
		UnsupportedEncodingException{
		Element contentEle = articleEles.get(2);
		return extractContentImages(contentEle);
	}
	
	public List<String> extractAttacheUrls() throws MalformedURLException, 
		UnsupportedEncodingException {
		Elements attachLinks = extractAttachLinkElements();
		return fillUrl(attachLinks.eachAttr("href"));
	}
	
	private List<String> extractContentImages(Element contentEle) 
			throws MalformedURLException, UnsupportedEncodingException {
		Elements imageEles = contentEle.select(".MsoNormal img[src]");
		return fillUrl(imageEles.eachAttr("src"));
	}
	
	public List<String> fillUrl(List<String> hrefs) throws MalformedURLException {
		List<String> urls = new ArrayList<String>(); 
		for(String href: hrefs) {
			urls.add(fillUrl(href));
		}
		return urls;
	}
	
	public String fillUrl(String href) throws MalformedURLException {
		return new URL(articleUrl, href).toString();
	}
	
	public List<String> encodeUrl(List<String> urls) throws UnsupportedEncodingException {
		List<String> urlsEncoded = new ArrayList<String>(urls.size());
		for(String url: urls) {
			urlsEncoded.add(encodeUrl(url));
		}
		return urlsEncoded;
	}
	
	public String encodeUrl(String url) throws UnsupportedEncodingException {
		int index = url.lastIndexOf("/");
		String fileName = url.substring(index + 1);
		return url.substring(0, index + 1) + URLEncoder.encode(fileName, charsetName);
	}

	// extract the time stamp from string contains string whose format like 20yy-MM-dd HH:mm:ss
	private long extractTime(String publishedTimeContent) throws ParseException{
		String dateRegix = "20[0-9]{2}-[0-9]{1,2}-[0-9]{1,2} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}";
        Pattern pattern = Pattern.compile(dateRegix);
        Matcher matcher = pattern.matcher(publishedTimeContent);
        if(!matcher.find()){
            return -1;
        }
        String dateStr = matcher.group();
        return DateUtil.dateToStamp(dateStr);
	}
	
	private int extractClickCount(String updateTimeAndClickStr) {
		int index = updateTimeAndClickStr.length() - 1;
		for(; index >= 0 && !Character.isDigit(updateTimeAndClickStr.charAt(index)); index --) {}
		int end = index + 1;
		for(; index >= 0 && Character.isDigit(updateTimeAndClickStr.charAt(index)); index --) {}
		int start = index + 1;
		if(start >= end) {
			return 0;
		}
		return Integer.parseInt(updateTimeAndClickStr.substring(start, end));
	}

	private List<Picture> extractPictures(Article article) 
			throws MalformedURLException, UnsupportedEncodingException {
		if(article == null) {
			return null;
		}
		List<String> imgUrls = extractContentImageUrls();
		List<Picture> pictures = new ArrayList<Picture>();
		
		for(int imgUrlIndex = 0; imgUrlIndex < imgUrls.size(); imgUrlIndex ++) {
			String imgUrl = imgUrls.get(imgUrlIndex);
			Picture picture = new Picture();
			picture.setArticleId(article.getId());
			picture.setGmtCreated(article.getGmtCreated());
			picture.setUrl(imgUrl);
			if(downloadImages) {
				// It's possible that downloadImage throws an exception when download images.
				String path = null;
				try {
					// TODO@DoneSpeak consider download lazily
					path = Downloader.download(imgUrl, imagesStoredDestDir + article.getId(), null);
				} catch (IOException e) {
					// ignore the image
					continue;
				}
				picture.setPath(imagePathToUrl(path));
			}
			
			pictures.add(picture);
		}
		return pictures;
	}
	
	private String imagePathToUrl(String path) {
		if(path == null) {
			return path;
		}
		return path.replace(imagesStorePathPrefix, websiteHost).replace("\\", "/");
	}

	private List<Attach> extractAttaches(Article article) {
		if(article == null) {
			return null;
		}
		Elements attachLinks = extractAttachLinkElements();
		return extractAttachesFromAttachlinks(article, attachLinks);
	}
	
	private Elements extractAttachLinkElements() {
		Elements attachesAndUpdateTimeAndClickCount = articleEles.get(3).getElementsByTag("tbody").first()
				.getElementsByTag("tr").first().getElementsByTag("td");
		Element attachesEle = attachesAndUpdateTimeAndClickCount.first();
		return attachesEle.select("a[href]");
	}
	
	private List<Attach> extractAttachesFromAttachlinks(Article article, Elements attachLinks){
		if(article == null) {
			return null;
		}
		List<Attach> attaches = new ArrayList<Attach>(); 
		for(Element attachLink: attachLinks) {
			String attachName = formatAttachName(attachLink.text());
			
			Attach attach = new Attach();
			attach.setArticleId(article.getId());
			attach.setCategoryId(article.getCategoryId());
			attach.setPublisherId(article.getPublisherId());
			attach.setPublishedTime(article.getPublishedTime());
			attach.setUpdatedTime(article.getUpdatedTime());
			attach.setBaseClick(article.getBaseClick());
			attach.setAdditionClick(0);
			attach.setFilename(attachName);
			attach.setGmtCreated(article.getGmtCreated());
			attach.setGmtModified(article.getGmtModified());
			attach.setArticleUrl(article.getUrl());
			String attachUrl = attachLink.attr("href");
			
			try {
				attachUrl = fillUrl(attachUrl);
				attach.setUrl(attachUrl);
				if(extractAttachContent) {
					try {
						String content = extractFileTextFromUrl(attach.getUrl());
						int digestLength = content.length() < DIGEST_MAX_LENGTH? content.length(): DIGEST_MAX_LENGTH;
						String digest = content.substring(0, digestLength);
						attach.setDigest(digest);
						attach.setContent(content);
					} catch (Exception e) {
						attach.setDigest("");
						attach.setContent("");
						logger.error("Fail to extract content from " + attachUrl, e);
					}
				}
//			} catch (UnsupportedEncodingException e) {
//				logger.error("Fail to encode url " + attachUrl + " and extract content of the file.", e);
			} catch (MalformedURLException e) {
				logger.error("Fail to fill url " + attachUrl + " and extract content of the file.", e);
			}	
			attaches.add(attach);
		}
		return attaches;
	}
	
	private String extractFileTextFromUrl(String url) throws MalformedURLException, Exception {
		Extractor extractor = new CommonExtractor();
		return extractor.extractText(new URL(url));
	}
	
	// Removes the blank symbol at the beginning and end of the string.
	private String formatTitle(String title){
		// Removes the blank symbol at the beginning and end of the string.
	    if(null!=title && !"".equals(title)){  
	    	title = title.replaceAll("^[　*| *| *|//s*]*", "").replaceAll("[　*| *| *|//s*]*$", "");
	    }
	    return title;
	}
	
	private String formatAttachName(String attachName) {
		if(attachName.startsWith("・")) {
			return attachName.replaceFirst("・", "");
		}
		return attachName;
	}
	
	public static void main(String[] args) {
		String path = "C:\\var\\szuboardse\\www\\store\\files\\image\\45455\\454545415.gif";
		System.out.println(imagesStorePathPrefix);
		System.out.println(websiteHost);
		String result = path.replace(imagesStorePathPrefix, websiteHost);
		System.out.println(result.replace("\\", "/"));
	}
}
