package cn.donespeak.szuboardse.base;

import java.util.HashMap;
import java.util.Map;

public class ResponseMapJson extends ResponseBaseJson<Map<String, Object>>{
	public ResponseMapJson(){
		this.code = SUCCESS_CODE;
		this.msg = SUCCESS_MSG;
		this.data = new HashMap<String, Object>();
	}	
	public ResponseMapJson(String code, String msg) {
		this.code = code;
		this.msg = msg;
		this.data = null;
	}	
	public ResponseMapJson(String code, String msg, Map<String, Object> data){
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	public void push(String field, Object value){
		if(data != null) {
			data.put(field, value);
		}
	}
	
	public void delete(String field){
		if(data != null) {
			data.remove(field);
		}
	}
	
	public void empty(){
		data = null;
	}
	
	public void reset(){
		this.code = SUCCESS_CODE;
		this.msg = SUCCESS_MSG;
		this.data = new HashMap<String, Object>();
	}
}
