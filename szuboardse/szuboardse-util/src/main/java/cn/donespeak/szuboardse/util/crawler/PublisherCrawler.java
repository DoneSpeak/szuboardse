package cn.donespeak.szuboardse.util.crawler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.util.DateUtil;

public class PublisherCrawler {
	private final static String PUBLISHER_LIST_URL = "http://www1.szu.edu.cn/board/userlist.asp";
	private final static String CHARSET = "GB2312";
	
	public static List<Publisher> listPublishers() throws IOException {
		Document doc = getPageDocument();
		return extractPublishers(doc);
	}
	
	public static List<Publisher> listPublishersFromFile(File file) throws IOException {
		return listPublishersFromFile(file, CHARSET);
	}
	
	public static List<Publisher> listPublishersFromFile(File file, String charset) throws IOException {
		Document doc = getPageDocumentFromFile(file, charset);
		return extractPublishers(doc);
	}
	
	public static Document getPageDocument() throws IOException {
		Document doc = Jsoup.connect(PUBLISHER_LIST_URL).get();
		return doc;
	}
	
	public static Document getPageDocumentFromFile(File file, String charset) throws IOException {
		return Jsoup.parse(file, charset);
	}
	
	private static List<Publisher> extractPublishers(Document doc) {
		
		Elements optionEles = doc.select("option");
		List<String> publisherNames = optionEles.eachAttr("value");
		
		List<Publisher> publishers = new ArrayList<Publisher>(publisherNames.size());
		int id = 1;
		for(String name: publisherNames) {
			name = formatName(name);
			if(name == null) {
				continue;
			}
			Publisher publisher = new Publisher(id ++, name);
			publisher.setGmtCreated(DateUtil.getNow());
			publishers.add(publisher);
		}
		return publishers;
	}
	
	private static String formatName(String name) {
		int index = name.indexOf("#");
		if(index < 0){
			return null;
		}
		return name.substring(0, name.indexOf("#"));
	}
	private static List<String> publishersToInsertSql(List<Publisher> publishers) {
		List<String> insertSqls = new ArrayList<String>();
		for(Publisher publisher: publishers) {
			insertSqls.add(publisherToInsert(publisher));
		}
		return insertSqls;
	}
	private static String publisherToInsert(Publisher publisher) {
		String sql = "insert into publisher(id, name, gmt_created) values (" + publisher.getId() + ",'"
				+ publisher.getName() + "'," + publisher.getGmtCreated() + ");";
		return sql;
	}

	public static void main(String[] args) {
		String path = "/home/ygr/选择发文单位.html";
		List<Publisher> publishers = null;
		try {
			publishers = PublisherCrawler.listPublishersFromFile(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		for(Publisher p: publishers) {
			System.out.println(p.toString());
		}
		System.out.println("一共有：" + publishers.size());
		for(String sql: publishersToInsertSql(publishers)) {
			System.out.println(sql);
		}
	}
	
}
