package cn.donespeak.szuboardse.util;

import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;

public class CustomClaims extends DefaultClaims{
	// pre expiration 定义 jwt 的预过期时间，超过该时间并没有失效。但建议更新 jwt
	public static final String PEXP = "pexp";
	
    public CustomClaims setPreExpiration(Date pexp) {
        setValue(PEXP, pexp);
        return this;
    }

    public Date getPreExpiration() {
        return getDate(PEXP);
    }
}
