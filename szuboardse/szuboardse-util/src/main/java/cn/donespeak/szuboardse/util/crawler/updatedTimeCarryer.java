package cn.donespeak.szuboardse.util.crawler;

import org.jsoup.select.Elements;

public class updatedTimeCarryer {
	private long articleId;
	private long updatedTime;
	private Elements articleEles;
	private ArticleListItem articleListItem;
	
	/* getter and setter */
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	public long getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	public Elements getArticleEles() {
		return articleEles;
	}
	public void setArticleEles(Elements articleEles) {
		this.articleEles = articleEles;
	}
	public ArticleListItem getArticleListItem() {
		return articleListItem;
	}
	public void setArticleListItem(ArticleListItem articleListItem) {
		this.articleListItem = articleListItem;
	}
}
