package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.similarities.Similarity;

import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.util.ResultUtil;
/**
 * Document 域的规定：
 * id、URL、日期、用于排序的文本 -> store, no analyzed, no norms, no 项向量
 * 标题、摘要 -> store, analyzed, 项向量: WITH_POSITION_OFFSETS（用于高亮）
 * 正文 -> no store, analyzed, 项向量: WITH_POSITION_OFFSETS（用于高亮）
 * 文档类型、数据库主键 -> store, no analyzed, no 项向量
 * 隐藏关键字 -> no store, not analyzed, no 项向量
 * @author DoneSpeak
 *
 */
public class ContentIndexer {
	IndexWriter indexWriter;
    
	public ContentIndexer() throws IOException {
		init();
	}
	
	public void init() throws IOException {
		IndexWriterConfig config = new IndexWriterConfig(LuceneHelper.getChineseAnalyzer());
		indexWriter = new IndexWriter(LuceneHelper.getContentIndexFSD(), config);
	}
	
	public void addArticles(List<Article> articles) throws IOException {
		if(articles == null || articles.size() == 0){
			return;
		}
		for(Article article: articles) {
			addArticle(article);
		}
	}
	
	public void addArticle(Article article) throws IOException {
		if(article == null){
			return;
		}
		indexWriter.addDocument(createDocFromArticle(article));
	}
	
	public void addAttaches(List<Attach> attaches) throws IOException {
		if(attaches == null || attaches.size() == 0){
			return;
		}
		for(Attach attach: attaches) {
			addAttach(attach);
		}
	}
	
	public void addAttach(Attach attach) throws IOException {
		if(attach == null){
			return;
		}
		indexWriter.addDocument(createDocFromAttach(attach));
	}
	
	public void updateArticle(Article article) throws IOException {
		Document doc = createDocFromArticle(article);
		String articleId = ResultUtil.idWithPrefix(article.getId(), Region.ARTICLE);
		indexWriter.updateDocument(new Term(FieldName.ID.name(), articleId), doc);
	}
	
	public void updateAttach(Attach attach) throws IOException {
		Document doc = createDocFromAttach(attach);
		String attachId = ResultUtil.idWithPrefix(attach.getId(), Region.ATTACH);
		indexWriter.updateDocument(new Term(FieldName.ID.name(), attachId), doc);
	}
	
	public void cleanAttachesInArticle(long articleId) throws IOException {
		indexWriter.deleteDocuments(new Term(FieldName.ARTICLE_ID.name(), articleId +  ""));
	}
	
	public void deleteAll() throws IOException {
		indexWriter.deleteAll();
	}
	
	public void commit() throws IOException {
		indexWriter.commit();
	}
	
	public void close() throws IOException {
		indexWriter.close();
	}
	
	private Document createDocFromArticle(Article article) {
		Document doc = new Document();
		// StringField 索引但不分词
		// TODO@DoneSpeak 需要设置为不记录 项向量
		doc.add(new StringField(FieldName.ID.name(), ResultUtil.idWithPrefix(article.getId(), Region.ARTICLE),
				Store.YES));
		doc.add(new StoredField(FieldName.URL.name(), article.getUrl()));
		
		doc.add(new TextField(FieldName.TITLE.name(), article.getTitle(), Field.Store.YES));		
		doc.add(new Field(FieldName.CONTENT.name(), article.getContent(), TextField.TYPE_NOT_STORED));
		
		doc.add(new StringField(FieldName.CATEGORY.name(), article.getCategoryId() + "", Store.YES));
		doc.add(new StringField(FieldName.PUBLISHER.name(), article.getPublisherId() + "", Store.YES));
		
		doc.add(new StoredField(FieldName.PUBLISHED_TIME.name(), article.getPublishedTime() + ""));
		doc.add(new LongPoint(FieldName.PUBLISHED_TIME.name(), article.getPublishedTime()));

		doc.add(new StoredField(FieldName.UPDATED_TIME.name(), article.getUpdatedTime() + ""));
		doc.add(new LongPoint(FieldName.UPDATED_TIME.name(), article.getUpdatedTime()));
		
		// 由于点击次数需要不断更新，较有挑战性，暂时不做
//		doc.add(new Field(FieldName.CLICK.name(), article.getBaseClick() + article.getAdditionClick() + "", 
//				TextField.TYPE_STORED));
		doc.add(new StringField(FieldName.REGION.name(), Region.ARTICLE.code() + "", Store.YES));
		return doc;
	}

	private Document createDocFromAttach(Attach attach) {
		Document doc = new Document();
		// StringField 索引但不分词无项向量
		// TextField 索引并分词
		// SoredField 不索引不分词
		// TODO@DoneSpeak 需要设置为不记录 项向量
		doc.add(new StringField(FieldName.ID.name(), ResultUtil.idWithPrefix(attach.getId(), Region.ATTACH),
				Store.YES));
		doc.add(new StoredField(FieldName.URL.name(), attach.getUrl()));

		doc.add(new TextField(FieldName.ARTICLE_ID.name(), attach.getArticleId() + "", Field.Store.YES));
		
		doc.add(new TextField(FieldName.TITLE.name(), attach.getFilename(), Field.Store.YES));
		doc.add(new Field(FieldName.CONTENT.name(), attach.getContent(), TextField.TYPE_NOT_STORED));
		
		doc.add(new StringField(FieldName.CATEGORY.name(), attach.getCategoryId() + "", Store.YES));
		doc.add(new StringField(FieldName.PUBLISHER.name(), attach.getPublisherId() + "", Store.YES));
		
		doc.add(new StoredField(FieldName.PUBLISHED_TIME.name(), attach.getPublishedTime() + ""));
		doc.add(new LongPoint(FieldName.PUBLISHED_TIME.name(), attach.getPublishedTime()));

		doc.add(new StoredField(FieldName.UPDATED_TIME.name(), attach.getUpdatedTime() + ""));
		doc.add(new LongPoint(FieldName.UPDATED_TIME.name(), attach.getUpdatedTime()));

		doc.add(new StoredField(FieldName.ARTICLE_URL.name(), attach.getArticleUrl() + ""));
		// 由于点击次数需要不断更新，较有挑战性，暂时不做
//		doc.add(new Field(FieldName.CLICK.name(), article.getBaseClick() + article.getAdditionClick() + "", 
//				TextField.TYPE_STORED));
		doc.add(new StringField(FieldName.REGION.name(), Region.ATTACH.code() + "", Store.YES));
		return doc;
	}
	
	public static void main(String[] args) throws IOException {
		ContentIndexer indexer = new ContentIndexer();
		Document doc = new Document();
		doc.add(new TextField("txt", "这是内容", Store.YES));
		indexer.indexWriter.addDocument(doc);
		indexer.commit();
		indexer.close();
	}
}
