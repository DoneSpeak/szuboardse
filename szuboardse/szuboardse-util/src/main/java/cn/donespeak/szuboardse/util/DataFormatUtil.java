package cn.donespeak.szuboardse.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * @author Donespeak
 * @data 2017/10/25 14:00
 */
public class DataFormatUtil {
	
	/**
	 * create list from file, a line as an element
	 * @param filePath A existing local plain text file
	 * @return a list form by the lines of the file
	 * @throws IOException 
	 */
	public static List<String> getStringListFromFile(String filePath) throws IOException{
		ArrayList<String> list = null;
        try (BufferedReader reader = new BufferedReader(
        		new InputStreamReader(new FileInputStream(filePath)))){
            String line = null;
            list = new ArrayList<String>();
            while((line = reader.readLine()) != null){
            	list.add(line);
            }
        }
        return list;
	}
	
	/**
	 * write strings in collection to destination file.
	 * @throws IOException 
	 */
	public static void strings2File(Collection<String> collection, String destFilePath) throws IOException{
		objString2File(new ObjectStringizer<String>(){
			@Override
			public String stringize(String t) {
				return t.toString();
			}			
		}, collection, destFilePath);
	}

	/**
	 * read data from srcFilePath，format each line according to LineFormater，and output to distFilePath
	 * @throws IOException 
	 */
	public static int formatFile(LineFormater formater, String srcFilePath, String distFilePath) throws IOException{
		int count = 0;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(srcFilePath)));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(distFilePath)));) {
            String line = null;
            while((line = reader.readLine()) != null){
            	Collection<String> strs = formater.getLinesToCollection(line);
            	for(String str: strs){
            		writer.write(str);
            		writer.newLine();
            		count ++;
            	}
                writer.flush();
            }
        }
        return count;
	}

	public static int removeSameLineInFile(String srcFilePath) throws IOException{
		Random random = new Random();
		String tempFile = srcFilePath + "-temp-" + random.nextInt(100000000) + ".gr";
		HashSet<String> lineSet = new HashSet<String>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(srcFilePath)));
        	BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile)));) {
            String line = null;
            while((line = reader.readLine()) != null){
            	lineSet.add(line);
            }
            for(String str: lineSet){
            	writer.write(str);
        		writer.newLine();
            }
            writer.flush();
            new File(srcFilePath).delete();
            new File(tempFile).renameTo(new File(srcFilePath));
        }
        return lineSet.size();
	}
	
	/**
	 * output object to distFilePath
	 * @throws IOException 
	 */
	public static <T> void objList2ObjectFile(List<T> list, String destFilePath) throws IOException{
		File file =new File(destFilePath);
        try (ObjectOutputStream objOut=new ObjectOutputStream(new FileOutputStream(file))) {
            for(T t: list){
            	objOut.writeObject(t);
            }
            objOut.flush();
            objOut.close();
        }
	}
	
	public static <T> void objString2File(ObjectStringizer<T> objectStringizer, 
			Collection<T> collection, String destFilePath) throws IOException{
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(destFilePath)))) {            
            for(T t: collection){
            	String line = objectStringizer.stringize(t);
            	writer.write(line);
            	writer.newLine();
            }
        	writer.flush();
        }
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> getObjectsFromFile(T obj, String filePath) 
			throws ClassNotFoundException, IOException{
        File file =new File(filePath);
        ArrayList<T> list = new ArrayList<T>();
        try (ObjectInputStream objIn =new ObjectInputStream(new FileInputStream(file))) {
            T t = null;
            while((t = (T)objIn.readObject()) != null){
            	list.add(t);
            }
        }
        return list;
	}
	
	/**
	 * 移除空白符号(包括中英文空白符号)
	 * @param line
	 * @return
	 */
	public static String removeEmpty(String line) {
		// TODO@DoneSpeak
		return line;
	}
}
