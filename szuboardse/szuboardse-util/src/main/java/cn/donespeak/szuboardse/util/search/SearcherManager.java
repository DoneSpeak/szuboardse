package cn.donespeak.szuboardse.util.search;

import java.io.IOException;

import org.apache.log4j.Logger;

public class SearcherManager {
	Logger logger = Logger.getLogger(SearcherManager.class);
	public ContentSearcher getContentSearcher(boolean blocking) {
		// TODO@DoneSpeak 需要修改优化，该实现仅仅为临时实现罢了
		try {
			ContentSearcher.refreshManager(blocking);
		} catch (IOException e) {
			logger.warn("Fail to refresh contentSearcher", e);
		}
		return new ContentSearcher();
	}
	
	public QuerySearcher getQuerySearcher(boolean blocking) {
		// TODO@DoneSpeak 需要修改优化，该实现仅仅为临时实现罢了
		try {
			QuerySearcher.refreshManager(blocking);
		} catch (IOException e) {
			logger.warn("Fail to refresh contentSearcher", e);
		}
		return new QuerySearcher();
	}
}
