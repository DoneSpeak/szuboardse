package cn.donespeak.szuboardse.util;

import java.util.Collection;

public interface LineFormater {
	Collection<String> getLinesToCollection(String line);
}
