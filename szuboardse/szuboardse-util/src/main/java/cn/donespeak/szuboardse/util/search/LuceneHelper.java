package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.nio.file.FileSystems;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import cn.donespeak.szuboardse.util.SystemUtil;
import cn.donespeak.szuboardse.util.search.ikanalyzer.GIKAnalyzer;

public class LuceneHelper {

	public static final String CONTENT_INDEX_PATH;
	public static final String RELATIVE_QUERY_INDEX_PATH;
	
	static {
		if(SystemUtil.isLinux()) {
			CONTENT_INDEX_PATH = "/var/szuboardse/www/indexes/content-indexes/";
			RELATIVE_QUERY_INDEX_PATH = "/var/szuboardse/www/indexes/relative-query-indexes/";
		} else {
			// windows
			CONTENT_INDEX_PATH = "C:\\var\\szuboardse\\www\\indexes\\content-indexes\\";
			RELATIVE_QUERY_INDEX_PATH = "C:\\var\\szuboardse\\www\\indexes\\relative-query-indexes\\";
		}
	}
	
	public static Directory getContentIndexFSD() throws IOException {
		Directory directory = FSDirectory.open(
		         FileSystems.getDefault().getPath(CONTENT_INDEX_PATH));
		return directory;
	}
	
	public static IndexWriter getRelativeQueryIndexWriter() throws IOException {
		return new IndexWriter(getRelativeQueryFSDir(), getIndexWriterConfig());
	}

	public static Directory getRelativeQueryFSDir() throws IOException {
		Directory directory = FSDirectory.open(
		         FileSystems.getDefault().getPath(RELATIVE_QUERY_INDEX_PATH));
		return directory;
	}
	
	public static IndexWriterConfig getIndexWriterConfig() {
		IndexWriterConfig config = new IndexWriterConfig(getChineseAnalyzer());
		return config;
	}
		
	public static Analyzer getChineseAnalyzer() {
		return new GIKAnalyzer();
	}
	
	public static IndexSearcher getIndexReader() throws IOException {
		DirectoryReader directoryReader = DirectoryReader.open(getContentIndexFSD());
		IndexSearcher indexSearcher = new IndexSearcher(directoryReader);
		return indexSearcher;
	}
	
	public static Query getQuery(FieldName field, String keyword) throws ParseException {
		QueryParser queryParser = new QueryParser(field.name(), getChineseAnalyzer());
		Query query = queryParser.parse(keyword);
		return query;
	}
}
