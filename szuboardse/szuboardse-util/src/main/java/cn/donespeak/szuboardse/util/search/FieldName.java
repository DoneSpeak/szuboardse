package cn.donespeak.szuboardse.util.search;

import cn.donespeak.szuboardse.dto.SearchOption.Region;

public enum FieldName {
	ID,
	URL,
	TITLE,
	CONTENT,
	CATEGORY,
	PUBLISHER,
	PUBLISHED_TIME,
	UPDATED_TIME,
	CLICK,
	REGION,

	ARTICLE_ID,
	ARTICLE_URL,
	QUERY
	;
	
	public static String toArticleFieldIdValue(long id) {
		return Region.ARTICLE.name() + "#" + id;
	}
	
	public static long articleFieldIdValueToId(String fieldId) {
		String id = fieldId.replaceFirst(Region.ARTICLE.name() + "#", "");
		return Long.parseLong(id);
	}
	
	public static String toAttachFieldIdValue(long id) {
		return Region.ARTICLE.name() + "#" + id;
	}
	
	public static long attachFieldIdValueToId(String fieldId) {
		String id = fieldId.replaceFirst(Region.ATTACH.name() + "#", "");
		return Long.parseLong(id);
	}
}
