package cn.donespeak.szuboardse.util.crawler;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.util.board.CategoryCache;
import cn.donespeak.szuboardse.util.board.PublisherCache;
import cn.donespeak.szuboardse.util.crawler.BoardSearchOption.DayyOption;

/**
 * 
 * @author Donespeak
 * Reference: 
 * [jsoup中select用法详解](http://blog.csdn.net/zhangconglin/article/details/50483733)
 * [Jsoup教程,jsoup开发指南,jsoup中文使用手册,jsoup中文文档](http://blog.csdn.net/xmtblog/article/details/76204714)
 */
public class ArticleListCrawler {
	private static Logger logger = Logger.getLogger(ArticleListCrawler.class);
	
	public static final String SZU_BOARD_URL = "http://www1.szu.edu.cn/board/";
	public static final String CHARSET = "GB2312";
	
	private Map<String, String> boardSearchParams;
	
	public ArticleListCrawler() {}
	
	public List<ArticleListItem> listArticleItems(String html) throws IOException{
		logger.info("start crawl article list from String");
		Document doc = Jsoup.parse(html, SZU_BOARD_URL);
		return parseArtcleItems(doc);
	}
	
	public List<ArticleListItem> listArticleItems(File file) throws IOException{
		logger.info("start crawl article list from " + file.getAbsolutePath());
		Document doc = Jsoup.parse(file, CHARSET, SZU_BOARD_URL);
		return parseArtcleItems(doc);
	}
	
	public List<ArticleListItem> listArticleItems(BoardSearchOption option) throws IOException{
		boardSearchParams = option.toParamMap();
		logger.info("start crawl article list from " + SZU_BOARD_URL + " with params: " + boardSearchParams);
		Document doc = getPageDocument();
		List<ArticleListItem> items = parseArtcleItems(doc);
		return items;
	}
	
	private List<ArticleListItem> parseArtcleItems(Document doc){
		if(doc.getElementsByTag("tbody").size() < 9) {
			return new ArrayList<ArticleListItem>();
		}
		List<ArticleListItem> items = new ArrayList<ArticleListItem>();
		Elements articleListItemEles = doc.getElementsByTag("tbody").get(8).getElementsByTag("tr");
		int articleRecordStartIndex = 2;
		for(int i = articleRecordStartIndex; i < articleListItemEles.size(); i ++) {
			Element itemEle = articleListItemEles.get(i);
			try {
				Elements tds = itemEle.select("td");
				String categoryName = tds.get(1).text();
				String publisherName = tds.get(2).text();
				Element titleLink = tds.get(3).selectFirst("a[href]");
				Element titleB = titleLink.getElementsByTag("b").first();
				String title = "";
				if(titleB != null) {
					title = titleB.text();
				} else {
					title = titleLink.text();
					if(title.startsWith("・")){
						title = title.replaceFirst("・", "");
					}
				}
				String url = titleLink.attr("href");
				long articleId = extractArticleIdFromUrl(url);
				url = formatUrl(url);
				
				ArticleListItem item = new ArticleListItem();
				item.setArticleId(articleId);
				Category category = mapCategory(categoryName);
				if(category == null) {
					category = mapCategory(extractCategoryOrPublisherName(categoryName));
					if(category == null) {
						// TODO@Donespeak 这是一个新的类别 - 添加到数据库
						logger.warn("Article " + title + " (" + url + ") has new category type " + categoryName);
						continue;
					}
				}
				Publisher publisher = mapPublisher(publisherName);
				if(publisher == null) {
					publisher = mapPublisher(extractCategoryOrPublisherName(publisherName));
					if(publisher == null) {
						// TODO@DoneSpeak 这是一个新的发文单位 - 添加到数据库
						logger.warn("The publisher " + publisherName + " is a new publisher type.");
						continue;
					}
				}
				item.setCategory(category);
				item.setPublisher(publisher);
				item.setTitle(title);
				item.setUrl(url);
				items.add(item);
			} catch (Exception e) {
				// do nothings
				logger.error("Fail to extract article item", e);
			}
		}
		return items;
	}
	
	private Category mapCategory(String categoryName) {
		return CategoryCache.get(categoryName);
	}
	
	private Publisher mapPublisher(String publisherName) {
		return PublisherCache.get(publisherName);
	}
	
	private String extractCategoryOrPublisherName(String name) {
		return Jsoup.parse(name).text();
	}
	
	private long extractArticleIdFromUrl(String url) {
		int index = url.lastIndexOf("=");
		return Long.parseLong(url.substring(index + 1).trim());
	}
	
	private String formatUrl(String url){
		if(!url.startsWith(SZU_BOARD_URL)) {
			if(url.startsWith("/")) {
				url = SZU_BOARD_URL + url.substring(1);
			} else {
				url = SZU_BOARD_URL + url;
			}
		}
		return url;
	}

	private Document getPageDocument() throws IOException {
		Connection conn = Jsoup.connect(SZU_BOARD_URL);
		for(Entry<String, String> entry: boardSearchParams.entrySet()) {
			conn.data(entry.getKey(), entry.getValue());
		}
		return conn.post();
	}
}
