package cn.donespeak.szuboardse.util;

import java.util.Base64;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import cn.donespeak.szuboardse.base.constant.AuthConstant;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * reference:
 * [jwt](https://yujunhao8831.github.io/2017/06/27/jwt/)
 * [RESTful登录设计（基于Spring及Redis的Token鉴权）](http://www.scienjus.com/restful-token-authorization/)
 */
public class JwtUtil {
	
	public static enum TokenState {
		// 过期的
		EXPIRED,
		// 无效的
		INVALID,
		// 有效的
		VALID;
	}
	
	private static Logger logger = Logger.getLogger(JwtUtil.class);
	
	// 默认的 token 有效时间： 30天
	private static long DEFAULT_TTLMILLIS = 30L * 24 * 60 * 60 * 1000;
	// 测试使用 5分钟
//	private static long DEFAULT_TTLMILLIS = 15 * 60 * 1000;

	public static String createLoginJWT(long userId) {
		return createLoginJWT(userId, DEFAULT_TTLMILLIS);
	}
	
	/**
	 * 创建jwt, 设置过期时间
	 * reference: [jwt](https://yujunhao8831.github.io/2017/06/27/jwt
	 * @param userId
	 * @param ttlMillis 有效时长，毫秒为单位
	 * @return
	 */
	public static String createLoginJWT(long userId, long ttlMillis) {
		return createLoginJWT(userId + "", ttlMillis);
	}
	
	public static String createLoginJWT(String userId, long ttlMillis) {
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		long nowMillis = DateUtil.getNow();
		Date now = new Date(nowMillis);
		SecretKey key = generalKey();
		JwtBuilder builder = Jwts.builder()
				.setId(CommonUtil.getUUID()) // 当前 token 的唯一标识
				.setIssuedAt(now)            // token 创建时间， Unix 时间戳格式
				.setSubject(userId)         // 主题，接收方
				.signWith(signatureAlgorithm, key);
		if (ttlMillis >= 0) {
			long expMillis = nowMillis + ttlMillis; // 设置失效时间
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}
		return builder.compact();
	}

	/**
	 * 由字符串生成加密key
	 * @return
	 */
	private static SecretKey generalKey() {
		String jwtSecret = AuthConstant.JWT_SECRECT;
		byte[] encodedKey = Base64.getEncoder().encode(jwtSecret.getBytes());
		SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
		return key;
	}
	
	public static String refreshLoginJWT(String token) {
		return refreshLoginJWT(token, DEFAULT_TTLMILLIS);
	}
	
	public static String refreshLoginJWT(String token, long ttlMillis) {
		Claims claims = parseJWT(token);
		if(validateJWT(claims) != TokenState.VALID) {
			// 必需是有效的token才能 refresh
			return null;
		}
		String userId = claims.getSubject();
		return createLoginJWT(userId, ttlMillis);
	}
	
	/**
	 * 验证token
	 * 
	 * @param token
	 * @return
	 */
	public static TokenState validateJWT(String token) {
		if (StringUtils.isEmpty(token)) {
			return TokenState.INVALID;
		}
		Claims claims = parseJWT(token);
		if (claims == null || null == claims.getExpiration()) {
			return TokenState.INVALID;
		} else if (claims.getExpiration().before(DateUtil.now())) {
			return TokenState.EXPIRED;
		}
		return TokenState.VALID;
	}
	
	public static boolean jwtIsValid(Claims claims) {
		return validateJWT(claims) == TokenState.VALID;
	}
	
	public static TokenState validateJWT(Claims claims) {
		if (claims == null || null == claims.getExpiration()) {
			return TokenState.INVALID;
		} else if (claims.getExpiration().before(DateUtil.now())) {
			return TokenState.EXPIRED;
		}
		return TokenState.VALID;
	}
	
	
	public static Claims parseJWT(String token) {
		if (StringUtils.isEmpty(token)) {
			return null;
		}
		SecretKey key = generalKey();
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
		} catch (ExpiredJwtException e) {
			claims = TokenClaims.getInstance(token);
			logger.debug("the user(" + claims.getSubject() + ") token expired in ( "
					+ DateUtil.format(claims.getExpiration()) + " ) ");
		} catch (Exception e) {
			logger.error("Fail to parse jwt", e);
		}
		return claims;
	}

	public static void main(String[] args) {
//		String token = JwtUtil.createLoginJWT(123456, 60000);
//		String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJmOWQzMDc1Zi05ZGI4LTRjOWYtOTRkMS04MzIxMTQ0N2E5NWYiLCJpYXQiOjE1MjIwNTUxNzcsInN1YiI6IjEyMzQ1NiIsImV4cCI6MTUyMjA1NTE4M30.03754ObokC8tgSnuH8e7ZMgqKxHMySE03GRvxmo-Gzs";
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzYTlkZjgyZS0xMzQzLTQwOTMtYmI0Ni04ZThiNWQ0MjlmMzkiLCJpYXQiOjE1MjIwNTU1NDgsInN1YiI6IjEyMzQ1NiIsImV4cCI6MTUyMjA1NTYwOH0.03754ObokC8tgSnuH8e7ZMgqKxHMySE03GRvxmo-Gzs";
		System.out.println("token:" + token);
		System.out.println(JwtUtil.parseJWT(token));
		System.out.println(JwtUtil.validateJWT(token));
	}
}
