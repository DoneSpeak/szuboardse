package cn.donespeak.szuboardse.util;

import java.util.ArrayList;
import java.util.List;

import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Attach;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.util.board.CategoryCache;
import cn.donespeak.szuboardse.util.board.PublisherCache;
import cn.donespeak.szuboardse.vo.ResultItem;

public class ResultUtil {
	
	public static List<ResultItem> articlesToItems(List<Article> articles) {
		if(articles == null) {
			return new ArrayList<ResultItem>();
		}
		List<ResultItem> items = new ArrayList<ResultItem>(articles.size());
		for(Article article: articles) {
			items.add(articleToResultItem(article));
		}
		return items;
	}
	
	public static ResultItem articleToResultItem(Article article) {
		ResultItem item = ResultItem.getArticleResultImte();
		
		item.setRegion(Region.ARTICLE);
		item.setArticleId(article.getId());
		item.setTitle(article.getTitle());
		item.setUrl(article.getUrl());
		
		item.setCategory(CategoryCache.get(article.getCategoryId()));
		item.setPublisher(PublisherCache.get(article.getPublisherId()));
		
		item.setPublishedTime(article.getPublishedTime());
		item.setUpdatedTime(article.getUpdatedTime());
		
		item.setDigest(article.getDigest());
		item.setIsfavorite(false);
		item.setArticleUrl(article.getUrl());
		// item.setPictures(null);
		// item.setAttaches(null);
		return item;
	}
	
	public static List<ResultItem> attachesToItems(List<Attach> attachs) {
		if(attachs == null) {
			return new ArrayList<ResultItem>();
		}
		List<ResultItem> items = new ArrayList<ResultItem>(attachs.size());
		for(Attach attach: attachs) {
			items.add(attachToResultItem(attach));
		}
		return items;
	}
	
	public static ResultItem attachToResultItem(Attach attach) {
		ResultItem item = ResultItem.getAttachResultImte();

		item.setAttachId(attach.getId());
		item.setArticleId(attach.getId());
		item.setTitle(attach.getFilename());
		item.setUrl(attach.getUrl());

		item.setCategory(CategoryCache.get(attach.getCategoryId()));
		item.setPublisher(PublisherCache.get(attach.getPublisherId()));
		
		item.setPublishedTime(attach.getPublishedTime());
		item.setUpdatedTime(attach.getUpdatedTime());
		
		item.setDigest(attach.getDigest());
		item.setIsfavorite(false);
		item.setArticleUrl(attach.getArticleUrl());
		return item;
	}
	
	public static String idWithPrefix(long id, Region region) {
		if(region != Region.ARTICLE && region != Region.ATTACH) {
			String regionName = region == null? null: region.name(); 
			throw new IllegalArgumentException("reigon should be ARTICLE or ATTACH, but get " + regionName);
		}
		return region.name() + "#" + id;
	}
	
	public static long idRemovePrefix(String idWithPrefix, Region region) {
		if(region != Region.ARTICLE && region != Region.ATTACH) {
			String regionName = region == null? null: region.name(); 
			throw new IllegalArgumentException("reigon should be ARTICLE or ATTACH, but get " + regionName);
		}
		String id = idWithPrefix.replaceFirst(region.name() + "#", "");
		return Long.parseLong(id);
	}
}
