package cn.donespeak.szuboardse.base.responsecode;

/**
 * 继承了 BaseCode 的不可变类
 * @author donespeak
 *
 */
public class CodeEntry implements BaseCode{

	private final String category;	
	private final String subcode;
	private final String message;
	
	public CodeEntry(String category, String subcode, String message) {
		this.category = category;
		this.subcode = subcode;
		this.message = message;
	}
	
	@Override
	public String category() {
		return category;
	}
	@Override
	public String subcode() {
		return subcode;
	}
	@Override
	public String code() {
		return category + ":" + subcode;
	}
	@Override
	public String message() {
		return message;
	}
}
