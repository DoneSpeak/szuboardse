package cn.donespeak.szuboardse.base;

public interface ResponseJson {

	public static final String SUCCESS_CODE = "success";
	public static final String SUCCESS_MSG = "Success";
	
	String getCode();
	String getMsg();
	Object getData();
}
