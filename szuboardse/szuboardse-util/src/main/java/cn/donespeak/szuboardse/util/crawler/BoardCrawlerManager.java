package cn.donespeak.szuboardse.util.crawler;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BoardCrawlerManager {
	private static long latestArticleId;
	
	/**
	 *  getter and setter 
	 */
	public static long getLatestArticleId() {
		return latestArticleId;
	}
	public static void setLatestArticleId(long latestArticleId) {
		BoardCrawlerManager.latestArticleId = latestArticleId;
	}
}
