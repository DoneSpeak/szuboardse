package cn.donespeak.szuboardse.base.responsecode;

public enum SearchCode {
	LUCENE_PARSE_ERROR("anaylze_error", "分析失败"),
	SEARCH_FAIL("search_fail", "检索失败");
	
	private static final String CATEGORY = "search";
	public final CodeEntry entry;	
	SearchCode(String subcode, String message) {
		this.entry = new CodeEntry(CATEGORY, subcode, message);
	}
}
