package cn.donespeak.szuboardse.base.responsecode;

public enum Auth {
	USER_NOT_FOUND("user_not_found", "用户不存在"), 
	EMAIL_OR_PASSWORD_INCORRECT("email_or_password_incorrect", "邮箱或者密码错误")
	;
	
	private final String CATEGORY = "auth";
	public final CodeEntry entry;	
	Auth(String subcode, String message) {
		this.entry = new CodeEntry(CATEGORY, subcode, message);
	}
}
