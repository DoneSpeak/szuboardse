package cn.donespeak.szuboardse.util.board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import cn.donespeak.szuboardse.entity.Category;

/**
 * TODO@Donespeak: 计算该cache所需要耗费的内存，并评估该cache的线程安全性，评估读写效率
 * @author ygr
 *
 */
public class CategoryCache {
	private static Logger logger = Logger.getLogger(CategoryCache.class);
	// 对比这样的实现和通过 redis 这样的缓存技术的实现的区别
	// TODO@DoneSpeak 目前还没有做到保持这三个数据的同步
	private static CopyOnWriteArrayList<Category> categories = null;
	private static List<Category> unmodifiedCategories = null;
	private static ConcurrentHashMap<String, Category> nameCategoryMap = null;
	private static ConcurrentHashMap<Integer, Category> idCategoryMap = null;

	private static final Object lock = new Object();
	
	private CategoryCache() {}
	
	public static void initCache(List<Category> categories) {
		logger.info("Initialize CategoryCache");
		updateCache(categories);
	}
	// TODO@DoneSpeak: 思考一个线程安全类的设计原则是什么
	// TODO@DoneSepak: 如果需要使用外面传入的引用类型类添加数据应该如何操作
	// TODO@DoneSpeak: 是否需要考虑传入的引用类型类在外面被修改的问题
	public static void updateCache(List<Category> newCategories) {
		ConcurrentHashMap<String, Category> tmpNameCategoryMap = new ConcurrentHashMap<String, Category>();
		ConcurrentHashMap<Integer, Category> tempIdCategoryMap = new ConcurrentHashMap<Integer, Category>();
		for(Category category: newCategories) {
			tmpNameCategoryMap.put(category.getName(), category);
			tempIdCategoryMap.put(category.getId(), category);
		}
		synchronized(lock) {
			categories = new CopyOnWriteArrayList<Category>(newCategories);
			nameCategoryMap = tmpNameCategoryMap;
			idCategoryMap = tempIdCategoryMap;
			// update unmodified categories
			unmodifiedCategories = Collections.unmodifiableList(categories);
		}
	}
	
	public static void addCategory(Category category) {
		synchronized(lock) {
			categories.add(category);
			idCategoryMap.put(category.getId(), category);
			nameCategoryMap.put(category.getName(), category);
			// update unmodified categories
			unmodifiedCategories = Collections.unmodifiableList(categories);
		}
	}
	
	// TODO@DoneSpeak: 内部的数据是否可以直接暴露到外面，以及是否需要返回副本
	/**
	 * It will always return the unmodified copy of the original category list.
	 * However, It will get same copy unless you push a new category or update the category list.
	 * @return An unmodified copy of the categories.
	 */
	public static List<Category> listCategories() {
		return unmodifiedCategories;
	}
	
	public static Category get(int categoryId) {
		return idCategoryMap.get(categoryId);
	}
	
	public static Category get(String categoryName) {
		return nameCategoryMap.get(categoryName);
	}
}
