package cn.donespeak.szuboardse.util.extractor;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class CommonExtractor implements Extractor {

	@Override
	public String extractText(File file) throws Exception {
		Extractor extractor = getExtractor(file.getName());
		return extractor.extractText(file);
	}

	@Override
	public String extractText(URL url) throws Exception {
		Extractor extractor = getExtractor(url.getPath());
		return extractor.extractText(url);
	}

	@Override
	public void extractText2File(File srcFile, File destFile) throws Exception {
		Extractor extractor = getExtractor(srcFile.getName());
		extractor.extractText2File(srcFile, destFile);
	}

	@Override
	public void extractText2File(URL url, File destFile) throws IOException, UnExtractableException {
		Extractor extractor = getExtractor(url.getPath());
		extractor.extractText2File(url, destFile);
	}
	
	private Extractor getExtractor(String path) throws UnExtractableException {
		TextType type = TextType.getTextType(path);
		if(type == null) {
			throw new UnExtractableException("unsupported text type.");
		}
		Extractor extractor = ExtractorFactory.getExtracter(type);
		if(extractor == null) {
			throw new UnExtractableException("unsupported text type.");
		}
		return extractor;
	}
}
