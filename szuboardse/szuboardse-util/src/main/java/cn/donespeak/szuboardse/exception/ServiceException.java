package cn.donespeak.szuboardse.exception;

import cn.donespeak.szuboardse.base.responsecode.BaseCode;

public class ServiceException extends Exception{

	private static final long serialVersionUID = 7842009070720708559L;

	private String code;
	private String message;

	public ServiceException(){
		super();
	}
	
	
	public ServiceException(BaseCode code, String message) {
		super(message);
		this.code = code.code();
		this.message = message;
	}
	
	public ServiceException(BaseCode baseCode) {
		super(baseCode.message());
		this.code = baseCode.code();
		this.message = baseCode.message();
	}
	
	public ServiceException(BaseCode code, Throwable cause) {
        super(cause);
        this.code = code.code();
    }
	
	public ServiceException(Throwable cause) {
        super(cause);
    }

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
