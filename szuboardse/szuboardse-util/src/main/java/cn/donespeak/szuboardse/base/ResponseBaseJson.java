package cn.donespeak.szuboardse.base;

public abstract class ResponseBaseJson<E> implements ResponseJson{
	protected String code;
	protected String msg;
	protected E data;
	
	public static final String SUCCESS_CODE = "success";
	public static final String SUCCESS_MSG = "Success";
	
	public ResponseBaseJson() {
		this.code = SUCCESS_CODE;
		this.msg = SUCCESS_MSG;
	}
	/* getter and setter*/
	@Override
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public E getData() {
		return data;
	}
	public void setData(E data) {
		this.data = data;
	}
}
