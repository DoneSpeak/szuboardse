package cn.donespeak.szuboardse.util.crawler;

import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;

public class ArticleListItem {

	private Category category;
	private Publisher publisher;
	private String title;
	private String url;
	private long articleId;
	
	/* getter and setter */
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	
	@Override
	public String toString() {
		return "ArticleListItem [category=" + category + ", publisher=" + publisher + ", title=" + title + ", url="
				+ url + ", articleId=" + articleId + "]";
	}
}
