package cn.donespeak.szuboardse.util.msg;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.util.MailSSLSocketFactory;

/**
 * 发送邮件
 * @author donespeak
 * @date 2018/3/26
 */
// TODO@DoneSpeak: 还需要继续优化
public class EmailSender {
	private final String account;  
    private final String password;     
    private final String host;     
    private final String port;
    private final String protocol;
    private final String sender;
    
    public static final EmailSender DEFAULT_EMAIL_SENDER;
    
    static {
    	// TODO@DoneSpeak 从 properties 获取配置内容
    	String account = "carelative@163.com";
    	String password = "ItisCarelative18";
    	String host = "smtp.163.com";
    	String port = "465";
    	String protocol = "smtp";
    	String sender = "Carelative 服务";
    	
    	DEFAULT_EMAIL_SENDER = new EmailSender(account, password, host, port, protocol, sender);
    }
    
    public static EmailSender getDefaultEmailSender() {
    	return DEFAULT_EMAIL_SENDER;
    }
    
    public EmailSender(String account, String password, String host, String port, String protocol, String sender){
        this.account = account;
        this.password = password;
        this.host = host;
        this.port = port;
        this.protocol = protocol;
        this.sender = sender;
    }
    
    private class MyAuthenricator extends Authenticator{  
        String userName = null;
        String password = null;
        
        public MyAuthenricator(String userName, String password){  
            this.userName = userName;
            this.password = password; 
        } 
        
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {  
            return new PasswordAuthentication(userName, password);  
        }  
    }

    public void send(String recipient, String subject, String content) throws GeneralSecurityException, 
		MessagingException, UnsupportedEncodingException{
		
		MimeMessage mimeMessage = initMineMessage();
		mimeMessage.setFrom(new InternetAddress(account, sender));
		mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
		mimeMessage.setSentDate(new Date());
		mimeMessage.setSubject(subject);
		setEmailBody(mimeMessage, content);
		mimeMessage.saveChanges();
	
		Transport.send(mimeMessage);
	}
    
    private MimeMessage initMineMessage() throws GeneralSecurityException{
		Properties prop = new Properties();
		prop.setProperty("mail.transport.protocol", protocol);
		prop.setProperty("mail.smtp.host", host);
		prop.setProperty("mail.smtp.port", port);
		prop.setProperty("mail.smtp.auth", "true");

//		SSL is required for enterprise email
//		caused GeneralSecurityException
		MailSSLSocketFactory sf = new MailSSLSocketFactory();
		sf.setTrustAllHosts(true);
		
		prop.put("mail.smtp.ssl.enable", "true");
		prop.put("mail.smtp.ssl.socketFactory", sf);

//		singleton type. If you want to create two different session, please use getInstance.
		Session session = Session.getDefaultInstance(prop, new MyAuthenricator(account, password));
		session.setDebug(true);
		
		MimeMessage mimeMessage = new MimeMessage(session);
		return mimeMessage;
	}
        
    private void setEmailBody(MimeMessage mimeMessage, String content) throws MessagingException{
		// Multipart can contain MimeBodyPart
		Multipart mulitpart = new MimeMultipart();

		// MimeBodyPart can contain text, image and attachment
		MimeBodyPart body = new MimeBodyPart();
		// HTML-type content
		body.setContent(content, "text/html; charset=UTF-8");
		mulitpart.addBodyPart(body);

		// set content of email
		mimeMessage.setContent(mulitpart);
		// if send text, use following method
		// mimeMessage.setText(content);
	}
}
