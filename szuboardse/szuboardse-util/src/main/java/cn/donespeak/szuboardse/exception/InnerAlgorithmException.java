package cn.donespeak.szuboardse.exception;

public class InnerAlgorithmException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InnerAlgorithmException(){
		super();
	}
	
	public InnerAlgorithmException(String message) {
		super(message);
	}
	
	public InnerAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public InnerAlgorithmException(Throwable cause) {
        super(cause);
    }
}
