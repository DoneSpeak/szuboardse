package cn.donespeak.szuboardse.util;

import java.util.UUID;

public class CommonUtil {
	public static String getUUID(){
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
	public static void main(String[] args ){
		System.out.println(getUUID());
	}
}
