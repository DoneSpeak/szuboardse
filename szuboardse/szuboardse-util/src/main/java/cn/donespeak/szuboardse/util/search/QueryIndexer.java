package cn.donespeak.szuboardse.util.search;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

import cn.donespeak.szuboardse.util.DataFormatUtil;

public class QueryIndexer {
	private IndexWriter indexWriter;
	
	public QueryIndexer() throws IOException {
		init();
	}
	
	public void init() throws IOException {
		indexWriter = LuceneHelper.getRelativeQueryIndexWriter();
	}
	
	public boolean addQuery(String query) throws IOException {
		if(query == null) {
			return false;
		}
		query = DataFormatUtil.removeEmpty(query);
		if(query.length() == 0) {
			return false;
		}
		Document doc = new Document();
		// TODO@DoneSpeak: 如何防止相同的query提交
		doc.add(new Field(FieldName.QUERY.name(), query, TextField.TYPE_STORED));
		indexWriter.addDocument(doc);
		return true;
	}
	
	public void addQueryAndClose(String query) throws IOException {
		addQuery(query);
		commit();
		close();
	}
	
	public void commit() throws IOException {
		indexWriter.commit();
	}
	
	public void deleteAll() throws IOException {
		indexWriter.deleteAll();
	}
	
	public void close() throws IOException {
		indexWriter.close();
	}
}
