package cn.donespeak.szuboardse.util.board;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;

import cn.donespeak.szuboardse.entity.Publisher;

public class PublisherCache {
	private static Logger logger = Logger.getLogger(PublisherCache.class);
	// 对比这样的实现和通过 redis 这样的缓存技术的实现的区别
	// TODO@DoneSpeak 目前还没有做到保持这三个数据的同步
	private static CopyOnWriteArrayList<Publisher> publishers = null;
	private static List<Publisher> unmodifiedpublishers = null;
	private static ConcurrentHashMap<String, Publisher> namePublisherMap = null;
	private static ConcurrentHashMap<Integer, Publisher> idPublishereMap = null;

	private static final Object lock = new Object();
	
	private PublisherCache() {}
	
	public static void initCache(List<Publisher> pubishers) {
		logger.info("Initialize publisherCache");
		updateCache(pubishers);
	}
	// TODO@DoneSpeak: 思考一个线程安全类的设计原则是什么
	// TODO@DoneSepak: 如果需要使用外面传入的引用类型类添加数据应该如何操作
	// TODO@DoneSpeak: 是否需要考虑传入的引用类型类在外面被修改的问题
	public static void updateCache(List<Publisher> newPublishers) {
		ConcurrentHashMap<String, Publisher> tmpNameIdMap = new ConcurrentHashMap<String, Publisher>();
		ConcurrentHashMap<Integer, Publisher> tempIdNameMap = new ConcurrentHashMap<Integer, Publisher>();
		for(Publisher publisher: newPublishers) {
			tmpNameIdMap.put(publisher.getName(), publisher);
			tempIdNameMap.put(publisher.getId(), publisher);
		}
		synchronized(lock) {
			publishers = new CopyOnWriteArrayList<Publisher>(newPublishers);
			namePublisherMap = tmpNameIdMap;
			idPublishereMap = tempIdNameMap;
			// update unmodified categories
			unmodifiedpublishers = Collections.unmodifiableList(publishers);
		}
	}
	
	public static void addPublisher(Publisher publisher) {
		synchronized(lock) {
			publishers.add(publisher);
			idPublishereMap.put(publisher.getId(), publisher);
			namePublisherMap.put(publisher.getName(), publisher);
			// update unmodified categories
			unmodifiedpublishers = Collections.unmodifiableList(publishers);
		}
	}
	
	// TODO@DoneSpeak: 内部的数据是否可以直接暴露到外面，以及是否需要返回副本
	public static List<Publisher> listPublishers() {
		return unmodifiedpublishers;
	}

	public static Publisher get(int publisherId) {
		return idPublishereMap.get(publisherId);
	}
	
	public static Publisher get(String publisherName) {
		return namePublisherMap.get(publisherName);
	}
}
