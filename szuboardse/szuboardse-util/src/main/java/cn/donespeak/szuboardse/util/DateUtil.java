package cn.donespeak.szuboardse.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	/**
	 * transform yyyy-MM-dd HH:mm:ss format or yyyy/MM/dd HH:mm:ss format 
	 * 		or yyyy-MM-dd format or yyyy/MM/dd format date string to timestamp
	 * @param datestring a string with format like yyyy-MM-dd HH:mm:ss or yyyy/MM/dd HH:mm:ss or yyyy-MM-dd or yyyy/MM/dd
	 * @return the time stamp
	 * @throws ParseException
	 */
	public static long dateToStamp(String datestring) throws ParseException{
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;        
    	try {
    		date = sdf1.parse(datestring);
		} catch (ParseException e) {
			datestring = datestring.replace('/', '-');
			date = sdf2.parse(datestring);
		}    	
        return date.getTime();
    }
	
	public static String stampToDate(long timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(timestamp);
        return sdf.format(date);
    }
	
	public static String stampToDate(String timestamp){
        long lt = new Long(timestamp);
        return stampToDate(lt);
    }
	
	public static long dateAddDaysToTimeStamp(Date date, int n){
		return dateAddDays(date, n).getTime();
	}
	
	public static Date dateAddDays(Date date, int n){
		Calendar dd = Calendar.getInstance();
        dd.setTime(date);
        dd.add(Calendar.DATE, n);
        return dd.getTime();
	}
	
	public static long timeSpan(Date start, Date end){
		return end.getTime() - start.getTime();
	}
		
	public static long nDaysToTimeStamp(int n){
		long timestamps = n * 24L * 60L * 60L;
		return timestamps;
	}

	public static long getNow() {
		return new Date().getTime();
	}

	public static Date now() {
		return new Date();
	}

	public static Object format(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
	}
}
