package cn.donespeak.szuboardse.util.crawler;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import org.jsoup.select.Elements;
import org.junit.Test;

import cn.donespeak.szuboardse.dto.FullArticle;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.crawler.WrongPageLayoutException;

public class TestArticleCrawler {
	
//	@Test
	public void testCrawlerWithMutilTable() {
		System.out.println("测试内容中含有表格的文章");
		ArticleListItem item = new ArticleListItem();
		item.setArticleId(22222);
		item.setCategory(new Category(1, "我"));
		item.setPublisher(new Publisher(2, "你"));
		item.setTitle("This is title");
		item.setUrl("http://www1.szu.edu.cn/board/view.asp?id=364142");
		File file = new File("/home/ygr/有关秋季赴美国名校宣讲会的紧急通知—深圳大学内部网.html");
		try {
			ArticleCrawler crawler = new ArticleCrawler(item, file);
			FullArticle fullArticle = crawler.extractFullArticle();
			System.out.println(fullArticle.getArticle());
			System.out.println(fullArticle.getPictures());
			System.out.println(fullArticle.getAttaches());
		} catch (IOException | WrongPageLayoutException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test
	public void testCrawlerWithAttaches() {
		System.out.println("测试含有附件的文章");
		ArticleListItem item = new ArticleListItem();
		item.setArticleId(22222);
		item.setCategory(new Category(1, "我"));
		item.setPublisher(new Publisher(2, "你"));
		item.setTitle("This is title");
		item.setUrl("http://www1.szu.edu.cn/board/view.asp?id=362811");
		File file = new File("/home/ygr/attachesArticle.html");
		try {
			ArticleCrawler crawler = new ArticleCrawler(item, file);
			FullArticle fullArticle = crawler.extractFullArticle();
			System.out.println(fullArticle.getArticle());
			System.out.println(fullArticle.getPictures());
			System.out.println(fullArticle.getAttaches());
		} catch (IOException | WrongPageLayoutException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCrawlerWithPicture() {
		System.out.println("测试含有图片的文章");
		ArticleListItem item = new ArticleListItem();
		item.setArticleId(22222);
		item.setCategory(new Category(1, "我"));
		item.setPublisher(new Publisher(2, "你"));
		item.setTitle("This is title");
		item.setUrl("http://www1.szu.edu.cn/board/view.asp?id=363841");
		File file = new File("/home/ygr/articleWithPicture.html");
		try {
			ArticleCrawler crawler = new ArticleCrawler(item, file);
			FullArticle fullArticle = crawler.extractFullArticle();
			System.out.println(fullArticle.getArticle());
			System.out.println(fullArticle.getPictures());
			System.out.println(fullArticle.getAttaches());
		} catch (IOException | WrongPageLayoutException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
