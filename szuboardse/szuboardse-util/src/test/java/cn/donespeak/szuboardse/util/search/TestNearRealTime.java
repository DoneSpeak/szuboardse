package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.nio.file.FileSystems;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

/*
 * 测试目标：
 * 1. 添加新的document之后，之前获取的indexSearch时候可以立即检索到
 * 2. 删除或者更新文档之后，是否可以立即检索到 -- 需要使用 indexWriter.commit(), 并重新获取 indexSearcher.
 * 3. 不同次数获得的indexSearch是否一样？ -- 如果磁盘中的索引发生变化，并调用了searcherManager.maybeRefresh();就会不一样。
 * 		否则打开是一样的
 * 4. 如何关闭之前获取到的indexSearch? -- 使用 searcherManager.release(indexSearcher); 即可，
 * 		但只有在所有相关的 indexSearcher都关闭之后才正真释放资源。
 * 5. 如何获得最新的IndexSearch?
 * 		每次获取之前都检测索引是否发生了变化，发生了变化就刷新, 并重新获得 indexSearch.
 * 		if (!searcherManager.isSearcherCurrent()) {
			// 说明有新数据，需要刷新
			searcherManager.maybeRefresh();
		}
 * 6. 如何将内存中的索引更新到磁盘存储中？
 * 		indexWriter.commit();
 */
public class TestNearRealTime {
	private SearcherManager manager;
	
	public TestNearRealTime() throws IOException {
		manager = new SearcherManager(getFSDirectory(), null);
	}
	
	public Directory getFSDirectory() throws IOException {
		Directory directory = FSDirectory.open(
		         FileSystems.getDefault().getPath("/var/szuboardse/test/indexes/searchermanager"));
		return directory;
	}
	
	public IndexWriter getIndexWriter() throws IOException{
		IndexWriter indexWriter = new IndexWriter(getFSDirectory(), new IndexWriterConfig(new StandardAnalyzer()));
		return indexWriter;
	}

	public static void main(String[] args) throws IOException {
		Directory ramDirectory = new RAMDirectory();
		Document document = new Document();
		document.add(new StringField("title", "Doc1", Store.YES));
		document.add(new IntPoint("ID", 1));
		IndexWriter indexWriter = new IndexWriter(ramDirectory, new IndexWriterConfig(new StandardAnalyzer()));
		indexWriter.addDocument(document);
		indexWriter.commit();
		SearcherManager searcherManager = new SearcherManager(ramDirectory, null);
		document = new Document();
		document.add(new StringField("title", "Doc2", Store.YES));
		document.add(new IntPoint("ID", 2));
		indexWriter.addDocument(document);
		// 即使commit，搜索依然不可见，需要重新打开reader
		indexWriter.commit();
		// 只能看到Doc1
		IndexSearcher indexSearcher = searcherManager.acquire();
		int count = indexSearcher.count(new MatchAllDocsQuery());
		if (count > 0) {
			TopDocs topDocs = indexSearcher.search(new MatchAllDocsQuery(), count);
			for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
				System.out.println(indexSearcher.doc(scoreDoc.doc));
			}
		}
		System.out.println("=================================");
		// 最好放到finally语句块中
		searcherManager.release(indexSearcher);
		if (!searcherManager.isSearcherCurrent()) {
			// 说明有新数据，需要刷新
			searcherManager.maybeRefresh();
		}
		indexSearcher = searcherManager.acquire();
		count = indexSearcher.count(new MatchAllDocsQuery());
		if (count > 0) {
			TopDocs topDocs = indexSearcher.search(new MatchAllDocsQuery(), count);
			for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
				System.out.println(indexSearcher.doc(scoreDoc.doc));
			}
		}
		System.out.println("=================================");
		searcherManager.release(indexSearcher);
		
		indexSearcher = searcherManager.acquire();
//		document = new Document();
//		document.add(new StringField("title", "Doc3", Store.YES));
//		document.add(new IntPoint("ID", 3));
//		indexWriter.addDocument(document);
//		indexWriter.commit();
//		if (!searcherManager.isSearcherCurrent()) {
//			// 说明有新数据，需要刷新
//			searcherManager.maybeRefresh();
//		}
		IndexSearcher indexSearcher2 = searcherManager.acquire();
		
		System.out.println(indexSearcher);
		System.out.println(indexSearcher2);
		System.out.println(indexSearcher2 == indexSearcher);
		searcherManager.release(indexSearcher);
		TopDocs topDocs = indexSearcher2.search(new TermQuery(new Term("title", "Doc1")), 5);
		System.out.println(topDocs.totalHits);
		for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
			System.out.println(indexSearcher2.doc(scoreDoc.doc));
		}
	}
}
