package cn.donespeak.szuboardse.util;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;

import cn.donespeak.szuboardse.entity.Article;
import cn.donespeak.szuboardse.entity.Category;
import cn.donespeak.szuboardse.entity.Publisher;
import cn.donespeak.szuboardse.exception.crawler.WrongPageLayoutException;
import cn.donespeak.szuboardse.util.crawler.ArticleCrawler;
import cn.donespeak.szuboardse.util.crawler.ArticleListItem;

public class TestUrlReachable {
	static String baseUri = "http://www1.szu.edu.cn/board/view.asp?id=";
	
	/**
	 * 找到最小的可以访问的路径
	 * @param args
	 * @throws IOException
	 * @throws WrongPageLayoutException
	 * @throws ParseException
	 */
	public static void main(String[] args) throws IOException, WrongPageLayoutException, ParseException {
		for(int start = 10000; start <= 65380; start ++) {
			String url = baseUri + start;
			boolean reachable = testUrlReachableWithTimeOut(start);
			System.out.println(start + " : " + reachable);
			if(reachable) {
				System.out.println("The first reachable url: " + url);
				return;
			}
		}
	}
	
	public static boolean testUrlReachableWithTimeOut(long articleId) {
		ArticleListItem item = new ArticleListItem();
		item.setArticleId(articleId);
		item.setCategory(new Category(1, ""));
		item.setPublisher(new Publisher(2, ""));
		item.setTitle("title");
		item.setUrl(baseUri + articleId);
		
		
		Article article = null;
		try {
			ArticleCrawler crawler = new ArticleCrawler(item);
			article = crawler.extractArticle();
		} catch (Exception e) {
			return false;
		}
//		System.out.println(article);
		return article != null;
    }  
}
