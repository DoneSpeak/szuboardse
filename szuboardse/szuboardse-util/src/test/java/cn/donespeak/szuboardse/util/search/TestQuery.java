package cn.donespeak.szuboardse.util.search;

import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

public class TestQuery {
	public void variousQuery() {
		// term 是最小的单位，不再进行切分 -> 可以用来搜索id, url, category这些不变的类型
		Query termQuery = new TermQuery(new Term("field", "毕业设计"));
		try {
			Query multiFieldQuery = MultiFieldQueryParser.parse("query", 
					new String[]{"title", "content"}, 
					new BooleanClause.Occur[]{BooleanClause.Occur.MUST, BooleanClause.Occur.MUST}, 
					LuceneHelper.getChineseAnalyzer());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
