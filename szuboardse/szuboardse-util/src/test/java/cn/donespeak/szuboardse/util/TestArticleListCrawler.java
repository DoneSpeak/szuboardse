package cn.donespeak.szuboardse.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cn.donespeak.szuboardse.util.crawler.ArticleListCrawler;
import cn.donespeak.szuboardse.util.crawler.ArticleListItem;
import junit.framework.TestCase;

public class TestArticleListCrawler extends TestCase {

	public void testListArticleUrlsFromLocalFile(){
		String localFile = "/home/ygr/view-source_www1.szu.edu.cn_board_.html";
		try {
			// TODO@DoneSpeak initialize the category and publisher cache
			ArticleListCrawler listCrawler = new ArticleListCrawler();
			
			List<ArticleListItem> items = listCrawler.listArticleItems(new File(localFile));
			int index = 0;
			for(ArticleListItem item: items) {
				System.out.println((index ++) + ": " + item);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
