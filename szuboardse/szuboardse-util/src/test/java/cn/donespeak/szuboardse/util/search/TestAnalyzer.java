package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.FileSystems;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import cn.donespeak.szuboardse.util.search.ikanalyzer.GIKAnalyzer;

public class TestAnalyzer {

	Directory directory = new RAMDirectory();
	
	public void init() throws IOException {
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter indexWriter = new IndexWriter(directory, config);
		indexWriter.deleteAll();
		String[] texts = new String[] { "million dddfd dfae edredfd dgkh dtexd asde",
				"take me to your heart, I miss you every days", "Every one has a die, but not to me, not today.",
				"There are million dollars", "Here million million million peoper, and million dogs" };

		String[] strs = new String[] { "yes million dogs", "have a good day",
				"If you have one million dollars, what would you do." };

		for (String text : texts) {
			Document doc = new Document();
			doc.add(new TextField("text", text, Store.NO));
			indexWriter.addDocument(doc);
		}
		indexWriter.close();
	}
	
	public void testReuseAnalyzer() throws IOException, ParseException {
		Analyzer analyzer = new StandardAnalyzer();

		IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(directory));

		QueryParser parser = new QueryParser("text", analyzer);
		Query query = parser.parse("million");

		TopDocs hits = searcher.search(query, 10);
		
		QueryParser parser2 = new QueryParser("text", analyzer);
		Query query2 = parser2.parse("million");

		TopDocs hits2 = searcher.search(query2, 10);
	}
	
	public static void testGKAnnalyzer() throws IOException {
		Analyzer analyzer = new GIKAnalyzer();
		TokenStream tokenStream = analyzer.tokenStream("Field", new StringReader("计算机与软件学院 奖学金"));
		CharTermAttribute cta = tokenStream.addAttribute(CharTermAttribute.class);
		tokenStream.reset();  //必须先调用reset方法
		while(tokenStream.incrementToken()){
			System.out.println(cta);
		}
	}
	
	public static void main(String[] args) throws IOException, ParseException {
		TestAnalyzer tester = new TestAnalyzer();
		tester.init();
		tester.testReuseAnalyzer();
		testGKAnnalyzer();
	}
}
