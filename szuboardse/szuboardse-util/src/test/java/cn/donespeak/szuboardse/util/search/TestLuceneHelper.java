package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.junit.Test;

import cn.donespeak.szuboardse.util.search.ikanalyzer.GIKAnalyzer;

public class TestLuceneHelper {

	public void testGetAnalyzer() throws IOException {
		String queryStr = "模块并且访问权限类已经不鼓励使用的吧奖学金is to be a";
		Analyzer analyzer = new GIKAnalyzer(true);
		TokenStream tokenStream = analyzer.tokenStream("txt", new StringReader(queryStr));
		CharTermAttribute  term = tokenStream.getAttribute(CharTermAttribute.class);
		tokenStream.reset();
		while(tokenStream.incrementToken()) {
			System.out.println(term.toString());
		}
	}
}
