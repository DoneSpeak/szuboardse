package cn.donespeak.szuboardse.util.search;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import cn.donespeak.szuboardse.dto.SearchOption;
import cn.donespeak.szuboardse.dto.SearchOption.Region;
import cn.donespeak.szuboardse.enums.TimeDurationOption;
import cn.donespeak.szuboardse.vo.ResultItem;
import cn.donespeak.szuboardse.vo.ResultPage;

public class TestContentSearch {
	

	private static void searchInTitle() throws ParseException, IOException {
		ContentSearcher contentSearcher = new ContentSearcher();
		IndexSearcher searcher = contentSearcher.acquireIndexSearcher();
		try {
			String title = "法学院民商法协会2018年招新通知";
			QueryParser parser = new QueryParser(FieldName.TITLE.name(), LuceneHelper.getChineseAnalyzer());
			Query query = parser.parse(title);
			TopDocs topDocs = searcher.search(query, 50);
			ScoreDoc[] hits = topDocs.scoreDocs;
			System.out.println(topDocs.totalHits);
			System.out.println(hits.length);
			for(ScoreDoc hit: hits) {
				Document doc = searcher.doc(hit.doc);
				System.out.println(doc.get(FieldName.ID.name()));
			}
			System.out.println("检索结果正确：" + "ARTICLE#362717".equals(searcher.doc(hits[0].doc).get(FieldName.ID.name())));
		} finally {
			contentSearcher.releaseIndexSearcher(searcher);
		}
	}
	
	private static void searchWithOption() {
		String title = "法学院民商法协会2018年招新通知";
		SearchOption searchOption = new SearchOption();
		searchOption.setLimitQuery(title);
		searchOption.setPageIndex(1);
		searchOption.setRegion(Region.ALL);
		searchOption.setDuration(TimeDurationOption.THREE_MONTHS.toDuration());
		System.out.println(searchOption);
		ResultPage resultPage = null;
		ContentSearcher contentSearcher = new ContentSearcher();
		try {
			resultPage = contentSearcher.search(searchOption.trimQuery());
		} catch (ParseException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		System.out.println(resultPage.getResultItems().size());
		for(ResultItem item: resultPage.getResultItems()) {
			System.out.println(item);
		}
		boolean right = resultPage.getResultItems().size() > 0 
				&& title.equals(resultPage.getResultItems().get(0).getTitle());
		System.out.println("结果正确：" + right);
	}
	
	public static void main(String[] args) throws IOException, ParseException {
		System.out.println("Search with title");
		searchInTitle();
		System.out.println("========================");
		System.out.println("Search with searchOption");
		searchWithOption();
	}
}
