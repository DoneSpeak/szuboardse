package cn.donespeak.szuboardse.util.search;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.FileSystems;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TextFragment;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class TestHighlighter {
	private String indexDir = "/home/ygr/test/lucene/highlighter/indexes";

	public static final FieldType TYPE_STORED = new FieldType();

	static {
		TYPE_STORED.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
		TYPE_STORED.setTokenized(true);
		TYPE_STORED.setStored(true);
		TYPE_STORED.freeze();
	}

	public void init() throws IOException {
		Directory directory = FSDirectory.open(FileSystems.getDefault().getPath(indexDir));
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter indexWriter = new IndexWriter(directory, config);
		indexWriter.deleteAll();
		String[] texts = new String[] { "million dddfd dfae edredfd dgkh dtexd asde",
				"take me to your heart, I miss you every days", "Every one has a die, but not to me, not today.",
				"There are million dollars", "Here million million million peoper, and million dogs" };

		String[] strs = new String[] { "yes million dogs", "have a good day",
				"If you have one million dollars, what would you do." };

		for (String text : texts) {
			Document doc = new Document();
			doc.add(new Field("text", text, TYPE_STORED));
			indexWriter.addDocument(doc);
		}
//		for (String str : strs) {
//			Document doc = new Document();
//			doc.add(new Field("text", str, StringField.TYPE_STORED));
//			indexWriter.addDocument(doc);
//		}
		indexWriter.close();
	}

	public void search() throws IOException, ParseException, InvalidTokenOffsetsException {
		// ... Above, create documents with two fields, one with term vectors
		// (tv) and one without (notv)
		Analyzer analyzer = new StandardAnalyzer();
		Directory directory = FSDirectory.open(FileSystems.getDefault().getPath(indexDir));

		IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(directory));

		QueryParser parser = new QueryParser("text", analyzer);
		Query query = parser.parse("million");

		TopDocs hits = searcher.search(query, 10);
		ScoreDoc[] scoreDocs = hits.scoreDocs;
		SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter("<span class='highlighter'>", "</span>");
		Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));
		System.out.println("There are " + scoreDocs.length + " results");
		for (int i = 0; i < scoreDocs.length; i++) {
			int id = hits.scoreDocs[i].doc;
			Document doc = searcher.doc(id);
			String text = doc.get("text");
			if (text != null) {
				highlighter.setTextFragmenter(new SimpleFragmenter(140));
				TokenStream tokenStream = analyzer.tokenStream("text", new StringReader(text));
				String highlightText = highlighter.getBestFragment(tokenStream, text);
				System.out.println("doc[" + i + "]: " + highlightText);
			}
		}
	}

	public static void main(String[] args) {
		TestHighlighter tester = new TestHighlighter();
		System.out.println(TextField.TYPE_STORED.toString());
		System.out.println(TYPE_STORED.toString());
		System.out.println(StringField.TYPE_STORED.toString());
		try {
			tester.init();
			tester.search();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
