package cn.donespeak.szuboardse.util.search;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;

public class TestQuerySearch {
	
	public static void main(String[] args) throws IOException, ParseException {
		QueryIndexer indexer = new QueryIndexer();
		String[] queries = new String[]{
				"羽毛球比赛",
				"篮球比赛",
				"足球比赛"
		};
		for(String q: queries) {
			indexer.addQuery(q);
		}
		indexer.commit();
		indexer.close();
		QuerySearcher searcher = new QuerySearcher();
		List<String> result = searcher.search("球类比赛", 15);
		for(String r: result) {
			System.out.println(r);
		}
	}
}
