DROP DATABASE IF EXISTS szuboard;
CREATE DATABASE szuboard;

USE szuboard;

create table user
(
    uid bigint unsigned not null auto_increment,
    name varchar(100) not null,
    email varchar(50) not null,
    avatar varchar(256) not null,
    password varchar(150) not null,
	registration_time bigint not null,
    gmt_created bigint not null,
    primary key (uid),
    index idx_email(email)
)engine=innodb auto_increment=10000 default charset=utf8;

create table category
(
    id tinyint unsigned not null auto_increment,
    name varchar(50) not null,
    gmt_created bigint not null,
    primary key (id)
)engine=innodb default charset=utf8;

create table publisher
(
    id int unsigned not null auto_increment,
    name varchar(50) not null,
    gmt_created bigint not null,
    primary key (id)
)engine=innodb default charset=utf8;

create table article
(
    id bigint unsigned not null auto_increment,
    url varchar(500) not null,
    title varchar(100) not null,
    digest varchar(1000) not null,
    category_id tinyint unsigned not null,
    publisher_id int unsigned not null,
    published_time bigint not null,
    updated_time bigint,
    base_click int not null,
    addition_click int not null,
    gmt_created bigint not null,
    gmt_modified bigint not null,
    primary key(id),
    key article_category_id_fk(category_id),
    key article_publisher_id_fk(publisher_id),
    constraint article_category_id_fk foreign key(category_id) references category(id),
    constraint article_publisher_id_fk foreign key(publisher_id) references publisher(id)
) engine=innodb auto_increment=10000 default charset=utf8;

create table attach
(
    id bigint unsigned not null auto_increment,
    url varchar(500) not null,
    filename varchar(100) not null,
    digest varchar(1000) not null,
    article_id bigint unsigned not null,
    category_id tinyint unsigned not null,
    publisher_id int unsigned not null,
    published_time bigint not null,
    updated_time bigint,
    base_click int not null,
    addition_click int not null,
    gmt_created bigint not null,
    gmt_modified bigint not null,
    primary key(id),
    key attach_article_id_fk(article_id),
    key attach_category_id_fk(category_id),
    key attach_publisher_id_fk(publisher_id),
    constraint attach_article_id_fk foreign key(article_id) references article (id),
    constraint attach_category_id_fk foreign key(category_id) references category(id),
    constraint attach_publisher_id_fk foreign key(publisher_id) references publisher(id)
) engine=innodb auto_increment=10000 default charset=utf8;

create table content(
    article_id bigint unsigned,
    attach_id bigint unsigned,
    content longtext not null,
    constraint content_article_id_fk foreign key(article_id) references article (id),
    constraint content_attach_id_fk foreign key(attach_id) references attach(id)
) engine=innodb auto_increment=10000 default charset=utf8;

create table picture
(
    id bigint unsigned not null auto_increment,
    article_id bigint unsigned not null,
    path varchar(500) not null,
    url varchar(500) not null,
    gmt_created bigint not null,
    primary key(id),
    key picture_article_id_fk(article_id),
    constraint picture_article_id_fk foreign key(article_id) references article(id)
) engine=innodb auto_increment=10000 default charset=utf8;

create table favorite_content
(
    id bigint unsigned not null auto_increment,
    uid bigint unsigned not null,
    article_id bigint unsigned,
    attach_id bigint unsigned,
    gmt_created bigint not null,
    primary key(id),
    key favorite_content_user_id_fk(uid),
    constraint favorite_content_user_id_fk foreign key(uid) references user(uid)
) engine=innodb auto_increment=10000 default charset=utf8;

create table favorite_source
(
    id bigint unsigned not null auto_increment,
    uid bigint unsigned not null,
    category_id tinyint unsigned not null,
    publisher_id int unsigned not null,
    gmt_created bigint not null,
    primary key(id),
    key favorite_source_user_id_fk(uid),
    key favorite_source_category_id_fk(category_id),
    key favorite_source_publisher_id_fk(publisher_id),
    constraint favorite_source_user_id_fk foreign key(uid) references user(uid),
    constraint favorite_source_category_id_fk foreign key(category_id) references category(id),
    constraint favorite_source_publisher_id_fk foreign key(publisher_id) references publisher(id)
) engine=innodb auto_increment=10000 default charset=utf8;
/* ================= */
/*
create table topic
(
    id bigint unsigned not null auto_increment,
    word varchar(50),
    gmt_created bigint not null
)

create table favorite_topic
(
    uid bigint unsigned not null,
    topic_id bigint unsigned not null,
    gmt_created bigint not null
)

create table message
(
    id bigint unsigned not null auto_increment,
    recipient bigint unsigned not null,
    content varchar(500) not null,
    gmt_created bigint not null
)

create table email
(
    id bigint unsigned not null auto_increment,
    recipient bigint unsigned not null,
    subtitle varchar(200) bigint not null,
    content varchar(2000) not null,
    sended_time bigint not null
)
*/