-- 选择mysql数据库
use mysql;
-- 创建本地用户
create user 'superboy'@'localhost' identified by 'iamcooker';
-- 刷新MySQL的系统权限相关表，使添加用户操作生效，以免会出现拒绝访问
flush privileges;

-- 从任意ip登陆的用户
create user 'superboy'@'%' identified by 'iamcooker';

-- 赋予部分权限，其中的shopping.*表示对以shopping所有文件操作。
grant select,delete,update,insert on szuboard.* to superboy@'localhost' identified by 'iamcooker';
grant select,delete,update,insert on szuboard.* to superboy@'%' identified by 'iamcooker';

flush privileges;