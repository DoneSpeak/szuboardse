# 检索
```js
GET /search
params {
    query: String, // [required] 
    region: int,   // [optional] 1: 文章，2: 附件
    category: int, // [optional] 根据列表获取 @see GET /category
    publisher: int,// [optional] 根据列表获取 @see GET /publisher
    duration: String, // [optional] @Todo 之后在准确定义
    
    pageIndex: int, // [required] 从第1页开始
    pageSize: int   // [required]
}

response {
    code: String,
    msg: String,
    data: {
        items: [
            {
                type: String, // ARTICLE: 文章， ATTACH: 附件
                id: long,
                articleId: long,
                attachId: long,
                title: String,
                url: String,
                category: {
                    id: int,
                    name: String
                },
                publisher: {
                    id: int,
                    name: String
                },
                publishedTime: long,
                updateTime: long,
                digest: String,
                isfavorite: boolean,
                articleUrl: String, // 附件特有
                pictures: [     // 文章特有
                    id: long,
                    articleId: long,
                    path: String,  // 本地服务器的路径
                    url: String,   // 图片来源的路径
                ],
                attaches: [      // 文章特有
                    id: long,
                    filename: String,
                    url: String
                ]
            },
            ...
        ],
        pageInfo: {
            pageIndex: 1;
            pageSize: 10;
            pageNum: 1;
            total: 2;
        },
        timecost: 12133,
        totalhits: 2
    }
}
```