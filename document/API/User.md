# 注册用户
```js
POST /users
params {
    email: String,
    password: String
}

response {
    code: String,
    msg: String,
    data: {}
}
```

# 获取有户主页
```js
GET /user/profile
header: {x-cross-token: String}

response {
    code: String, 
    msg: String,
    data: {
        user: {
            email: String,
            name: String,
            avatar: String
        },
        sources: [
            {
                id: long,
                categoryId: int,
                publisherId: int
            },
            ...
        ]
    }
}
```