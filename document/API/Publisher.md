# 获取所有的发布单位
```js
GET /publishers

response {
    publishers: [
        {
            id: int,
            name: String
        },
        ...
    ]
}
```