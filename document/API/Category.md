# 获取所有的分类
```js
GET /categories

response {
    categories: [
        {
            id: int,
            name: String
        },
        ...
    ]
}
```