# 获取收藏的文章

# 收藏一篇文章
```js
POST /favorite/articles
header: {x-cross-token: String}

params {
    id: long
}
```

# 收藏一件附件
```js
POST /favorite/attaches
header: {x-cross-token: String}

params {
    id: long
}
```

# 取消收藏文章
```js
DELETE /favorite/articles/{articleId}
header: {x-cross-token: String}
```

# 取消收藏附件
```js
DELETE /favorite/attaches/{attachId}
header: {x-cross-token: String}
```

# 标记喜欢的内容来源
```js
POST /favorite/source

params {
    category: int,
    publisher: int
}

```

# 获取已经标记喜欢的内容来源
```js
GET /favorite/source
header: {x-cross-token: String}

response {
    code: String,
    msg: String,
    data: {
        sources: [
            {
                category: int,
                publisher: int
            },
            ...
        ]
    }
}
```

# 取消对某个主题的关注
```js
DELETE /favorite/source/{favoriteSourceId}
```