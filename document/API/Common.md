# 相应格式
```js
Response {
    code: String,
    msg: String,
    data: {}
}
```
# 请求规范

# 响应码

# Code 说明
### auth
| code                        | msg              | description |
| --------------------------- | ---------------- | ----------- |
| user_not_found              | 用户不存在       |             |
| email_or_password_incorrect | 邮箱或者密码错误 |

### system
| code               | msg          | description |
| ------------------ | ------------ | ----------- |
| inner_error        | 系统内部错误 |             |
| database_exception | 数据库错误   |             |

### user
| code             | msg          | description |
| ---------------- | ------------ | ----------- |
| user:email_taken | 邮箱已被注册 |             |

### search
| code          | msg      | description |
| ------------- | -------- | ----------- |
| anaylze_error | 分析失败 |             |
| search_fail   | 检索失败 |             |