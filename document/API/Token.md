# 通用说明
httpStatus:  
401: 用户没有登录或者token失效

# 获取token -> 登录
```js
POST /tokens
params {
    email: String,
    password: String
}

response {
    code: String,
    msg: String,
    data: {
        token: String,
        user: {
            uid: long,
            name: String,
            email: String,
            avatar: String
        }
    }
}

code:  [
    "success",
    "auth:user_not_found"
]

```

# 刷新token，使得有效期延长
```js
PUT /tokens

response {
    code: String,
    msg: String,
    data: {
        token: String
    }
}

code: [
    "success"
]
```