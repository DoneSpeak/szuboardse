# 部署后台代码
文件替换

打包
cd szuboardse/szuboardse
mvn clean
mvn package -Dmaven.test.skip=true
copy szuboardse-web\target\szuboardse-web.war C:\server\tomcat\webapps

部署


启动tomcat服务
部署待windows下
```shell
cd /tomcat/bin
service.bat install # 安装tomcat服务器
# 服务安装后会输出服务名称
# The service 'Tomcat8' has been installed.
net start Tomcat8 # Tomcat8 是服务安装之后的名字
```

# 部署前端代码
## 编译
```shell
npm install
npm run build
```
## 拷贝dist内的文件到配置的网站根目录下
