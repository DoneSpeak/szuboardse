参考: 
https://blog.csdn.net/cclovett/article/details/26377269
[配置tomcat+nginx反向代理](https://github.com/biezhi/java-bible/blob/master/learn_server/config-nginx-proxy.md)
下载 -> 解压 -> 双击 nginx.exe(一闪而过) -> 修改 conf/nginx.conf

```shell
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;

    #access_log  logs/host.access.log  main;

    location / {
        root   C:\var\szuboardse\www\static;
        index  index.html index.htm;
    }

	location /resources {
	    proxy_pass http://127.0.0.1:8080/szuboardse-web;
	}

	location /files	{
	    root C:\var\szuboardse\www\store;
	}

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
}

```

```shell
nginx -s reload
```